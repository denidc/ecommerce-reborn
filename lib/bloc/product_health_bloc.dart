import 'package:bloc/bloc.dart';
import 'package:ecommerce_reborn/model/product.dart';

class ProductHealthEvent {}

abstract class ProductHealthState {}

class ProductHealthUninitialized extends ProductHealthState {}

class ProductHealthLoaded extends ProductHealthState {
  List<Product> products;
  bool hasReachedMax;

  ProductHealthLoaded({this.products, this.hasReachedMax});

  ProductHealthLoaded copyWith({List<Product> products, bool hasReachedMax}) {
    return ProductHealthLoaded(
        products: products ?? this.products,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax
    );
  }
}

class ProductHealthBloc extends Bloc<ProductHealthEvent, ProductHealthState>{
  // ProductBloc(ProductState initialState) : super(initialState);
  ProductHealthBloc() :super(ProductHealthUninitialized());

  @override
  Stream<ProductHealthState> mapEventToState(ProductHealthEvent event) async*{
    List<Product> products;

    if(state is ProductHealthUninitialized){
      products = await Product.connectToApi("2", 0, 10);
      yield ProductHealthLoaded(products: products, hasReachedMax: false);
    }else{
      ProductHealthLoaded productHealthLoaded = state as ProductHealthLoaded;
      products = await Product.connectToApi("2", productHealthLoaded.products.length, 10);

      yield (products.isEmpty) ? productHealthLoaded.copyWith(hasReachedMax: true) :
      ProductHealthLoaded(products: productHealthLoaded.products + products, hasReachedMax: false);
    }
    throw UnimplementedError();
  }
}
