import 'package:ecommerce_reborn/model/product.dart';
import 'package:bloc/bloc.dart';

class ProductBeautyEvent {}

abstract class ProductBeautyState {}

class ProductBeautyUninitialized extends ProductBeautyState {}

class ProductBeautyLoaded extends ProductBeautyState {
  List<Product> products;
  bool hasReachedMax;

  ProductBeautyLoaded({this.products, this.hasReachedMax});
  ProductBeautyLoaded copyWith({List<Product> products, bool hasReachedMax}) {
    return ProductBeautyLoaded(
        products: products ?? this.products,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax
    );
  }
}

class ProductBeautyBloc extends Bloc<ProductBeautyEvent, ProductBeautyState>{
  // ProductBeautyBloc(ProductBeautyState initialState) : super(initialState);
  ProductBeautyBloc() :super(ProductBeautyUninitialized());

  @override
  Stream<ProductBeautyState> mapEventToState(ProductBeautyEvent event) async*{
    List<Product> products;

    if(state is ProductBeautyUninitialized){
      products = await Product.connectToApi("1", 0, 10);
      yield ProductBeautyLoaded(products: products, hasReachedMax: false);
    }else{
      ProductBeautyLoaded productBeautyLoaded = state as ProductBeautyLoaded;
      products = await Product.connectToApi("1", productBeautyLoaded.products.length, 10);

      yield (products.isEmpty) ? productBeautyLoaded.copyWith(hasReachedMax: true) :
        ProductBeautyLoaded(products: productBeautyLoaded.products + products,
            hasReachedMax: false);
    }
    // throw UnimplementedError();
  }
}
