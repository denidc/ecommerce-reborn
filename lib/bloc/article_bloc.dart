import 'package:bloc/bloc.dart';
import 'package:ecommerce_reborn/model/article.dart';

class ArticleEvent {}

abstract class ArticleState {}

class ArticleUninitialized extends ArticleState {}

class ArticleLoaded extends ArticleState {
  List<Article> articles;
  bool hasReachedMax;

  ArticleLoaded({this.articles, this.hasReachedMax});
  ArticleLoaded copyWith({List<Article> articles, bool hasReachedMax}) {
    return ArticleLoaded(
        articles: articles ?? this.articles,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax
    );
  }
}

class ArticleBloc extends Bloc<ArticleEvent, ArticleState>{
  // ProductBeautyBloc(ProductBeautyState initialState) : super(initialState);
  ArticleBloc() :super(ArticleUninitialized());

  @override
  Stream<ArticleState> mapEventToState(ArticleEvent event) async*{
    List<Article> articles;

    if(state is ArticleUninitialized){
      articles = await Article.connectToApi(0, 4);
      yield ArticleLoaded(articles: articles, hasReachedMax: false);
    }else{
      ArticleLoaded articleLoaded = state as ArticleLoaded;
      articles = await Article.connectToApi(articleLoaded.articles.length, 4);

      yield (articles.isEmpty) ? articleLoaded.copyWith(hasReachedMax: true) :
      ArticleLoaded(articles: articleLoaded.articles + articles,
          hasReachedMax: false);
    }
    // throw UnimplementedError();
  }
}