import 'package:ecommerce_reborn/bloc/article_bloc.dart';
import 'package:ecommerce_reborn/bloc/product_beauty_bloc.dart';
import 'package:ecommerce_reborn/bloc/product_health_bloc.dart';
import 'package:ecommerce_reborn/test.dart';
import 'package:ecommerce_reborn/ui/auth/login/login_activity.dart';
import 'package:ecommerce_reborn/ui/cart/cart_activity.dart';
import 'package:ecommerce_reborn/ui/cart/check_out_%20page.dart';
import 'package:ecommerce_reborn/ui/cart/confirmation_payment_page.dart';
import 'package:ecommerce_reborn/ui/cart/detail_payment_page.dart';
import 'package:ecommerce_reborn/ui/cart/payment_page.dart';
import 'package:ecommerce_reborn/ui/cart/verification_payment_page.dart';
import 'package:ecommerce_reborn/ui/colcal/colcal_activity.dart';
import 'package:ecommerce_reborn/ui/colcal/result_calculator_collagen.dart';
import 'package:ecommerce_reborn/ui/detail/article_detail_activity.dart';
import 'package:ecommerce_reborn/ui/detail/product_detail_activity.dart';
import 'package:ecommerce_reborn/ui/detail/tips_detail_activity.dart';
import 'package:ecommerce_reborn/ui/home/home_activity.dart';
import 'package:ecommerce_reborn/ui/main_activity.dart';
import 'package:ecommerce_reborn/ui/secret/secret_activity.dart';
import 'package:ecommerce_reborn/ui/setting/balance/setting_balance_page.dart';
import 'package:ecommerce_reborn/ui/setting/bank_account/setting_bank_account_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_address_page.dart';
import 'package:ecommerce_reborn/ui/setting/setting_profile_activity.dart';
import 'package:ecommerce_reborn/ui/shop/shop_activity.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  GestureBinding.instance.resamplingEnabled = true;

  OneSignal.shared.init(
      "0d2de6da-c471-4070-80f4-bf5a73b64f13",
      iOSSettings: null
  );
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProductBeautyBloc>(
          create: (context) => ProductBeautyBloc()..add(ProductBeautyEvent()),
        ),
        BlocProvider<ProductHealthBloc>(
          create: (context) => ProductHealthBloc()..add(ProductHealthEvent()),
        ),
        BlocProvider<ArticleBloc>(
          create: (context) => ArticleBloc()..add(ArticleEvent()),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          accentColor: Color(0xffC6AB74),
          accentColorBrightness: Brightness.light,
        ),
        home: MainActivity(0),
      ),
    );
  }
}
