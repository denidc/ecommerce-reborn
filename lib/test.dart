import 'package:auto_size_text/auto_size_text.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:video_player/video_player.dart';

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  bool isLanguage = false;
  String text = "";
  TextEditingController _oldUsernameText = TextEditingController(text: "");

  VideoPlayerController _controller;
  ChewieController chewieController;

  @override
  void initState() {
    super.initState();
    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.landscapeRight,
    //   DeviceOrientation.landscapeLeft,
    //   DeviceOrientation.portraitUp,
    //   DeviceOrientation.portraitDown,
    // ]);

     _controller = VideoPlayerController.network(
        "https://media.istockphoto.com/videos/woman-using-a-hand-sanitizer-alcohol-gel-indoors-video-id1210276758")
      ..initialize().then((value) {
        setState(() {});
      });

    chewieController = ChewieController(
      videoPlayerController: _controller,
      autoPlay: true,
      looping: false,
      allowPlaybackSpeedChanging: false,

    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    chewieController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: _controller.value.initialized
            ? AspectRatio(
          aspectRatio: _controller.value.aspectRatio,
          child: Stack(
            children: [
              Chewie(
                controller: chewieController,
              )
            ],
          ),
        )
            : Container(),
      ),
    );

    //text field banyak huruf
    // return SafeArea(
    //   child: Scaffold(
    //     body: Column(
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       children: [
    //         Container(
    //           height: height / 8,
    //           child: TextField(
    //             controller: _oldUsernameText,
    //             keyboardType: TextInputType.multiline,
    //             keyboardAppearance: Brightness.dark,
    //             textInputAction: TextInputAction.newline,
    //             maxLines: null,
    //             cursorColor: Colors.black,
    //             style: GoogleFonts.poppins(
    //                 textStyle: TextStyle(fontSize: 14)),
    //             decoration: InputDecoration(
    //               hintText: (isLanguage != true)?"Masukkan Nama Pengguna Sekarang":"Enter Username Now",
    //               hintStyle: GoogleFonts.poppins(
    //                   textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
    //               helperStyle: GoogleFonts.poppins(
    //                   textStyle: TextStyle(fontSize: 14)),
    //               enabledBorder: UnderlineInputBorder(
    //                 borderSide: BorderSide(color: Colors.grey, width: 2),
    //               ),
    //               focusedBorder: UnderlineInputBorder(
    //                 borderSide: BorderSide(color: Colors.black, width: 2),
    //               ),
    //             ),
    //           ),
    //         ),
    //         FlatButton(
    //           child: Text("tombol"),
    //           onPressed: (){
    //             setState(() {
    //               text = _oldUsernameText.text;
    //             });
    //             print(text);
    //           },
    //         ),
    //         AutoSizeText(
    //           text,
    //           style: GoogleFonts.poppins(textStyle:
    //           TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xffAFB1BD))),
    //           maxLines: 100,
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}
