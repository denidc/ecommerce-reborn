import 'dart:convert';
import 'package:ecommerce_reborn/utils/links.dart';
import 'package:http/http.dart' as http;

class Article{
  int id;
  int categoryId;
  int roomChatId;
  String about;
  String desc;
  String image;
  int likes;
  int shared;
  String date;

  Article(
      {this.id,
      this.categoryId,
      this.roomChatId,
      this.about,
      this.desc,
      this.image,
      this.likes,
      this.shared,
      this.date});

  static Future<List<Article>> connectToApi(int start,
      int limit) async {
    var apiResult = await http.get(
        Links.mainUrl + "/api/article/" + "offset=" +
            start.toString() + "&limit=" + limit.toString());
    var jsonData = json.decode(apiResult.body);
    var data = jsonData['results'] as List;

    return data
        .map((items) =>
        Article(
          id: items['id'],
          categoryId: items['category_id'],
          roomChatId: items['room_chat_id'],
          about: items['about'],
          desc: items['desc'],
          image: items['image'],
          likes: items['likes'],
          shared: items['shared'],
          date: items['updated_at'].toString().split("T")[0]
        )).toList();
  }
}