import 'dart:convert';
import 'package:ecommerce_reborn/utils/links.dart';
import 'package:http/http.dart' as http;

class Product {
  int id;
  int categoryId;
  int subCategoryId;
  String name;
  String image;
  int price;
  int rating;
  int isBookmark;

  Product({this.id,
    this.categoryId,
    this.subCategoryId,
    this.name,
    this.image,
    this.price,
    this.rating,
    this.isBookmark});

  // factory Product.getProduct(Map<String, dynamic> object){
  //   return Product(
  //     id: object['']
  //   );
  // }

  static Future<List<Product>> connectToApi(String category, int start,
      int limit) async {
    var apiResult = await http.get(
        Links.mainUrl + "/api/product/" + category + "/offset=" +
            start.toString() + "&limit=" + limit.toString());
    var jsonData = json.decode(apiResult.body);
    var data = jsonData['results'] as List;

    return data
        .map((items) =>
        Product(
          id: items['id'],
          categoryId: items['category_id'],
          subCategoryId: items['sub_category_id'],
          name: items['name'],
          image: items['image'],
          price: items['price'],
          rating: items['rating'],
          isBookmark: items['is_bookmark'],
        )).toList();
  }
}
