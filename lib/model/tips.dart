import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ecommerce_reborn/utils/links.dart';

class Tips{
  int id;
  String name;
  String image;
  String desc;

  Tips(this.id, this.name, this.image, this.desc);

//   static Future<List<Tips>> connectToApi(int start,
//       int limit) async {
//     var apiResult = await http.get(
//         Links.mainUrl + "/api/tips/" + "offset=" +
//             start.toString() + "&limit=" + limit.toString());
//     var jsonData = json.decode(apiResult.body);
//     var data = jsonData['results'] as List;
//
//     return data
//         .map((items) =>
//         Tips(
//             id: items['id'],
//             name: items['name'],
//             image: items['image'],
//             desc: items['desc'],
//         )).toList();
//   }
}