import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:ecommerce_reborn/ui/cart/cart_activity.dart';
import 'package:ecommerce_reborn/ui/detail/article_detail_activity.dart';
import 'package:ecommerce_reborn/ui/detail/product_detail_activity.dart';
import 'package:ecommerce_reborn/ui/notification/notification_activity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';
import 'package:video_player/video_player.dart';
import 'package:ecommerce_reborn/test.dart';

class HomeActivity extends StatefulWidget {
  @override
  _HomeActivityState createState() => _HomeActivityState();
}

class _HomeActivityState extends State<HomeActivity>
    with SingleTickerProviderStateMixin {
  bool isLanguage = false;
  bool isPopupCarousel = true;
  bool isLoading = true;
  TabController _tabController;
  ScrollController _scrollViewController;

  final key = GlobalKey<_HomeActivityState>();

  VideoPlayerController _controller;
  Future<void> _intializeVideoPlayerFuture;

  bool isPlay = false;

  double ratingCount = 5;
  int tabPage = 0;

  int _currentHeader = 0;
  List carouselHeader = [
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
  ];

  List<T> mapHeader<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    key.currentState?._onRefresh();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
    // _controller = VideoPlayerController.network(
    //     "https://media.istockphoto.com/videos/woman-using-a-hand-sanitizer-alcohol-gel-indoors-video-id1210276758");
    // _controller.addListener(() {
    //   if (_controller.value.position == _controller.value.duration) {
    //     setState(() {
    //       isPlay = false;
    //     });
    //   }
    // });
    // _controller.pause();
    // _controller.setLooping(false);
    // _controller.setVolume(1.0);
    // _scrollViewController = ScrollController();
    Future.delayed(Duration(seconds: 6), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _scrollViewController.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return (isLoading)
        ? Scaffold(
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              backgroundColor: Colors.white,
              toolbarHeight: height / 14,
              elevation: 0,
              flexibleSpace: SafeArea(
                child: Column(
                  children: [
                    Container(
                      width: width,
                      height: height / 14,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Shimmer.fromColors(
                                highlightColor: Color(0xffF2F2F2),
                                baseColor: Colors.white,
                                child: Container(
                                  height: height / 20,
                                  decoration: BoxDecoration(
                                      color: Color(0xffF0F0F5),
                                      borderRadius: BorderRadius.circular(6)),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.search,
                                          color: Color(0xffAFB1BD),
                                        ),
                                        SizedBox(
                                          width: width / 32,
                                        ),
                                        AutoSizeText(
                                          (isLanguage != true)
                                              ? "Cari Produk"
                                              : "Search Product",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xffAFB1BD))),
                                          maxLines: 1,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Shimmer.fromColors(
                              highlightColor: Color(0xffF2F2F2),
                              baseColor: Colors.white,
                              child: IconButton(
                                onPressed: () {

                                },
                                icon: Icon(
                                  Icons.notifications_none,
                                  color: Color(0xff707070),
                                ),
                              ),
                            ),
                            Shimmer.fromColors(
                              highlightColor: Color(0xffF2F2F2),
                              baseColor: Colors.white,
                              child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.shopping_cart,
                                  color: Color(0xff707070),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Shimmer.fromColors(
                    highlightColor: Color(0xffF2F2F2),
                    baseColor: Colors.white,
                    child: Container(
                      width: width,
                      height: height / 14,
                      decoration: BoxDecoration(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: height / 86,
                  ),
                  Shimmer.fromColors(
                    highlightColor: Color(0xffF2F2F2),
                    baseColor: Colors.white,
                    child: Container(
                      width: width,
                      height: height / 3.5,
                      decoration: BoxDecoration(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: height / 86,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Shimmer.fromColors(
                      highlightColor: Color(0xffF2F2F2),
                      baseColor: Colors.white,
                      child: Container(
                        width: width / 3,
                        height: height / 32,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(16)),
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    physics: BouncingScrollPhysics(),
                    child: Row(
                      children: [1, 2, 3, 4].map((e) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Container(
                            height: height / 2.4,
                            width: width / 2.6,
                            child: Column(
                              children: [
                                Stack(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 12),
                                      child: Shimmer.fromColors(
                                        highlightColor: Color(0xffF2F2F2),
                                        baseColor: Colors.white,
                                        child: Container(
                                          height: height / 4.2,
                                          width: width / 2.6,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: height / 86,
                                ),
                                Center(
                                  child: Shimmer.fromColors(
                                    highlightColor: Color(0xffF2F2F2),
                                    baseColor: Colors.white,
                                    child: Container(
                                      width: width / 4,
                                      height: height / 32,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(16)),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: height / 86,
                                ),
                                Center(
                                  child: Shimmer.fromColors(
                                    highlightColor: Color(0xffF2F2F2),
                                    baseColor: Colors.white,
                                    child: Container(
                                      width: width / 3,
                                      height: height / 32,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(16)),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: height / 86,
                                ),
                                Center(
                                  child: Shimmer.fromColors(
                                    highlightColor: Color(0xffF2F2F2),
                                    baseColor: Colors.white,
                                    child: Container(
                                      width: width / 4,
                                      height: height / 32,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(16)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  Shimmer.fromColors(
                      highlightColor: Color(0xffF2F2F2),
                      baseColor: Colors.white,
                      child: Divider(
                        thickness: 8,
                        color: Colors.white,
                      )),
                ],
              ),
            ),
          )
        : Scaffold(
            key: key,
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              toolbarHeight: height / 14,
              elevation: 0,
              flexibleSpace: SafeArea(
                child: Column(
                  children: [
                    Container(
                      width: width,
                      height: height / 14,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: height / 20,
                                decoration: BoxDecoration(
                                    color: Color(0xffF0F0F5),
                                    borderRadius: BorderRadius.circular(6)),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.search,
                                        color: Color(0xffAFB1BD),
                                      ),
                                      SizedBox(
                                        width: width / 32,
                                      ),
                                      AutoSizeText(
                                        (isLanguage != true)
                                            ? "Cari Produk"
                                            : "Search Product",
                                        style: GoogleFonts.poppins(
                                            textStyle: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xffAFB1BD))),
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: width / 48,),
                            GestureDetector(
                              onTap: (){
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: NotificationActivity()));
                              },
                              child: Stack(
                                children: [
                                  Icon(
                                    Icons.notifications_none,
                                    size: width / 14,
                                    color: Color(0xff707070),
                                  ),
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      width: width / 24,
                                      height: width / 24,
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle,
                                          border: Border.all(color: Colors.white)
                                      ),
                                      child: Center(
                                        child: AutoSizeText(
                                          "99",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 8,
                                                  fontWeight:
                                                  FontWeight.w500, color: Colors.white)),
                                          minFontSize: 0,
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(width: width / 48,),
                            GestureDetector(
                              onTap: (){
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: CartActivity()));
                              },
                              child: Stack(
                                  children: [
                                    Icon(
                                      Icons.shopping_cart,
                                      size: width / 14,
                                      color: Color(0xff707070),
                                    ),
                                    Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        width: width / 24,
                                        height: width / 24,
                                        decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle,
                                          border: Border.all(color: Colors.white)
                                        ),
                                        child: Center(
                                          child: AutoSizeText(
                                            "1",
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 8,
                                                    fontWeight:
                                                    FontWeight.w500, color: Colors.white)),
                                            minFontSize: 0,
                                            maxLines: 1,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              header: WaterDropMaterialHeader(
                color: Colors.grey,
                backgroundColor: Colors.white,
              ),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text("pull up load");
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text("Load Failed!Click retry!");
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("release to load more");
                  } else {
                    body = Text("No more Data");
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: width,
                          height: height / 12,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      "assets/image/image_referral.png"),
                                  fit: BoxFit.cover)),
                        ),
                        SizedBox(
                          height: height / 86,
                        ),
                        Stack(
                          children: [
                            CarouselSlider(
                              options: CarouselOptions(
                                  autoPlay: false,
                                  height: height / 3.5,
                                  scrollDirection: Axis.horizontal,
                                  enableInfiniteScroll: true,
                                  onScrolled: (v) {
                                    if (v.toString().split('.')[1] == "0") {
                                      setState(() {
                                        isPopupCarousel = true;
                                      });
                                    } else {
                                      setState(() {
                                        isPopupCarousel = false;
                                      });
                                    }
                                  },
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      _currentHeader = index;
                                    });
                                  }),
                              items: carouselHeader.map((e) {
                                return Container(
                                    width: width,
                                    decoration:
                                        BoxDecoration(color: Colors.pinkAccent),
                                    child: Center(child: SvgPicture.asset(e)));
                              }).toList(),
                            ),
                            Positioned(
                              left: 0,
                              child: AnimatedContainer(
                                duration: Duration(milliseconds: 200),
                                width: (isPopupCarousel) ? width / 8 : 0,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: Colors.greenAccent,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      bottomRight: Radius.circular(8)),
                                ),
                                child: Center(
                                    child: Text(
                                  "Text",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                              ),
                            ),
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: mapHeader<Widget>(
                                          carouselHeader, (index, url) {
                                        return AnimatedContainer(
                                          duration: Duration(milliseconds: 200),
                                          width: _currentHeader == index
                                              ? 12
                                              : 6.0,
                                          height: _currentHeader == index
                                              ? 12
                                              : 6.0,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10.0, horizontal: 2.0),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: _currentHeader == index
                                                ? Color(0xffC6AB74)
                                                : Colors.grey,
                                          ),
                                        );
                                      }),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: height / 86,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: AutoSizeText(
                            (isLanguage != true)
                                ? "Produk Diskon"
                                : "Product Discount",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          physics: BouncingScrollPhysics(),
                          child: Row(
                            children: [1, 2, 3, 4].map((e) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.rightToLeft,
                                          child: ProductDetailActivity()));
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Container(
                                    height: height / 2.4,
                                    width: width / 2.6,
                                    child: Column(
                                      children: [
                                        Stack(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 12),
                                              child: Container(
                                                height: height / 4.6,
                                                width: width / 2.6,
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            "assets/image/product.png"),
                                                        fit: BoxFit.cover),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                              ),
                                            ),
                                            Positioned(
                                              top: 0,
                                              left: 4,
                                              child: Container(
                                                height: height / 32,
                                                width: width / 8,
                                                decoration: BoxDecoration(
                                                    color: Color(0xffC6AB74),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            16)),
                                                child: Center(
                                                  child: AutoSizeText(
                                                    "-20%",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 11,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            color:
                                                                Colors.white)),
                                                    maxLines: 1,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              right: 0,
                                              top: 0,
                                              child: Container(
                                                color: Colors.transparent,
                                                child: Padding(
                                                  padding: EdgeInsets.all(
                                                      width.round() / 86),
                                                  child: Icon(
                                                    AntDesign.staro,
                                                    size: 18,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        Center(
                                          child: Container(
                                            height: height / 32,
                                            child: AutoSizeText(
                                              "Byoote",
                                              style: GoogleFonts.poppins(
                                                  textStyle: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                              minFontSize: 14,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: Container(
                                            height: height / 42,
                                            child: AutoSizeText(
                                              "Byoote gluthateon",
                                              style: GoogleFonts.poppins(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w400)),
                                              minFontSize: 0,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: Container(
                                            height: height / 32,
                                            child: AutoSizeText(
                                              "Rp 300.000",
                                              style: GoogleFonts.poppins(
                                                  textStyle: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                              minFontSize: 14,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            RatingBar(
                                              itemSize: 18,
                                              initialRating: ratingCount,
                                              ignoreGestures: true,
                                              minRating: 1,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Color(0xffC6AB74),
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                            SizedBox(
                                              width: 4,
                                            ),
                                            Text(ratingCount.toString(),
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 10,
                                                        color: Colors.grey)))
                                          ],
                                        ),
                                        SizedBox(
                                          height: height / 86,
                                        ),
                                        Container(
                                          height: height / 24,
                                          width: width / 2.6,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Color(0xffC6AB74)),
                                            borderRadius:
                                                BorderRadius.circular(4),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(vertical: height * 0.002),
                                            child: Center(
                                              child: AutoSizeText(
                                                "ADD TO BAG",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color:
                                                            Color(0xffC6AB74))),
                                                minFontSize: 0,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        Divider(
                          thickness: 4,
                        ),
                        SizedBox(
                          height: height / 86,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: AutoSizeText(
                            (isLanguage != true)
                                ? "Video Review Produk"
                                : "Video Review Product",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 4,
                              child: FutureBuilder(
                                future: _intializeVideoPlayerFuture,
                                builder: (context, snapshot) {
                                  return AspectRatio(
                                    aspectRatio: _controller.value.aspectRatio,
                                    child: VideoPlayer(_controller),
                                  );
                                  // if(snapshot.connectionState == ConnectionState.done){
                                  //
                                  // }else{
                                  //   return Center(
                                  //     child: CircularProgressIndicator(),
                                  //   );
                                  // }
                                },
                              ),
                            ),
                            (isPlay != true)
                                ? Container(
                                    width: MediaQuery.of(context).size.width,
                                    height:
                                        MediaQuery.of(context).size.height / 4,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "assets/image/image_product_beauty.png"),
                                            fit: BoxFit.cover)),
                                    child: Center(
                                      child: GestureDetector(
                                          onTap: () {
                                            // setState(() {
                                            //   isPlay = true;
                                            // });
                                            // _controller.initialize();
                                            // _controller.play();
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType.rightToLeft,
                                                    child: Test()));
                                          },
                                          child: Center(
                                              child: Container(
                                                width: width / 4,
                                                height: width / 4,
                                                child: SvgPicture.asset("assets/image/icon_play.svg"),
                                              ))),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(height: height / 86,),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: AutoSizeText(
                            (isLanguage != true)
                                ? "Review Produk Terbaru Byoote"
                                : "Video Review Product",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ),
                        SizedBox(
                          height: height / 86,
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          physics: BouncingScrollPhysics(),
                          child: Row(
                            children: [1, 2, 3].map((e) {
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 4),
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.rightToLeft,
                                            child: Test()));
                                  },
                                  child: Column(
                                    children: [
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                            width: width / 2.1,
                                            height: height / 8,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      "assets/image/image_product_beauty.png"),
                                                  fit: BoxFit.cover),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.bottomCenter,
                                            child: Container(
                                              width: width / 7,
                                              height: width / 7,
                                              child: SvgPicture.asset("assets/image/icon_play.svg"),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        width: width / 2.2,
                                        child: AutoSizeText(
                                          "REVIEW JUJUR BYOOTE BY PAO PAO LDP!",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                                          maxLines: 2,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        SizedBox(
                          height: height / 86,
                        ),
                        Divider(
                          thickness: 4,
                        ),
                        SizedBox(
                          height: height / 86,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              child: Row(
                                children: [1, 2, 3, 4].map((e) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              child: ProductDetailActivity()));
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8),
                                      child: Container(
                                        height: height / 2.4,
                                        width: width / 2.6,
                                        child: Column(
                                          children: [
                                            Stack(
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 12),
                                                  child: Container(
                                                    height: height / 4.6,
                                                    width: width / 2.6,
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                            image: AssetImage(
                                                                "assets/image/product.png"),
                                                            fit: BoxFit.cover),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8)),
                                                  ),
                                                ),
                                                Positioned(
                                                  right: 0,
                                                  top: 0,
                                                  child: Container(
                                                    color: Colors.transparent,
                                                    child: Padding(
                                                      padding: EdgeInsets.all(
                                                          width.round() / 86),
                                                      child: Icon(
                                                        AntDesign.staro,
                                                        size: 18,
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                            Center(
                                              child: Container(
                                                height: height / 32,
                                                child: AutoSizeText(
                                                  "Byoote",
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                          FontWeight.w500)),
                                                  minFontSize: 14,
                                                  maxLines: 1,
                                                ),
                                              ),
                                            ),
                                            Center(
                                              child: Container(
                                                height: height / 42,
                                                child: AutoSizeText(
                                                  "Byoote gluthateon",
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight:
                                                          FontWeight.w400)),
                                                  minFontSize: 0,
                                                  maxLines: 1,
                                                ),
                                              ),
                                            ),
                                            Center(
                                              child: Container(
                                                height: height / 32,
                                                child: AutoSizeText(
                                                  "Rp 300.000",
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                          FontWeight.w500)),
                                                  minFontSize: 14,
                                                  maxLines: 1,
                                                ),
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                RatingBar(
                                                  itemSize: 18,
                                                  initialRating: ratingCount,
                                                  ignoreGestures: true,
                                                  minRating: 1,
                                                  direction: Axis.horizontal,
                                                  allowHalfRating: true,
                                                  itemCount: 5,
                                                  itemBuilder: (context, _) =>
                                                      Icon(
                                                    Icons.star,
                                                    color: Color(0xffC6AB74),
                                                  ),
                                                  onRatingUpdate: (rating) {
                                                    print(rating);
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Text(ratingCount.toString(),
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 10,
                                                            color:
                                                                Colors.grey)))
                                              ],
                                            ),
                                            SizedBox(
                                              height: height / 86,
                                            ),
                                            Container(
                                              height: height / 24,
                                              width: width / 2.6,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Color(0xffC6AB74)),
                                                borderRadius:
                                                    BorderRadius.circular(4),
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(vertical: height * 0.002),
                                                child: Center(
                                                  child: AutoSizeText(
                                                    "ADD TO BAG",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Color(
                                                                0xffC6AB74))),
                                                    minFontSize: 0,
                                                    maxLines: 1,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            SizedBox(
                              height: height / 86,
                            ),
                            Center(
                              child: Container(
                                height: height / 24,
                                width: width / 2,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Center(
                                  child: AutoSizeText(
                                    "Lihat Semua",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black)),
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Divider(
                              thickness: 4,
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: AutoSizeText(
                                (isLanguage != true)
                                    ? "Secret Artikel"
                                    : "Video Review Product",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ),
                            SizedBox(
                              height: height / 86,
                            ),
                            Container(
                              width: width,
                              height: height / 3.4,
                              child: GridView.count(
                                scrollDirection: Axis.horizontal,
                                physics: BouncingScrollPhysics(),
                                crossAxisCount: 2,
                                childAspectRatio: 2 / 5.4,
                                children:
                                    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((e) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              child: ArticleDetailActivity()));
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width.round() / 86),
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.symmetric(vertical: 2),
                                            child: Container(
                                              width: width / 4,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(12),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          "assets/image/image_product_beauty.png"),
                                                      fit: BoxFit.cover
                                                  )
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: width / 86,
                                          ),
                                          Container(
                                            width: width / 4,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  height: height / 38,
                                                  child: AutoSizeText(
                                                    (isLanguage != true)
                                                        ? "Kecantikan"
                                                        : "Beauty",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color: Color(
                                                                0xff907135))),
                                                    maxLines: 1,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: height / 86,
                                                ),
                                                Container(
                                                  height: height / 17,
                                                  child: AutoSizeText(
                                                    (isLanguage != true)
                                                        ? "5 Produk The Ordinary yang Cocok Bagi Pemilik Kulit Keriput"
                                                        : "Beauty",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.w400)),
                                                    maxLines: 2,
                                                  ),
                                                ),
                                                Container(
                                                  height: height / 46,
                                                  child: AutoSizeText(
                                                    "11 Oktober 2020",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Colors.grey)),
                                                    maxLines: 1,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Divider(
                              thickness: 4,
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: AutoSizeText(
                                (isLanguage != true)
                                    ? "Top Referral"
                                    : "Top Referral",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ),
                            SizedBox(
                              height: height / 86,
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              child: Row(
                                children: [1, 2, 3].map((e) {
                                  return GestureDetector(
                                    onTap: () {},
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8),
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                              width: width / 2.4,
                                              height: height / 4,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: AssetImage("assets/image/bg_referral.png"),
                                                  fit: BoxFit.fill
                                                )
                                              ),),
                                          Column(
                                            children: [
                                              CircularProfileAvatar(
                                                '',
                                                radius: height * 0.04,
                                                borderColor: Colors.white,
                                                // borderWidth: height * 0.008,
                                                cacheImage: true,
                                                child: SvgPicture.asset(
                                                    "assets/image/image_profile.svg"),
                                                // child: Container(
                                                //   decoration: BoxDecoration(
                                                //     color: Colors.amber
                                                //   ),
                                                // ),
                                              ),
                                              SizedBox(
                                                height: height / 86,
                                              ),
                                              AutoSizeText(
                                                "Deni",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400)),
                                                maxLines: 1,
                                              ),
                                              AutoSizeText(
                                                "@denidc",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 11,
                                                        fontWeight:
                                                            FontWeight.w300)),
                                                maxLines: 1,
                                              ),
                                              SizedBox(
                                                height: height / 86,
                                              ),
                                              Container(
                                                width: width / 4,
                                                height: height / 38,
                                                decoration: BoxDecoration(
                                                    color: Colors.white),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    AutoSizeText(
                                                      "100",
                                                      style: GoogleFonts.poppins(
                                                          textStyle: TextStyle(
                                                              fontSize: 11,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600)),
                                                      maxLines: 1,
                                                    ),
                                                    SizedBox(
                                                      width: width / 86,
                                                    ),
                                                    AutoSizeText(
                                                      "Refr",
                                                      style: GoogleFonts.poppins(
                                                          textStyle: TextStyle(
                                                              fontSize: 11,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      maxLines: 1,
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Divider(
                              thickness: 4,
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: AutoSizeText(
                                (isLanguage != true)
                                    ? "Produk Review Populer"
                                    : "Top Referral",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ),
                            SizedBox(
                              height: height / 86,
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              child: Row(
                                children: [1, 2, 3, 4].map((e) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                    child: Container(
                                      height: height / 2.5,
                                      width: width / 2.6,
                                      child: Column(
                                        children: [
                                          Stack(
                                            children: [
                                              Container(
                                                height: height / 4.6,
                                                width: width / 2.6,
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            "assets/image/product.png"),
                                                        fit: BoxFit.cover),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                              ),
                                            ],
                                          ),
                                          Center(
                                            child: Container(
                                              height: height / 32,
                                              child: AutoSizeText(
                                                "Byoote",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                        FontWeight.w500)),
                                                minFontSize: 14,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ),
                                          Center(
                                            child: Container(
                                              height: height / 42,
                                              child: AutoSizeText(
                                                "Byoote gluthateon",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                        FontWeight.w400)),
                                                minFontSize: 0,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ),
                                          Center(
                                            child: Container(
                                              height: height / 32,
                                              child: AutoSizeText(
                                                "Rp 300.000",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                        FontWeight.w500)),
                                                minFontSize: 14,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              RatingBar(
                                                itemSize: 18,
                                                initialRating: ratingCount,
                                                ignoreGestures: true,
                                                minRating: 1,
                                                direction: Axis.horizontal,
                                                allowHalfRating: true,
                                                itemCount: 5,
                                                itemBuilder: (context, _) =>
                                                    Icon(
                                                  Icons.star,
                                                  color: Color(0xffC6AB74),
                                                ),
                                                onRatingUpdate: (rating) {
                                                  print(rating);
                                                },
                                              ),
                                              SizedBox(
                                                width: 4,
                                              ),
                                              Text(ratingCount.toString(),
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                          fontSize: 10,
                                                          color: Colors.grey)))
                                            ],
                                          ),
                                          SizedBox(
                                            height: height / 86,
                                          ),
                                          Container(
                                            height: height / 24,
                                            width: width / 2.6,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xffC6AB74)),
                                              borderRadius:
                                                  BorderRadius.circular(4),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(vertical: height * 0.002),
                                              child: Center(
                                                child: AutoSizeText(
                                                  "ADD TO BAG",
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Color(
                                                              0xffC6AB74))),
                                                  maxLines: 1,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            Divider(
                              thickness: 4,
                            ),
                            SizedBox(
                              height: height / 72,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: AutoSizeText(
                                (isLanguage != true)
                                    ? "Recently Reviewed"
                                    : "Top Referral",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ),
                            SizedBox(
                              height: height / 86,
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              child: Row(
                                children: [1, 2, 3].map((e) {
                                  return Container(
                                    width: width / 1.2,
                                    height: height / 3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              width: width / 4,
                                              height: height / 8,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(12),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          "assets/image/image_product_beauty.png"))),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: height / 34,
                                                  child: AutoSizeText(
                                                    "Byoote",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight.w500)),
                                                    minFontSize: 14,
                                                    maxLines: 1,
                                                  ),
                                                ),
                                                Container(
                                                  height: height / 38,
                                                  child: AutoSizeText(
                                                    "Byoote gluthateon",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w400)),
                                                    minFontSize: 0,
                                                    maxLines: 1,
                                                  ),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    RatingBar(
                                                      itemSize: 16,
                                                      initialRating:
                                                          ratingCount,
                                                      ignoreGestures: true,
                                                      minRating: 1,
                                                      direction:
                                                          Axis.horizontal,
                                                      allowHalfRating: true,
                                                      itemCount: 5,
                                                      itemBuilder:
                                                          (context, _) => Icon(
                                                        Icons.star,
                                                        color:
                                                            Color(0xffC6AB74),
                                                      ),
                                                      onRatingUpdate: (rating) {
                                                        print(rating);
                                                      },
                                                    ),
                                                    SizedBox(
                                                      width: 4,
                                                    ),
                                                    Text(ratingCount.toString(),
                                                        style: GoogleFonts.poppins(
                                                            textStyle: TextStyle(
                                                                fontSize: 10,
                                                                color: Colors
                                                                    .grey)))
                                                  ],
                                                ),
                                                Container(
                                                  height: height / 42,
                                                  child: AutoSizeText(
                                                    "300 reviews",
                                                    style: GoogleFonts.poppins(
                                                        textStyle: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Colors.grey)),
                                                    minFontSize: 0,
                                                    maxLines: 1,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(width.round() / 86),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                height: height / 14,
                                                child: AutoSizeText(
                                                  "sbelumnya sempet ragu juga mau minum2an kolagen ky gini sbenernya beneran ngefek apa nggak sih..?? terjawab sm pnjelasan dokter ini",
                                                  style: GoogleFonts.poppins(
                                                      textStyle: TextStyle(
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Colors.grey)),
                                                  minFontSize: 10,
                                                  maxLines: 4,
                                                ),
                                              ),
                                              AutoSizeText(
                                                "Tampilkan lebih banyak",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color:
                                                            Color(0xffC6AB74))),
                                                maxLines: 4,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(width.round() / 86),
                                          child: Row(
                                            children: [
                                              CircularProfileAvatar(
                                                '',
                                                radius: height * 0.03,
                                                borderColor: Colors.white,
                                                // borderWidth: height * 0.008,
                                                cacheImage: true,
                                                child: SvgPicture.asset(
                                                    "assets/image/image_profile.svg"),
                                                // child: Container(
                                                //   decoration: BoxDecoration(
                                                //     color: Colors.amber
                                                //   ),
                                                // ),
                                              ),
                                              AutoSizeText(
                                                "Deni Dwi Candra",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xffC6AB74))),
                                                maxLines: 1,
                                              ),
                                              AutoSizeText(
                                                " 2 menit",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: Colors.grey)),
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    // TabBar(
                    //   controller: _tabController,
                    //   indicatorColor: Color(0xffC6AB74),
                    //   unselectedLabelColor: Colors.grey,
                    //   labelColor: Colors.black,
                    //   onTap: (v){
                    //     setState(() {
                    //       tabPage = v;
                    //     });
                    //   },
                    //   tabs: [
                    //     Tab(child: Row(
                    //       mainAxisSize: MainAxisSize.min,
                    //       children: [
                    //         Container(
                    //           width: width / 12,
                    //           height: height / 18,
                    //           decoration: BoxDecoration(
                    //               image: DecorationImage(
                    //                 image: AssetImage((tabPage != 1)?
                    //                 "assets/image/icon_enable_beauty.png":
                    //                 "assets/image/icon_disable_beauty.png"),
                    //               )
                    //           ),
                    //         ),
                    //         SizedBox(width: width / 72,),
                    //         AutoSizeText(
                    //           (isLanguage != true)?"Kecantikan":"Beauty",
                    //           style: GoogleFonts.poppins(textStyle:
                    //           TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //           maxLines: 1,
                    //         ),
                    //       ],
                    //     ),),
                    //     Tab(child:
                    //     Row(
                    //       mainAxisSize: MainAxisSize.min,
                    //       children: [
                    //         Container(
                    //           width: width / 12,
                    //           height: height / 18,
                    //           decoration: BoxDecoration(
                    //               image: DecorationImage(
                    //                 image: AssetImage((tabPage != 0)?
                    //                 "assets/image/icon_enable_health.png":
                    //                 "assets/image/icon_disable_health.png"),
                    //               )
                    //           ),
                    //         ),
                    //         SizedBox(width: width / 72,),
                    //         AutoSizeText(
                    //           (isLanguage != true)?"Kesehatan":"Health",
                    //           style: GoogleFonts.poppins(textStyle:
                    //           TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //           maxLines: 1,
                    //         ),
                    //       ],
                    //     )),
                    //   ],
                    // ),
                    // TabBarView(
                    //     controller: _tabController,
                    //     children: [
                    //       Container(
                    //         height: height,
                    //         width: width,
                    //         child: SingleChildScrollView(
                    //           controller: _scrollViewController,
                    //           child: Column(
                    //             crossAxisAlignment: CrossAxisAlignment.start,
                    //             children: [
                    //               SizedBox(height: height / 72,),
                    //               SingleChildScrollView(
                    //                 scrollDirection: Axis.horizontal,
                    //                 physics: BouncingScrollPhysics(),
                    //                 child: Row(
                    //                   children: [
                    //                     1,2,3,4
                    //                   ].map((e) {
                    //                     return Padding(
                    //                       padding: const EdgeInsets.symmetric(horizontal: 8),
                    //                       child: Container(
                    //                         height: height / 2.3,
                    //                         width: width / 2.6,
                    //                         child: Column(
                    //                           children: [
                    //                             Stack(
                    //                               children: [
                    //                                 Padding(
                    //                                   padding: const EdgeInsets.only(top: 12),
                    //                                   child: Container(
                    //                                     height: height / 4.2,
                    //                                     width: width / 2.6,
                    //                                     decoration: BoxDecoration(
                    //                                         image: DecorationImage(
                    //                                             image: AssetImage("assets/image/image_product_beauty.png"),
                    //                                             fit: BoxFit.cover
                    //                                         ),
                    //                                         borderRadius: BorderRadius.circular(8)
                    //                                     ),
                    //                                   ),
                    //                                 ),
                    //                                 Positioned(
                    //                                   right: 0,
                    //                                   top: 0,
                    //                                   child: Container(
                    //                                     decoration: BoxDecoration(
                    //                                       color: Colors.white,
                    //                                       shape: BoxShape.circle,
                    //                                       boxShadow: [
                    //                                         BoxShadow(
                    //                                           color: Colors.grey.withOpacity(0.5),
                    //                                           spreadRadius: 2,
                    //                                           blurRadius: 5,
                    //                                           offset: Offset(0, 3), // changes position of shadow
                    //                                         ),
                    //                                       ],
                    //                                     ),
                    //                                     child: Icon(Icons.star_border, size: 32,
                    //                                       color: Colors.grey,),
                    //                                   ),
                    //                                 )
                    //                               ],
                    //                             ),
                    //                             Center(
                    //                               child: AutoSizeText(
                    //                                 "Byoote",
                    //                                 style: GoogleFonts.poppins(textStyle:
                    //                                 TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                                 maxLines: 1,
                    //                               ),
                    //                             ),
                    //                             Center(
                    //                               child: AutoSizeText(
                    //                                 "Byoote gluthateon",
                    //                                 style: GoogleFonts.poppins(textStyle:
                    //                                 TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //                                 maxLines: 1,
                    //                               ),
                    //                             ),
                    //                             Center(
                    //                               child: AutoSizeText(
                    //                                 "Rp 300.000",
                    //                                 style: GoogleFonts.poppins(textStyle:
                    //                                 TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                                 maxLines: 1,
                    //                               ),
                    //                             ),
                    //                             Row(
                    //                               mainAxisAlignment: MainAxisAlignment.center,
                    //                               children: <Widget>[
                    //                                 RatingBar(
                    //                                   itemSize: 18,
                    //                                   initialRating: ratingCount,
                    //                                   ignoreGestures: true,
                    //                                   minRating: 1,
                    //                                   direction: Axis.horizontal,
                    //                                   allowHalfRating: true,
                    //                                   itemCount: 5,
                    //                                   itemBuilder: (context, _) => Icon(
                    //                                     Icons.star,
                    //                                     color: Color(0xffC6AB74),
                    //                                   ),
                    //                                   onRatingUpdate: (rating) {
                    //                                     print(rating);
                    //                                   },
                    //                                 ),
                    //                                 SizedBox(width: 4,),
                    //                                 Text(ratingCount.toString(), style: GoogleFonts.poppins(
                    //                                     textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                    //                               ],
                    //                             ),
                    //                             SizedBox(height: height / 86,),
                    //                             Container(
                    //                               height: height / 32,
                    //                               width: width / 2.6,
                    //                               decoration: BoxDecoration(
                    //                                 border: Border.all(color: Color(0xffC6AB74)),
                    //                                 borderRadius: BorderRadius.circular(4),
                    //                               ),
                    //                               child: Center(
                    //                                 child: AutoSizeText(
                    //                                   "Tambah Keranjang",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                    //                                       color: Color(0xffC6AB74))),
                    //                                   maxLines: 1,
                    //                                 ),
                    //                               ),
                    //                             ),
                    //                           ],
                    //                         ),
                    //                       ),
                    //                     );
                    //                   }).toList(),
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 86,),
                    //               Center(
                    //                 child: Container(
                    //                   height: height / 24,
                    //                   width: width / 2,
                    //                   decoration: BoxDecoration(
                    //                     border: Border.all(color: Colors.black),
                    //                     borderRadius: BorderRadius.circular(4),
                    //                   ),
                    //                   child: Center(
                    //                     child: AutoSizeText(
                    //                       "Lihat Semua",
                    //                       style: GoogleFonts.poppins(textStyle:
                    //                       TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                    //                           color: Colors.black)),
                    //                       maxLines: 1,
                    //                     ),
                    //                   ),
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 72,),
                    //               Divider(thickness: 6,),
                    //               SizedBox(height: height / 72,),
                    //               Padding(
                    //                 padding: const EdgeInsets.symmetric(horizontal: 8),
                    //                 child: AutoSizeText(
                    //                   (isLanguage != true)?"Secret Artikel":"Video Review Product",
                    //                   style: GoogleFonts.poppins(textStyle:
                    //                   TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                   maxLines: 1,
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 86,),
                    //               Container(
                    //                 width: width,
                    //                 height: height / 3.4,
                    //                 child: GridView.count(
                    //                   scrollDirection: Axis.horizontal,
                    //                   physics: BouncingScrollPhysics(),
                    //                   crossAxisCount: 2,
                    //                   childAspectRatio: 2 / 6,
                    //                   children: [
                    //                     1,2,3,4,5,6,7,8,9,10
                    //                   ].map((e) {
                    //                     return Padding(
                    //                       padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                    //                       child: Row(
                    //                         children: [
                    //                           Container(
                    //                             width: width / 4,
                    //                             decoration: BoxDecoration(
                    //                                 borderRadius: BorderRadius.circular(12),
                    //                                 image: DecorationImage(
                    //                                     image: AssetImage("assets/image/image_product_beauty.png")
                    //                                 )
                    //                             ),
                    //                           ),
                    //                           SizedBox(width: width / 86,),
                    //                           Container(
                    //                             width: width / 2.8,
                    //                             child: Column(
                    //                               crossAxisAlignment: CrossAxisAlignment.start,
                    //                               mainAxisAlignment: MainAxisAlignment.center,
                    //                               children: [
                    //                                 AutoSizeText(
                    //                                   (isLanguage != true)?"Kecantikan":"Beauty",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Color(0xff907135))),
                    //                                   maxLines: 1,
                    //                                 ),
                    //                                 SizedBox(height: height / 86,),
                    //                                 AutoSizeText(
                    //                                   (isLanguage != true)?"5 Produk The Ordinary yang Cocok Bagi Pemilik Kulit Keriput":"Beauty",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //                                   maxLines: 2,
                    //                                 ),
                    //                                 AutoSizeText(
                    //                                   "11 Oktober 2020",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                    //                                   maxLines: 1,
                    //                                 ),
                    //                               ],
                    //                             ),
                    //                           )
                    //                         ],
                    //                       ),
                    //                     );
                    //                   }).toList(),
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 72,),
                    //               Divider(thickness: 6,),
                    //               SizedBox(height: height / 72,),
                    //               Padding(
                    //                 padding: const EdgeInsets.symmetric(horizontal: 8),
                    //                 child: AutoSizeText(
                    //                   (isLanguage != true)?"Top Referral":"Top Referral",
                    //                   style: GoogleFonts.poppins(textStyle:
                    //                   TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                   maxLines: 1,
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 86,),
                    //               SingleChildScrollView(
                    //                 scrollDirection: Axis.horizontal,
                    //                 physics: BouncingScrollPhysics(),
                    //                 child: Row(
                    //                   children: [1,2,3].map((e) {
                    //                     return Stack(
                    //                       alignment: Alignment.center,
                    //                       children: [
                    //                         Container(
                    //                             width: width / 2.4,
                    //                             height: height / 4,
                    //                             child: Image.asset("assets/image/bg_referral.png")
                    //                         ),
                    //                         Column(
                    //                           children: [
                    //                             CircularProfileAvatar(
                    //                               '',
                    //                               radius: height * 0.04,
                    //                               borderColor: Colors.white,
                    //                               // borderWidth: height * 0.008,
                    //                               cacheImage: true,
                    //                               child: SvgPicture.asset("assets/image/image_profile.svg"),
                    //                               // child: Container(
                    //                               //   decoration: BoxDecoration(
                    //                               //     color: Colors.amber
                    //                               //   ),
                    //                               // ),
                    //                             ),
                    //                             SizedBox(height: height / 86,),
                    //                             AutoSizeText(
                    //                               "Deni",
                    //                               style: GoogleFonts.poppins(textStyle:
                    //                               TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //                               maxLines: 1,
                    //                             ),
                    //                             AutoSizeText(
                    //                               "@denidc",
                    //                               style: GoogleFonts.poppins(textStyle:
                    //                               TextStyle(fontSize: 11, fontWeight: FontWeight.w300)),
                    //                               maxLines: 1,
                    //                             ),
                    //                             SizedBox(height: height / 86,),
                    //                             Container(
                    //                               width: width / 4,
                    //                               height: height / 38,
                    //                               decoration: BoxDecoration(
                    //                                   color: Colors.white
                    //                               ),
                    //                               child: Row(
                    //                                 mainAxisAlignment: MainAxisAlignment.center,
                    //                                 children: [
                    //                                   AutoSizeText(
                    //                                     "100",
                    //                                     style: GoogleFonts.poppins(textStyle:
                    //                                     TextStyle(fontSize: 11, fontWeight:
                    //                                     FontWeight.w600)),
                    //                                     maxLines: 1,
                    //                                   ),
                    //                                   SizedBox(width: width / 86,),
                    //                                   AutoSizeText(
                    //                                     "Refr",
                    //                                     style: GoogleFonts.poppins(textStyle:
                    //                                     TextStyle(fontSize: 11, fontWeight:
                    //                                     FontWeight.w300)),
                    //                                     maxLines: 1,
                    //                                   ),
                    //                                 ],
                    //                               ),
                    //                             )
                    //                           ],
                    //                         ),
                    //                       ],
                    //                     );
                    //                   }).toList(),
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 72,),
                    //               Divider(thickness: 6,),
                    //               SizedBox(height: height / 72,),
                    //               Padding(
                    //                 padding: const EdgeInsets.symmetric(horizontal: 8),
                    //                 child: AutoSizeText(
                    //                   (isLanguage != true)?"Produk Review Populer":"Top Referral",
                    //                   style: GoogleFonts.poppins(textStyle:
                    //                   TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                   maxLines: 1,
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 86,),
                    //               SingleChildScrollView(
                    //                 scrollDirection: Axis.horizontal,
                    //                 physics: BouncingScrollPhysics(),
                    //                 child: Row(
                    //                   children: [
                    //                     1,2,3,4
                    //                   ].map((e) {
                    //                     return Padding(
                    //                       padding: const EdgeInsets.symmetric(horizontal: 8),
                    //                       child: Container(
                    //                         height: height / 2.3,
                    //                         width: width / 2.6,
                    //                         child: Column(
                    //                           children: [
                    //                             Stack(
                    //                               children: [
                    //                                 Container(
                    //                                   height: height / 4.2,
                    //                                   width: width / 2.6,
                    //                                   decoration: BoxDecoration(
                    //                                       image: DecorationImage(
                    //                                           image: AssetImage("assets/image/image_product_beauty.png"),
                    //                                           fit: BoxFit.cover
                    //                                       ),
                    //                                       borderRadius: BorderRadius.circular(8)
                    //                                   ),
                    //                                 ),
                    //                               ],
                    //                             ),
                    //                             Center(
                    //                               child: AutoSizeText(
                    //                                 "Byoote",
                    //                                 style: GoogleFonts.poppins(textStyle:
                    //                                 TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                                 maxLines: 1,
                    //                               ),
                    //                             ),
                    //                             Center(
                    //                               child: AutoSizeText(
                    //                                 "Byoote gluthateon",
                    //                                 style: GoogleFonts.poppins(textStyle:
                    //                                 TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //                                 maxLines: 1,
                    //                               ),
                    //                             ),
                    //                             Center(
                    //                               child: AutoSizeText(
                    //                                 "Rp 300.000",
                    //                                 style: GoogleFonts.poppins(textStyle:
                    //                                 TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                                 maxLines: 1,
                    //                               ),
                    //                             ),
                    //                             Row(
                    //                               mainAxisAlignment: MainAxisAlignment.center,
                    //                               children: <Widget>[
                    //                                 RatingBar(
                    //                                   itemSize: 18,
                    //                                   initialRating: ratingCount,
                    //                                   ignoreGestures: true,
                    //                                   minRating: 1,
                    //                                   direction: Axis.horizontal,
                    //                                   allowHalfRating: true,
                    //                                   itemCount: 5,
                    //                                   itemBuilder: (context, _) => Icon(
                    //                                     Icons.star,
                    //                                     color: Color(0xffC6AB74),
                    //                                   ),
                    //                                   onRatingUpdate: (rating) {
                    //                                     print(rating);
                    //                                   },
                    //                                 ),
                    //                                 SizedBox(width: 4,),
                    //                                 Text(ratingCount.toString(), style: GoogleFonts.poppins(
                    //                                     textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                    //                               ],
                    //                             ),
                    //                             SizedBox(height: height / 86,),
                    //                             Container(
                    //                               height: height / 32,
                    //                               width: width / 2.6,
                    //                               decoration: BoxDecoration(
                    //                                 border: Border.all(color: Color(0xffC6AB74)),
                    //                                 borderRadius: BorderRadius.circular(4),
                    //                               ),
                    //                               child: Center(
                    //                                 child: AutoSizeText(
                    //                                   "Tambah Keranjang",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                    //                                       color: Color(0xffC6AB74))),
                    //                                   maxLines: 1,
                    //                                 ),
                    //                               ),
                    //                             ),
                    //                           ],
                    //                         ),
                    //                       ),
                    //                     );
                    //                   }).toList(),
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 72,),
                    //               Divider(thickness: 6,),
                    //               SizedBox(height: height / 72,),
                    //               Padding(
                    //                 padding: const EdgeInsets.symmetric(horizontal: 8),
                    //                 child: AutoSizeText(
                    //                   (isLanguage != true)?"Recently Reviewed":"Top Referral",
                    //                   style: GoogleFonts.poppins(textStyle:
                    //                   TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                   maxLines: 1,
                    //                 ),
                    //               ),
                    //               SizedBox(height: height / 86,),
                    //               SingleChildScrollView(
                    //                 scrollDirection: Axis.horizontal,
                    //                 physics: BouncingScrollPhysics(),
                    //                 child: Row(
                    //                   children: [
                    //                     1,2,3
                    //                   ].map((e) {
                    //                     return Container(
                    //                       width: width / 1.2,
                    //                       height: height / 2.9,
                    //                       child: Column(
                    //                         crossAxisAlignment: CrossAxisAlignment.start,
                    //                         children: [
                    //                           Row(
                    //                             children: [
                    //                               Container(
                    //                                 width: width / 4,
                    //                                 height: height / 8,
                    //                                 decoration: BoxDecoration(
                    //                                     borderRadius: BorderRadius.circular(12),
                    //                                     image: DecorationImage(
                    //                                         image: AssetImage("assets/image/image_product_beauty.png")
                    //                                     )
                    //                                 ),
                    //                               ),
                    //                               Column(
                    //                                 crossAxisAlignment: CrossAxisAlignment.start,
                    //                                 children: [
                    //                                   AutoSizeText(
                    //                                     "Byoote",
                    //                                     style: GoogleFonts.poppins(textStyle:
                    //                                     TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                                     maxLines: 1,
                    //                                   ),
                    //                                   AutoSizeText(
                    //                                     "Byoote gluthateon",
                    //                                     style: GoogleFonts.poppins(textStyle:
                    //                                     TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //                                     maxLines: 1,
                    //                                   ),
                    //                                   Row(
                    //                                     mainAxisAlignment: MainAxisAlignment.center,
                    //                                     children: <Widget>[
                    //                                       RatingBar(
                    //                                         itemSize: 18,
                    //                                         initialRating: ratingCount,
                    //                                         ignoreGestures: true,
                    //                                         minRating: 1,
                    //                                         direction: Axis.horizontal,
                    //                                         allowHalfRating: true,
                    //                                         itemCount: 5,
                    //                                         itemBuilder: (context, _) => Icon(
                    //                                           Icons.star,
                    //                                           color: Color(0xffC6AB74),
                    //                                         ),
                    //                                         onRatingUpdate: (rating) {
                    //                                           print(rating);
                    //                                         },
                    //                                       ),
                    //                                       SizedBox(width: 4,),
                    //                                       Text(ratingCount.toString(), style: GoogleFonts.poppins(
                    //                                           textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                    //                                     ],
                    //                                   ),
                    //                                   AutoSizeText(
                    //                                     "300 reviews",
                    //                                     style: GoogleFonts.poppins(textStyle:
                    //                                     TextStyle(fontSize: 12, fontWeight:
                    //                                     FontWeight.w400, color: Colors.grey)),
                    //                                     maxLines: 1,
                    //                                   ),
                    //                                 ],
                    //                               ),
                    //                             ],
                    //                           ),
                    //                           Padding(
                    //                             padding: const EdgeInsets.all(8.0),
                    //                             child: Column(
                    //                               crossAxisAlignment: CrossAxisAlignment.start,
                    //                               children: [
                    //                                 AutoSizeText(
                    //                                   "sbelumnya sempet ragu juga mau minum2an kolagen ky gini sbenernya beneran ngefek apa nggak sih..?? terjawab sm pnjelasan dokter ini",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 12, fontWeight:
                    //                                   FontWeight.w400, color: Colors.grey)),
                    //                                   maxLines: 4,
                    //                                 ),
                    //                                 AutoSizeText(
                    //                                   "Tampilkan lebih banyak",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 12, fontWeight:
                    //                                   FontWeight.w400, color: Color(0xffC6AB74))),
                    //                                   maxLines: 4,
                    //                                 ),
                    //                               ],
                    //                             ),
                    //                           ),
                    //                           Padding(
                    //                             padding: const EdgeInsets.all(8.0),
                    //                             child: Row(
                    //                               children: [
                    //                                 CircularProfileAvatar(
                    //                                   '',
                    //                                   radius: height * 0.03,
                    //                                   borderColor: Colors.white,
                    //                                   // borderWidth: height * 0.008,
                    //                                   cacheImage: true,
                    //                                   child: SvgPicture.asset("assets/image/image_profile.svg"),
                    //                                   // child: Container(
                    //                                   //   decoration: BoxDecoration(
                    //                                   //     color: Colors.amber
                    //                                   //   ),
                    //                                   // ),
                    //                                 ),
                    //                                 AutoSizeText(
                    //                                   "Deni Dwi Candra",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 12, fontWeight:
                    //                                   FontWeight.w600, color: Color(0xffC6AB74))),
                    //                                   maxLines: 1,
                    //                                 ),
                    //                                 AutoSizeText(
                    //                                   " 2 menit",
                    //                                   style: GoogleFonts.poppins(textStyle:
                    //                                   TextStyle(fontSize: 12, fontWeight:
                    //                                   FontWeight.w400, color: Colors.grey)),
                    //                                   maxLines: 1,
                    //                                 ),
                    //                               ],
                    //                             ),
                    //                           ),
                    //                         ],
                    //                       ),
                    //                     );
                    //                   }).toList(),
                    //                 ),
                    //               ),
                    //             ],
                    //           ),
                    //         ),
                    //       ),
                    //       new SafeArea(
                    //         top: false,
                    //         bottom: false,
                    //         child: Builder(
                    //           builder: (BuildContext context) {
                    //             return CustomScrollView(
                    //               slivers: <Widget>[
                    //                 SliverPadding(
                    //                   padding: const EdgeInsets.all(8.0),
                    //                   sliver: SliverList(
                    //                     delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                    //                       return Column(
                    //                         children: <Widget>[
                    //                           Container(
                    //                             height: 90,
                    //                             width: double.infinity,
                    //                             color: Colors.blueGrey,
                    //                             child: Column(
                    //                               mainAxisAlignment:
                    //                               MainAxisAlignment.center,
                    //                               children: <Widget>[
                    //                                 Text('$index'),
                    //                               ],
                    //                             ),
                    //                           ),
                    //                           SizedBox(height: 10),
                    //                         ],
                    //                       );
                    //                     },
                    //                       childCount: 10,
                    //                     ),
                    //                   ),
                    //                 ),
                    //               ],
                    //             );
                    //           },
                    //         ),
                    //       ),
                    //     ]
                    // ),
                  ],
                ),
              ),
            ),
          );
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({Key key, this.controller}) : super(key: key);

  static const _examplePlaybackRates = [
    0.25,
    0.5,
    1.0,
    1.5,
    2.0,
    3.0,
    5.0,
    10.0,
  ];

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
        Align(
          alignment: Alignment.topRight,
          child: PopupMenuButton<double>(
            initialValue: controller.value.playbackSpeed,
            tooltip: 'Playback speed',
            onSelected: (speed) {
              controller.setPlaybackSpeed(speed);
            },
            itemBuilder: (context) {
              return [
                for (final speed in _examplePlaybackRates)
                  PopupMenuItem(
                    value: speed,
                    child: Text('${speed}x'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                // Using less vertical padding as the text is also longer
                // horizontally, so it feels like it would need more spacing
                // horizontally (matching the aspect ratio of the video).
                vertical: 12,
                horizontal: 16,
              ),
              child: Text('${controller.value.playbackSpeed}x'),
            ),
          ),
        ),
      ],
    );
  }
}
