import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'confirmation_payment_page.dart';

class VerificationPaymentPage extends StatefulWidget {
  @override
  _VerificationPaymentPageState createState() => _VerificationPaymentPageState();
}

class _VerificationPaymentPageState extends State<VerificationPaymentPage> {
  bool isLanguage = false;
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery, imageQuality: 10);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.chevron_left,
            color: Color(0xffC6AB74),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add, color: Colors.transparent),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true) ? "Detail Pembayaran" : "Cart",
            style: GoogleFonts.poppins(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black)),
            maxLines: 1,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            children: [
              SizedBox(height: height / 86,),
              Container(
                width: width,
                height: height / 10,
                decoration: BoxDecoration(
                  color: Color(0xffF7F7F8),
                  borderRadius: BorderRadius.circular(4)
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AutoSizeText(
                            "RMUDDCDZI",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "24 Oktober 2020, 10;00 WIB",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500, color: Colors.grey)),
                            maxLines: 1,
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          AutoSizeText(
                            "RP 319.000",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "1 Items",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500, color: Colors.grey)),
                            maxLines: 1,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: height / 48,),
              Container(
                width: width,
                height: height / 4.6,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 0.8),
                  borderRadius: BorderRadius.circular(4)
                ),
                child: Column(
                  children: [
                    SizedBox(height: height / 86,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AutoSizeText(
                                "Metode Pembayaran",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500, color: Colors.grey)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                          Container(
                            width: width / 4,
                            height: height / 12,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage("assets/image/icon_bca.png"))),
                          ),
                        ],
                      ),
                    ),
                    Divider(thickness: 2,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        children: [
                          Container(
                            width: width / 2.5,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "Nama Bank",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500, color: Colors.grey)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "Nomor Rekening",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500, color: Colors.grey)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "Nama Pemilik Rekening",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500, color: Colors.grey)),
                                  maxLines: 1,
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: width / 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "BANK BCA",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "12345678",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "Deni Dwi Candra",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              Container(
                width: width,
                height: height / 3,
                decoration: BoxDecoration(
                    color: Color(0xffF7F7F8),
                    borderRadius: BorderRadius.circular(4)
                ),
                child: (_image != null)
                    ?Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Container(
                      width: width,
                      height: height / 3.4,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: FileImage(_image)
                        )
                      ),),
                    )
                    :Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.image,
                      color: Color(0xffDFDFE4),
                      size: width / 4,
                    ),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      "Upload Bukti Transfer",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400, color: Colors.grey)),
                      maxLines: 1,
                    ),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      "untuk mempercepat proses verifikasi pembayaran format gambar : .JPG .JPEG .PNG, max 10MB",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500, color: Colors.grey)),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              GestureDetector(
                onTap: (){
                  getImage();
                },
                child: Container(
                  width: width / 2,
                  height: height / 24,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(4)
                  ),
                  child: Center(
                    child: AutoSizeText(
                      "Upload Image",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 2.2,
              height: height / 18,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                border: Border.all(color: Color(0xffc6ab74),)
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                child: InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"CANCEL":"Check Out", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
            SizedBox(width: width / 86,),
            Container(
              width: width / 2.2,
              height: height / 18,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: ConfirmationPaymentPage()));
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"KONFIRMASI":"Check Out", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
