import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'detail_payment_page.dart';

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  bool isLanguage = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.chevron_left,
            color: Color(0xffC6AB74),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add, color: Colors.transparent),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true) ? "Proses Pembayaran" : "Cart",
            style: GoogleFonts.poppins(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black)),
            maxLines: 1,
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
            width: width,
            height: height / 14,
            color: Color(0xffF0F0F5),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AutoSizeText(
                    (isLanguage != true) ? "Total 1 Items" : "Cart",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.black)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    "RP 319.000",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: Colors.black)),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: height / 86,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(Icons.warning, color: Colors.red, size: 50,),
                    SizedBox(width: width / 24,),
                    Container(
                      width: width / 1.8,
                      child: AutoSizeText(
                        "Pastikan info rekening pembayaran sudah benar",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Colors.red)),
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: height / 86,),
                AutoSizeText(
                  "REKENING UTAMA",
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey)),
                  maxLines: 2,
                ),
                SizedBox(height: height / 86,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText(
                          "Bank Central Asia (BCA)",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          "a/n Deni Dwi Candra",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          "12345678910",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                          maxLines: 1,
                        ),
                        SizedBox(height: height / 62,),
                        Container(
                          width: width / 8,
                          height: height * 0.035,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(MaterialCommunityIcons.pencil_outline, size: 18,),
                              AutoSizeText(
                                "Edit",
                                style: GoogleFonts.poppins(textStyle:
                                TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: width / 4,
                      height: height / 12,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/image/icon_bca.png")
                        )
                      ),
                    ),
                  ],
                ),
                SizedBox(height: height / 86,),
                Divider(thickness: 2,),
                SizedBox(height: height / 86,),
                AutoSizeText(
                  "Transfer Bank Lain",
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey)),
                  maxLines: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          width: width / 4,
                          height: height / 12,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/image/icon_bca.png")
                              )
                          ),
                        ),
                        SizedBox(width: width / 86,),
                        AutoSizeText(
                          "BANK BCA",
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500)),
                          maxLines: 2,
                        ),
                      ],
                    ),
                    IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.keyboard_arrow_right),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          width: width / 4,
                          height: height / 12,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/image/icon_bni.png")
                              )
                          ),
                        ),
                        SizedBox(width: width / 86,),
                        AutoSizeText(
                          "BANK BNI",
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500)),
                          maxLines: 2,
                        ),
                      ],
                    ),
                    IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.keyboard_arrow_right),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          width: width / 4,
                          height: height / 12,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/image/icon_bri.png")
                              )
                          ),
                        ),
                        SizedBox(width: width / 86,),
                        AutoSizeText(
                          "BANK BRI",
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500)),
                          maxLines: 2,
                        ),
                      ],
                    ),
                    IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.keyboard_arrow_right),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: DetailPaymentPage()));
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"LANJUT PEMBAYARAN":"Check Out", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}