import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_order_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class ConfirmationPaymentPage extends StatefulWidget {
  @override
  _ConfirmationPaymentPageState createState() => _ConfirmationPaymentPageState();
}

class _ConfirmationPaymentPageState extends State<ConfirmationPaymentPage> {
  bool isLanguage = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: Center(
          child: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: AutoSizeText(
              (isLanguage != true) ? "Close" : "Close",
              style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xffC6AB74))),
              maxLines: 1,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: height / 86,),
            Center(
              child: AutoSizeText(
                (isLanguage != true) ? "THANK YOU" : "Close",
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        color: Color(0xffC6AB74))),
                maxLines: 1,
              ),
            ),
            Container(
              width: width / 1.1,
              child: Center(
                child: AutoSizeText(
                  (isLanguage != true) ? "PEMBAYARAN ANDA SEDANG DIKONFIRMASI" : "Close",
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500)),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SvgPicture.asset("assets/image/icon_order_confirm.svg"),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(

                children: [
                  SizedBox(height: height / 86,),
                  Container(
                    width: width,
                    height: height / 5,
                    decoration: BoxDecoration(
                      color: Color(0xffF7F7F8),
                      borderRadius: BorderRadius.circular(6)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizeText(
                                (isLanguage != true) ? "Nomor Pesanan" : "Close",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500, color: Colors.grey)),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "RMUDDCDZI",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                          SizedBox(height: height / 86,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizeText(
                                (isLanguage != true) ? "Tanggal Konfirmasi" : "Close",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500, color: Colors.grey)),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "24 Oktober 2020",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                          SizedBox(height: height / 86,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizeText(
                                (isLanguage != true) ? "Total Pesanan" : "Close",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500, color: Colors.grey)),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "Rp 319.000",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                          SizedBox(height: height / 86,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizeText(
                                (isLanguage != true) ? "Metode Pembayaran" : "Close",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500, color: Colors.grey)),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "Transfer BANK BCA",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: height / 86,),
                  Container(
                    width: width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Color(0xffc6ab74),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushReplacement(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: SettingOrderPage()));
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text(
                              (isLanguage != true)
                                  ? "Check Status Order"
                                  : "Sign in",
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: height / 86,),
                  Container(
                    width: width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                      border: Border.all(color: Color(0xffc6ab74))
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text(
                              (isLanguage != true)
                                  ? "Lanjut Belanja"
                                  : "Sign in",
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
