import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'check_out_ page.dart';

class CartActivity extends StatefulWidget {
  @override
  _CartActivityState createState() => _CartActivityState();
}

class _CartActivityState extends State<CartActivity> {
  bool isLanguage = false;
  ScrollController _scrollViewController;

  List<String> radioBtnVoucher = ["Redeem Reborn Points (0)", "Kode Voucher"];
  List<bool> isRadioBtnVoucher = [false, false];
  List<String> itemCart = ["Byoote", "Byoote Collagen"];
  List<int> quantityItemCart = [1, 1];

  @override
  void dispose() {
    _scrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.chevron_left,
            color: Color(0xffC6AB74),
            size: 32,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add, color: Colors.transparent),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true) ? "Keranjang" : "Cart",
            style: GoogleFonts.poppins(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black)),
            maxLines: 1,
          ),
        ),
      ),
      body: SingleChildScrollView(
        controller: _scrollViewController,
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(
              controller: _scrollViewController,
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                children: itemCart.map((e) {
                  return Container(
                    width: width,
                    height: height / 2.3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        height: height / 8,
                                        width: width / 4.8,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color(0xffC6AB74)),
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/image/image_product_beauty.png"),
                                              fit: BoxFit.fill),
                                        ),
                                      ),
                                      SizedBox(
                                        width: width / 18,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          AutoSizeText(
                                            e,
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                            maxLines: 1,
                                          ),
                                          SizedBox(
                                            height: height * 0.005,
                                          ),
                                          AutoSizeText(
                                            "Byoote collagen skin",
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.w400)),
                                            maxLines: 2,
                                          ),
                                          AutoSizeText(
                                            "Order No: CS-A7WHPL1001 ",
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    color: Colors.grey)),
                                            maxLines: 1,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.delete_outline,
                                          color: Color(0xffE0E0E0),
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.delete_outline,
                                          color: Colors.transparent,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(
                                height: height / 86,
                              ),
                              Divider(
                                thickness: 2,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  AutoSizeText(
                                    "Quantity",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500)),
                                    maxLines: 1,
                                  ),
                                  Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          if (quantityItemCart[
                                                  itemCart.indexOf(e)] >
                                              1) {
                                            setState(() {
                                              quantityItemCart[
                                                      itemCart.indexOf(e)] =
                                                  quantityItemCart[
                                                          itemCart.indexOf(e)] -
                                                      1;
                                            });
                                          }
                                        },
                                        child: Container(
                                          width: width / 12,
                                          height: height / 24,
                                          decoration: BoxDecoration(
                                              color: Color(0xffE0E0E0),
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(4),
                                                  bottomLeft:
                                                      Radius.circular(4))),
                                          child: Center(
                                            child: Icon(
                                              Icons.remove,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: width / 4.2,
                                        height: height / 24,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Color(0xffE0E0E0))),
                                        child: Center(
                                          child: AutoSizeText(
                                            quantityItemCart[
                                                    itemCart.indexOf(e)]
                                                .toString(),
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                            maxLines: 1,
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            quantityItemCart[itemCart
                                                .indexOf(e)] = quantityItemCart[
                                                    itemCart.indexOf(e)] +
                                                1;
                                          });
                                        },
                                        child: Container(
                                          width: width / 12,
                                          height: height / 24,
                                          decoration: BoxDecoration(
                                              color: Color(0xffE0E0E0),
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(4),
                                                  bottomRight:
                                                      Radius.circular(4))),
                                          child: Center(
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Divider(
                                thickness: 2,
                              ),
                              SizedBox(
                                height: height / 86,
                              ),
                              Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Unit Price",
                                        style: GoogleFonts.poppins(
                                            textStyle: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.grey)),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "Rp 300.000",
                                        style: GoogleFonts.poppins(
                                            textStyle: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: width / 4,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Total",
                                        style: GoogleFonts.poppins(
                                            textStyle: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.grey)),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "Rp 300.000",
                                        style: GoogleFonts.poppins(
                                            textStyle: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              AutoSizeText(
                                "SAVE 20%",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xffC6AB74))),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "Rp 200.000",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        decoration:
                                            TextDecoration.lineThrough)),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "Attribute",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey)),
                                maxLines: 1,
                              ),
                              AutoSizeText(
                                "Size: 50 gr",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400)),
                                maxLines: 1,
                              ),
                              SizedBox(
                                height: height / 86,
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          thickness: 6,
                        )
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    "Gunakan Atau Redeem Kode Voucher",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                  Column(
                    children: radioBtnVoucher.map((e) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              print(radioBtnVoucher.indexOf(e));
                              if (isRadioBtnVoucher[
                                      radioBtnVoucher.indexOf(e)] ==
                                  true) {
                                setState(() {
                                  isRadioBtnVoucher[0] = false;
                                  isRadioBtnVoucher[1] = false;
                                });
                              } else {
                                if (radioBtnVoucher.indexOf(e) != 1) {
                                  setState(() {
                                    isRadioBtnVoucher[
                                        radioBtnVoucher.indexOf(e)] = true;
                                    isRadioBtnVoucher[1] = false;
                                  });
                                } else {
                                  setState(() {
                                    isRadioBtnVoucher[
                                        radioBtnVoucher.indexOf(e)] = true;
                                    isRadioBtnVoucher[0] = false;
                                  });
                                }
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    height: height / 18,
                                    width: width / 14,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            color: Color(0xffE0E0E0),
                                            width: 4)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(1),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: (isRadioBtnVoucher[
                                                    radioBtnVoucher.indexOf(e)])
                                                ? Color(0xffC6AB74)
                                                : Colors.transparent),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: width / 68,
                                  ),
                                  AutoSizeText(
                                    e,
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600)),
                                    maxLines: 1,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: height / 24,
                            width: width / 5,
                            decoration: BoxDecoration(
                                color: (isRadioBtnVoucher[
                                        radioBtnVoucher.indexOf(e)])
                                    ? Colors.black
                                    : Color(0xffE0E0E0),
                                borderRadius: BorderRadius.circular(4)),
                            child: Center(
                              child: AutoSizeText(
                                "Apply",
                                style: GoogleFonts.poppins(
                                    textStyle: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white)),
                                maxLines: 1,
                              ),
                            ),
                          ),
                        ],
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 6,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Sub Total",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "Rp 300.000",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: height / 86,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Reborn Point",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "50",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  Divider(
                    thickness: 2,
                  ),
                  AutoSizeText(
                    "Kumpulkan Reborn Point(RP) setiap kelipatan belanja Rp 50,000 dan gunakan RP untuk potongan belanjamu berikutnya.",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.italic)),
                    maxLines: 2,
                  ),
                  SizedBox(
                    height: height / 32,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "TOTAL",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "Rp 300.000",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: CheckOutPage()));
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text(
                      (isLanguage != true) ? "CHECK OUT" : "Check Out",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
