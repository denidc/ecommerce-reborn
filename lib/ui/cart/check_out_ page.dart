import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'payment_page.dart';

class CheckOutPage extends StatefulWidget {
  @override
  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  bool isLanguage = false;

  List<String> radioBtnVoucher = ["Pengiriman Regular", "Pengiriman Premium"];
  List<String> radioBtnDesc = ["Estimasi pengiriman 3-5 hari kerja setalah tanggal konfirmasi pembayaran atau pembayaran sukses.",
    "Estimasi pengiriman 1 hari kerja setalah tanggal konfirmasi pembayaran atau pembayaran sukses."];
  List<bool> isRadioBtnVoucher = [false, false];
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.chevron_left,
            color: Color(0xffC6AB74),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add, color: Colors.transparent),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true) ? "Alamat Pengiriman" : "Cart",
            style: GoogleFonts.poppins(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black)),
            maxLines: 1,
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: (){
                      // Navigator.push(context, PageTransition(type:
                      // PageTransitionType.rightToLeft, child:
                      // InfoAddressPage()));
                    },
                    child: Container(
                      width: width,
                      decoration: BoxDecoration(
                          color: Colors.transparent
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Icon(Icons.location_on, color: Color(0xffC6AB74),),
                              AutoSizeText(
                                (isLanguage != true)?"Rumah":"Empty Bank Account",
                                style: GoogleFonts.poppins(textStyle:
                                TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                maxLines: 1,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              AutoSizeText(
                                (isLanguage != true)?"Alamat Utama":"Empty Bank Account",
                                style: GoogleFonts.poppins(textStyle:
                                TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                maxLines: 1,
                              ),
                              Icon(Icons.chevron_right, color: Color(0xffC6AB74),),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Divider(thickness: 2,),
                  SizedBox(height: height / 86,),
                  AutoSizeText(
                    (isLanguage != true)?"Nama Penerima:":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    (isLanguage != true)?"Deni Dwi Candra":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                  SizedBox(height: height / 62,),
                  AutoSizeText(
                    (isLanguage != true)?"Alamat:":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    (isLanguage != true)?"Jl. Ambengan Batu I, Kec. Tambaksari, Kota SBY, Jawa Timur, No.25 Rt.01. Rw.02 Tambaksari Kota Surabaya Jawa TImur, Indonesia 12345":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                    maxLines: 5,
                  ),
                  SizedBox(height: height / 62,),
                  AutoSizeText(
                    (isLanguage != true)?"Telepon Penerima:":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    (isLanguage != true)?"12345678910":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Divider(thickness: 4,),
            SizedBox(height: height / 86,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    (isLanguage != true)?"Metode Pengiriman":"Empty Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                  Column(
                    children: radioBtnVoucher.map((e) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: (){
                              print(radioBtnVoucher.indexOf(e));
                              if(isRadioBtnVoucher[radioBtnVoucher.indexOf(e)] == true){
                                setState(() {
                                  isRadioBtnVoucher[0] = false;
                                  isRadioBtnVoucher[1] = false;
                                });
                              }else{
                                if(radioBtnVoucher.indexOf(e) != 1){
                                  setState(() {
                                    isRadioBtnVoucher[radioBtnVoucher.indexOf(e)] = true;
                                    isRadioBtnVoucher[1] = false;
                                  });
                                }else{
                                  setState(() {
                                    isRadioBtnVoucher[radioBtnVoucher.indexOf(e)] = true;
                                    isRadioBtnVoucher[0] = false;
                                  });
                                }
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: height / 18,
                                              width: width / 14,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(color: Color(0xffE0E0E0), width: 4)
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.all(1),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: (isRadioBtnVoucher[radioBtnVoucher.indexOf(e)])
                                                          ?Color(0xffC6AB74):Colors.transparent
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: width / 68,),
                                            AutoSizeText(
                                              e,
                                              style: GoogleFonts.poppins(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight: FontWeight.w600)),
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                        Container(
                                          width: width / 1.2,
                                          child: AutoSizeText(
                                            radioBtnDesc[radioBtnVoucher.indexOf(e)],
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 11,
                                                    fontWeight: FontWeight.w300)),
                                            maxLines: 4,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }).toList(),
                  ),
                  Divider(thickness: 2,),
                  Container(
                    height: height / 12,
                    width: width,
                    decoration: BoxDecoration(
                      color: Color(0xffE0E0E0)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AutoSizeText(
                            "Gratis ongkos kirim minimal order Rp.500.000.",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "Tambah Rp. 200.000 lagi untuk mendapatkan gratis ongkos kirim.",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400, color: Color(0xffC6AB74))),
                            maxLines: 2,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(thickness: 6,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Sub Total",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "Rp 300.000",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  SizedBox(height: height / 86,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Biaya Pengiriman",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "Rp 19.000",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  SizedBox(height: height / 86,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Reborn Point",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "50",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  Divider(thickness: 2,),
                  AutoSizeText(
                    "Kumpulkan Reborn Point(RP) setiap kelipatan belanja Rp 50,000 dan gunakan RP untuk potongan belanjamu berikutnya.",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w300, fontStyle: FontStyle.italic)),
                    maxLines: 2,
                  ),
                  SizedBox(height: height / 32,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "TOTAL",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "Rp 319.000",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: PaymentPage()));
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"LANJUT PEMBAYARAN":"Check Out", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
