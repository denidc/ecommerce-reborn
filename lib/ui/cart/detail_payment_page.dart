import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'verification_payment_page.dart';

class DetailPaymentPage extends StatefulWidget {
  @override
  _DetailPaymentPageState createState() => _DetailPaymentPageState();
}

class _DetailPaymentPageState extends State<DetailPaymentPage> {
  bool isLanguage = false;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.chevron_left,
            color: Color(0xffC6AB74),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add, color: Colors.transparent),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true) ? "Detail Pembayaran" : "Cart",
            style: GoogleFonts.poppins(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black)),
            maxLines: 1,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                height: height / 64,
              ),
              Container(
                width: width,
                height: height / 7,
                decoration: BoxDecoration(
                  color: Color(0xffFBF4E7),
                ),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        (isLanguage != true) ? "SILAHKAN SELESAIKAN" : "Cart",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Color(0xffC6AB74))),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        (isLanguage != true) ? "PEMBAYARAN" : "Cart",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Color(0xffC6AB74))),
                        maxLines: 1,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: height / 86,
              ),
              AutoSizeText(
                (isLanguage != true) ? "TRANSFER KE" : "Cart",
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey)),
                maxLines: 1,
              ),
              Container(
                width: width / 4,
                height: height / 12,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/image/icon_bca.png"))),
              ),
              AutoSizeText(
                (isLanguage != true) ? "a.n PT. Bersinar Reborn Kembali" : "Cart",
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey)),
                maxLines: 1,
              ),
              SizedBox(
                height: height / 86,
              ),
              AutoSizeText(
                "00789088790",
                style: GoogleFonts.poppins(
                    textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
              SizedBox(
                height: height / 86,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.insert_drive_file,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: width / 86,
                  ),
                  AutoSizeText(
                    "Salin No. Rekening",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.red)),
                    maxLines: 1,
                  ),
                ],
              ),
              SizedBox(
                height: height / 86,
              ),
              Divider(
                thickness: 2,
              ),
              SizedBox(
                height: height / 86,
              ),
              AutoSizeText(
                "JUMLAH YANG DIBAYAR",
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey)),
                maxLines: 1,
              ),
              SizedBox(
                height: height / 86,
              ),
              AutoSizeText(
                "RP 319.000",
                style: GoogleFonts.poppins(
                    textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
              SizedBox(
                height: height / 86,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.insert_drive_file,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: width / 86,
                  ),
                  AutoSizeText(
                    "Salin No. Rekening",
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.red)),
                    maxLines: 1,
                  ),
                ],
              ),
              SizedBox(
                height: height / 86,
              ),
              Container(
                width: width,
                height: height / 8,
                decoration: BoxDecoration(
                  color: Color(0xffF7F7F8),
                  borderRadius: BorderRadius.circular(4)
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AutoSizeText(
                            "Nomor Pesanan",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "RMUDDCDZI",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AutoSizeText(
                            "Total Pesanan",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "Rp 319.000",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AutoSizeText(
                            "Kode Unik",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "1",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: width / 1.1,
                child: AutoSizeText(
                  "Pastikan Anda melakukan pembayaran dalam waktu 24 jam setelah pesanan dibuat untuk menghindari pembatalan otomatis dan silahkan lakukan konfirmasi pembayaran jika kamu sudah melakukan pembayaran di halaman Akun . Ayo kumpulkan Reborn Point dari setiap pembelian yang kamu lakukan.",
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w300)),
                  maxLines: 10,
                ),
              ),
              SizedBox(
                height: height / 86,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: VerificationPaymentPage()));
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"KONFIRMASI PEMBAYARAN":"Check Out", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
