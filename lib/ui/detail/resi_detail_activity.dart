import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ResiDetailActivity extends StatefulWidget {
  @override
  _ResiDetailActivityState createState() => _ResiDetailActivityState();
}

class _ResiDetailActivityState extends State<ResiDetailActivity> {
  List<bool> isPassed = [false, true, true, true, true];
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: (){

            },
            icon: Icon(Icons.add, color: Colors.transparent,),
          )
        ],
        title: Center(
          child: AutoSizeText(
            "Proses Pengiriman",
            style: GoogleFonts.poppins(textStyle:
            TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black)),
            maxLines: 1,
          ),
        ),
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
        ),
      ),
      body: ListView.builder(
        itemCount: isPassed.length,
          itemBuilder: (_,index){
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: width / 48),
              child: Column(
                children: [
                  SizedBox(height: height * 0.01,),
                  Row(
                    children: [
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              shape: BoxShape.circle
                          ),
                          child: Icon(Icons.fiber_manual_record,
                            color: (isPassed[index])?Colors.grey:Color(0xff00B212), size: 18,)),
                      SizedBox(width: width * 0.01,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: width / 1.3,
                            child: AutoSizeText(
                              (isPassed[index])?
                              "[ SURABAYA ] WITH DELIVERY COURIER [SIDOARJO INBOUND]"
                              : "[ SURABAYA ] DELIVERED TO [DENI | 27-11-2020 | SURABAYA]",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                                  color: (isPassed[index])?Colors.grey:Color(0xff00B212))),
                              maxLines: 2,
                            ),
                          ),
                          AutoSizeText(
                            "27-11-2020 15 : 30",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 10, fontWeight: FontWeight.w400)),
                            maxLines: 1,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Divider(thickness: 0.5,)
                ],
              ),
            );
          }
      ),
    );
  }
}
