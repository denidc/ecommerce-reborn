import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TipsDetailActivity extends StatefulWidget {
  @override
  _TipsDetailActivityState createState() => _TipsDetailActivityState();
}

class _TipsDetailActivityState extends State<TipsDetailActivity> {
  bool isLanguage = false;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: height / 14,
        elevation: 0,
        flexibleSpace: SafeArea(
          child: Column(
            children: [
              Container(
                width: width,
                height: height / 14,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: (){

                        },
                        icon: Icon(Icons.chevron_left, color: Color(0xffC6AB74),),
                      ),
                      AutoSizeText(
                        (isLanguage != true)?"Tips":"Tips",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: (){

                            },
                            icon: Icon(Icons.share, color: Color(0xff707070),),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        AutoSizeText(
                          (isLanguage != true)?"Dipublish oleh ":"Article",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          (isLanguage != true)?"REBORN":"Article",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: width / 12,
                          height: height / 18,
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffC6AB74)),
                              shape: BoxShape.circle
                          ),
                          child: Center(
                            child: AutoSizeText(
                              "R",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                                  color: Color(0xffC6AB74))),
                              maxLines: 1,
                            ),
                          ),
                        ),
                        SizedBox(width: width / 86,),
                        AutoSizeText(
                          "REBORN BEAUTY",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              Center(
                child: AutoSizeText(
                  "SANDBAGGING",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                  maxLines: 1,
                ),
              ),
              SizedBox(height: height / 86,),
              Container(
                width: width,
                height: height / 3.4,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/image/banner_glossary.png"),
                        fit: BoxFit.fill
                    )
                ),
              ),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  "Untuk tampil cantik setiap orang perlu melakukan riasan pada wajahnya. Tak heran produk kecantikan atau make up pun kini semakin beragam ditawarkan dipasaran. Mulai dari lipstik, foundation, eyeliner, dan lain sebagainya merupakan peralatan wajib yang sangat dibutuhkan untuk seseorang bisa tampil cantik. Bagi kaum wanita, make up merupakan suatu kebutuhan yang tidak boleh sampai terlewatkan dalam hidup. Bahkan seorang wanita rela menghabiskan seluruh uang yang dimilikinya hanya untuk membeli perlengkapan make up demi menunjang penampilannya.",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                  maxLines: 100,
                ),
              ),
              SizedBox(height: height / 86,),
              Divider(thickness: 6,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true)?"Secret Artikel Terbaru":"Video Review Product",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              SizedBox(height: height / 86,),
              Container(
                width: width,
                height: height / 3.4,
                child: GridView.count(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  crossAxisCount: 2,
                  childAspectRatio: 2 / 6,
                  children: [
                    1,2,3,4,5,6,7,8,9,10
                  ].map((e) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                      child: Row(
                        children: [
                          Container(
                            width: width / 4,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: DecorationImage(
                                    image: AssetImage("assets/image/image_product_beauty.png")
                                )
                            ),
                          ),
                          SizedBox(width: width / 86,),
                          Container(
                            width: width / 2.8,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                AutoSizeText(
                                  (isLanguage != true)?"Kecantikan":"Beauty",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Color(0xff907135))),
                                  maxLines: 1,
                                ),
                                SizedBox(height: height / 86,),
                                AutoSizeText(
                                  (isLanguage != true)?"5 Produk The Ordinary yang Cocok Bagi Pemilik Kulit Keriput":"Beauty",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                  maxLines: 2,
                                ),
                                AutoSizeText(
                                  "11 Oktober 2020",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(height: height / 86,),
              Divider(thickness: 6,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true)?"Video Review Terbaru":"Video Review Product",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              SizedBox(height: height / 86,),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                child: Row(
                  children: [
                    1,2,3
                  ].map((e) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Container(
                                width: width / 2.2,
                                height: height / 8,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage("assets/image/image_product_beauty.png"),
                                      fit: BoxFit.cover
                                  ),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  width: width / 6.6,
                                  height: height / 20,
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Icon(Icons.play_arrow, color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            width: width / 2.2,
                            child: AutoSizeText(
                              "REVIEW JUJUR BYOOTE BY PAO PAO LDP!",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(height: height / 18,)
            ],
          ),
        ),
      ),
    );
  }
}
