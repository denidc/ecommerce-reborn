import 'package:auto_size_text/auto_size_text.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProductDetailActivity extends StatefulWidget {
  @override
  _ProductDetailActivityState createState() => _ProductDetailActivityState();
}

class _ProductDetailActivityState extends State<ProductDetailActivity> with SingleTickerProviderStateMixin{
  bool isLanguage = false;
  double ratingCount = 5;
  double ratingCountDetail = 4.5;
  int tabPage = 0;
  TabController _tabController;
  ScrollController _scrollViewController;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }

  String detail = "Byoote Suplemen Kesehatan adalah minuman kecantikan yang diformulasikan dari Jepang, yang mengandung bahan utama Glutathione Grade A dan Collagen.\n\nBerikut kandungan lengkap yang terdapat dalam Byoote:\n\nL-Glutathione Grade A, Collagen, Alga Biru, Snow Algae,\nGreen Algae, Vitamin C, dan Vitamin B-Complex Lengkap\nyang diproses menggunakan teknologi dari Jepang.\n\nSecara keseluruhan Manfaat Utama Byoote:\n\nMengecilkan pori-pori\nMencerahkan kulit, menjadikan kulit bersinar\nMengencangkan Kulit dan terlihat lebih muda\nMenyamarkan garis halus, kerutan dan bekasluka\n\nCara penggunaan:\n\n1 Sachet Byoote dituang ke 200 ml air putih aduk & minum\nMinum Byoote 1 sachet dipagi hari sebelum sarapan\ndan 1 sachet dimalam hari untuk hasil maksimal.\n\nNote : Tidak dianjurkan untuk wanita hamil,\nwanita menyusui, dan balita";
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: height / 14,
        elevation: 0,
        leading: Container(),
        flexibleSpace: SafeArea(
          child: Column(
            children: [
              Container(
                width: width,
                height: height / 14,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                      ),
                      AutoSizeText(
                        (isLanguage != true)?"Detail Produk":"Product Detail",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: (){

                            },
                            icon: Icon(Icons.share, color: Color(0xff707070),),
                          ),
                          IconButton(
                            onPressed: (){

                            },
                            icon: Icon(Icons.shopping_cart, color: Color(0xff707070),),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: SingleChildScrollView(
          controller: _scrollViewController,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    width: width,
                    height: height / 2.8,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/image/image_product_beauty.png"),
                            fit: BoxFit.fill
                        )
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: width / 8,
                        height: height / 16,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xffC6AB74),
                        ),
                        child: Center(
                          child: AutoSizeText(
                            "-20%",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 11, fontWeight: FontWeight.w500, color: Colors.white)),
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText(
                      "Byoote",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                    AutoSizeText(
                      "Byoote collagen skin",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                      maxLines: 1,
                    ),
                    Row(
                      children: [
                        AutoSizeText(
                          "Rp 200.000",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                              color: Colors.grey, decoration: TextDecoration.lineThrough)),
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          " SAVE 20%",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    SizedBox(height: height /86,),
                    AutoSizeText(
                      "Rp 300.000",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                    Row(
                      children: <Widget>[
                        RatingBar(
                          itemSize: 18,
                          initialRating: ratingCount,
                          ignoreGestures: true,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Color(0xffC6AB74),
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                        SizedBox(width: 4,),
                        Text(ratingCount.toString(), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                      ],
                    ),
                    Row(
                      children: [
                        AutoSizeText(
                          "20 reviewers recommend this",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                              color: Colors.grey)),
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          " See Review",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    Divider(thickness: 2,),
                    SizedBox(height: height / 86,),
                    Row(
                      children: [
                        AutoSizeText(
                          "SIZE",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                          maxLines: 1,
                        ),
                        SizedBox(width: width / 48,),
                        AutoSizeText(
                          "50gr",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    Container(
                      width: width / 6,
                      height: height / 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffC6AB74)),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: Center(
                        child: AutoSizeText(
                          "50gr",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              Divider(thickness: 4,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true)?"Details":"Details",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              Container(
                width: width / 2,
                decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Color(0xffC6AB74), width: 2)),
                ),),
              Container(
                width: width,
                decoration: BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.grey, width: 2)),
                ),),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  detail,
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                  maxLines: 100,
                ),
              ),
              SizedBox(height: height / 86,),
              Divider(thickness: 4,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText(
                      (isLanguage != true)?"RATING PENGGUNA RATA-RATA":"Product Detail",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                    SizedBox(height: height / 86,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        RatingBar(
                          itemSize: width / 12,
                          initialRating: ratingCountDetail,
                          ignoreGestures: true,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Color(0xffC6AB74),
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                        SizedBox(width: 4,),
                        Text(ratingCountDetail.toString(), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w600)))
                      ],
                    ),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      (isLanguage != true)?"770 of 780 reviewers recomend this product":"Details",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                      maxLines: 1,
                    ),
                    SizedBox(height: height / 86,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                              width:  width / 3,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  AutoSizeText(
                                    (isLanguage != true)?"Packaging":"Packaging",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                    maxLines: 1,
                                  ),
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        ratingCountDetail.toString(),
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "/5",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            SizedBox(width: width / 48,),
                            Container(
                              width:  width / 2.8,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  AutoSizeText(
                                    (isLanguage != true)?"Warna":"Color",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                    maxLines: 1,
                                  ),
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        ratingCountDetail.toString(),
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "/5",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              width:  width / 3,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  AutoSizeText(
                                    (isLanguage != true)?"Texture":"Texture",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                    maxLines: 1,
                                  ),
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        ratingCountDetail.toString(),
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "/5",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            SizedBox(width: width / 48,),
                            Container(
                              width:  width / 2.8,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  AutoSizeText(
                                    (isLanguage != true)?"Manfaat Produk":"Product Benefit",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                    maxLines: 1,
                                  ),
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        ratingCountDetail.toString(),
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "/5",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: height / 86,),
                    Divider(thickness: 2,),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      (isLanguage != true)?"Photo review pengguna":"Product Benefit",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                      maxLines: 1,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      physics: BouncingScrollPhysics(),
                      child: Row(
                        children: [
                          1,2,3,4
                        ].map((e) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: Container(
                              width: width / 3.6,
                              height: height / 8,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                      image: AssetImage("assets/image/image_review_user.png"),
                                      fit: BoxFit.fill
                                  )
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(height: height / 64,),
                    Container(
                      width: width,
                      height: height / 24,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: Center(
                        child: AutoSizeText(
                          (isLanguage != true)?"Tulis Review":"Review Write",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                          maxLines: 1,
                        ),
                      ),
                    ),
                    SizedBox(height: height / 48,),
                  ],
                ),
              ),
              TabBar(
                controller: _tabController,
                indicatorColor: Color(0xffC6AB74),
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.black,
                onTap: (v){
                  setState(() {
                    tabPage = v;
                  });
                },
                tabs: [
                  Tab(child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Semua Review(6)":"Beauty",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),),
                  Tab(child:
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Review Photo":"Health",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  )),
                ],
              ),
              Container(
                height: height + height / 2.2,
                child: TabBarView(
                    controller: _tabController,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Column(
                        children: [
                          1,2,3,4
                        ].map((e) {
                          return Container(
                            child: Column(
                              children: [
                                SizedBox(height: height / 86,),
                                Row(
                                  children: [
                                    CircularProfileAvatar(
                                      '',
                                      radius: height * 0.03,
                                      borderColor: Colors.white,
                                      // borderWidth: height * 0.008,
                                      cacheImage: true,
                                      child: SvgPicture.asset("assets/image/image_profile.svg"),
                                      // child: Container(
                                      //   decoration: BoxDecoration(
                                      //     color: Colors.amber
                                      //   ),
                                      // ),
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        AutoSizeText(
                                          "Nur Sukma Wati",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                                              color: Color(0xffC6AB74))),
                                          maxLines: 1,
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            AutoSizeText(
                                              "2 hari",
                                              style: GoogleFonts.poppins(textStyle:
                                              TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                                                  color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              " • ",
                                              style: GoogleFonts.poppins(textStyle:
                                              TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                                                  color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              "Join Juni 19",
                                              style: GoogleFonts.poppins(textStyle:
                                              TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                                                  color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              " • ",
                                              style: GoogleFonts.poppins(textStyle:
                                              TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                                                  color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              "Top Referral Oktober 20",
                                              style: GoogleFonts.poppins(textStyle:
                                              TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                                                  color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(height: height / 86,),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          RatingBar(
                                            itemSize: width / 24,
                                            initialRating: ratingCountDetail,
                                            ignoreGestures: true,
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Color(0xffC6AB74),
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          ),
                                          SizedBox(width: 4,),
                                          Text(ratingCountDetail.toString(), style: GoogleFonts.poppins(
                                              textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.w600)))
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                width:  width / 3,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    AutoSizeText(
                                                      (isLanguage != true)?"Packaging":"Packaging",
                                                      style: GoogleFonts.poppins(textStyle:
                                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                      maxLines: 1,
                                                    ),
                                                    Row(
                                                      children: [
                                                        AutoSizeText(
                                                          ratingCountDetail.toString(),
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                                          maxLines: 1,
                                                        ),
                                                        AutoSizeText(
                                                          "/5",
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                          maxLines: 1,
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              SizedBox(width: width / 48,),
                                              Container(
                                                width:  width / 2.8,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    AutoSizeText(
                                                      (isLanguage != true)?"Warna":"Color",
                                                      style: GoogleFonts.poppins(textStyle:
                                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                      maxLines: 1,
                                                    ),
                                                    Row(
                                                      children: [
                                                        AutoSizeText(
                                                          ratingCountDetail.toString(),
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                                          maxLines: 1,
                                                        ),
                                                        AutoSizeText(
                                                          "/5",
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                          maxLines: 1,
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width:  width / 3,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    AutoSizeText(
                                                      (isLanguage != true)?"Texture":"Texture",
                                                      style: GoogleFonts.poppins(textStyle:
                                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                      maxLines: 1,
                                                    ),
                                                    Row(
                                                      children: [
                                                        AutoSizeText(
                                                          ratingCountDetail.toString(),
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                                          maxLines: 1,
                                                        ),
                                                        AutoSizeText(
                                                          "/5",
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                          maxLines: 1,
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              SizedBox(width: width / 48,),
                                              Container(
                                                width:  width / 2.8,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    AutoSizeText(
                                                      (isLanguage != true)?"Manfaat Produk":"Product Benefit",
                                                      style: GoogleFonts.poppins(textStyle:
                                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                      maxLines: 1,
                                                    ),
                                                    Row(
                                                      children: [
                                                        AutoSizeText(
                                                          ratingCountDetail.toString(),
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xffC6AB74))),
                                                          maxLines: 1,
                                                        ),
                                                        AutoSizeText(
                                                          "/5",
                                                          style: GoogleFonts.poppins(textStyle:
                                                          TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                          maxLines: 1,
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: height / 86,),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8),
                                  child: AutoSizeText(
                                    "Been consuming this collagen drink for several weeks, pas laser muka beberapa minggu lalu, belom konsumsi byoote, kulit kering kelupasnya tuh sampe 1 mggu, 2 mggu lalu laser lagi dan kulit recovery nya kebantu bgt dari byoote. Keringnya cuma 2 hr dan ga kelupas banyak. kulit lebih cepet lembab dan lebih cerah.",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                    maxLines: 100,
                                  ),
                                ),
                                Row(
                                  children: [
                                    IconButton(
                                      onPressed: (){

                                      },
                                      icon: Icon(AntDesign.like2),
                                    ),
                                    AutoSizeText(
                                      "20 LIKE",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                                      maxLines: 1,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        }).toList(),
                      ),
                      Container(
                        child: Text("Articles Body"),
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    Future.delayed(Duration(seconds: 3), (){
                      Navigator.pop(context);
                    });
                    showDialog(
                       barrierDismissible: false,
                        context: context,
                        builder: (ctx){
                          return AlertDialog(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset("assets/image/icon_bag.svg"),
                                Container(
                                  width: width,
                                  height: height / 5,
                                  child: Column(
                                    children: [
                                      Center(
                                        child: AutoSizeText(
                                          (isLanguage != true)?"Berhasil Ditambahkan Ke Shooping Bag":"Successfully Added To Shopping Bag",
                                          wrapWords: true,
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Color(0xffc6ab74))),
                                          maxLines: 1,
                                        ),
                                      ),
                                      Divider(thickness: 2,),
                                      Row(
                                        children: [
                                          Container(
                                            width: width / 7.6,
                                            height: height / 8,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(12),
                                                image: DecorationImage(
                                                    image: AssetImage("assets/image/image_product_beauty.png")
                                                )
                                            ),
                                          ),
                                          SizedBox(width: width / 86,),
                                          Container(
                                            width: width / 2,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                AutoSizeText(
                                                  "Byoote",
                                                  style: GoogleFonts.poppins(textStyle:
                                                  TextStyle(fontSize: 14, fontWeight:
                                                  FontWeight.w600,)),
                                                  maxLines: 1,
                                                ),
                                                AutoSizeText(
                                                  "Byoote collagen skin",
                                                  style: GoogleFonts.poppins(textStyle:
                                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                                  maxLines: 2,
                                                ),
                                                AutoSizeText(
                                                  "Size: 50gr",
                                                  style: GoogleFonts.poppins(textStyle:
                                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                                  maxLines: 1,
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                    );
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"Tambah ke Keranjang":"Add to Bag", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
