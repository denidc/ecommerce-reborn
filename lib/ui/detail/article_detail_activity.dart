import 'package:auto_size_text/auto_size_text.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ArticleDetailActivity extends StatefulWidget {
  @override
  _ArticleDetailActivityState createState() => _ArticleDetailActivityState();
}

class _ArticleDetailActivityState extends State<ArticleDetailActivity> {
  bool isLanguage = false;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: height / 14,
        elevation: 0,
        leading: Container(),
        flexibleSpace: SafeArea(
          child: Column(
            children: [
              Container(
                width: width,
                height: height / 14,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.chevron_left, color: Color(0xffC6AB74), size: 32,),
                      ),
                      AutoSizeText(
                        (isLanguage != true)?"Artikel":"Article",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: (){

                            },
                            icon: Icon(Icons.notifications_none, color: Color(0xff707070),),
                          ),
                          IconButton(
                            onPressed: (){

                            },
                            icon: Icon(Icons.shopping_cart, color: Color(0xff707070),),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width.round() / 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        AutoSizeText(
                          (isLanguage != true)?"Dipublish oleh ":"Article",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          (isLanguage != true)?"REBORN":"Article",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: width / 12,
                          height: height / 18,
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffC6AB74)),
                              shape: BoxShape.circle
                          ),
                          child: Center(
                            child: AutoSizeText(
                              "R",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                                  color: Color(0xffC6AB74))),
                              maxLines: 1,
                            ),
                          ),
                        ),
                        SizedBox(width: width / 86,),
                        AutoSizeText(
                          "REBORN BEAUTY",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w600,
                              color: Color(0xffC6AB74))),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      "KECANTIKAN",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                      maxLines: 1,
                    ),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      "COMES FROM WITHIN",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                      maxLines: 1,
                    ),
                    SizedBox(height: height / 86,),
                    AutoSizeText(
                      "Mulailah hari dengan kebahagiaan, dan membuat cerita yang baru melalui versi terbaik kamu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Arcu, et mauris ut dui. Ultrices dui pellentesque netus eget aliquam enim leo. Tempor dignissim enim massa tellus. Netus purus sem arcu cras velit. Vulputate malesuada feugiat enim, elit. Volutpat leo elit.",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                      maxLines: 100,
                    ),
                    SizedBox(height: height / 86,),
                    Container(
                      width: width,
                      height: height / 4,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/image/image_review_user.png"),
                              fit: BoxFit.fill
                          )
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width.round() / 18),
                child: Column(
                  children: [
                    AutoSizeText(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Arcu, et mauris ut dui. Ultrices dui pellentesque netus eget aliquam enim leo. Tempor dignissim enim massa tellus. Netus purus sem arcu cras velit. Vulputate malesuada feugiat enim, elit. Volutpat leo elit.",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                      maxLines: 100,
                    ),
                    SizedBox(height: height / 86,),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText(
                          "Baca Juga:",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                          maxLines: 100,
                        ),
                        SizedBox(width: width/86,),
                        Container(
                          width: width / 1.6,
                          child: AutoSizeText(
                            "Menurut Dermatologi, Kandungan Makeup yang Disarankan, Pemutih Wajah Alami",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                                color: Color(0xffC6AB74))),
                            maxLines: 5,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              Divider(thickness: 6,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Row(
                          children: [
                            1,2,3
                          ].map((e) {
                            return CircularProfileAvatar(
                              '',
                              radius: height * 0.028,
                              borderColor: Colors.white,
                              // borderWidth: height * 0.008,
                              cacheImage: true,
                              child: SvgPicture.asset("assets/image/image_profile.svg"),
                              // child: Container(
                              //   decoration: BoxDecoration(
                              //     color: Colors.amber
                              //   ),
                              // ),
                            );
                          }).toList(),
                        ),
                        SizedBox(width: width / 86,),
                        AutoSizeText(
                          "+6 Likes",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                              color: Colors.grey)),
                          maxLines: 1,
                        ),
                      ],
                    ),
                    Divider(thickness: 2,),
                    SizedBox(width: width / 86,),
                    Row(
                      children: [
                        Row(
                          children: [
                            Icon(Icons.favorite_border, color: Colors.grey,),
                            SizedBox(width: width / 86,),
                            AutoSizeText(
                              "9",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                                  color: Colors.grey)),
                              maxLines: 1,
                            ),
                          ],
                        ),
                        SizedBox(width: width / 18,),
                        Row(
                          children: [
                            Icon(Icons.chat_bubble_outline, color: Colors.grey,),
                            SizedBox(width: width / 86,),
                            AutoSizeText(
                              "5",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                                  color: Colors.grey)),
                              maxLines: 1,
                            ),
                          ],
                        ),
                        SizedBox(width: width / 18,),
                        Row(
                          children: [
                            Icon(Icons.link, color: Colors.grey,),
                            SizedBox(width: width / 86,),
                            AutoSizeText(
                              "10",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                                  color: Colors.grey)),
                              maxLines: 1,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: height / 18,)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
