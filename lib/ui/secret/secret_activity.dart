import 'dart:convert';
import 'package:ecommerce_reborn/model/tips.dart';
import 'package:ecommerce_reborn/ui/cart/cart_activity.dart';
import 'package:ecommerce_reborn/ui/detail/article_detail_activity.dart';
import 'package:ecommerce_reborn/ui/notification/notification_activity.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/bloc/article_bloc.dart';
import 'package:ecommerce_reborn/utils/links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SecretActivity extends StatefulWidget {
  @override
  _SecretActivityState createState() => _SecretActivityState();
}

class _SecretActivityState extends State<SecretActivity> {
  bool isLanguage = false;
  ScrollController _scrollViewController;
  ScrollController _controller;
  ArticleBloc articleBloc;

  Future<List<Tips>> future;
  List<Tips> tips;
  Future<List<Tips>> getDataTips() async{
    var data = await http.get(Links.mainUrl+"/api/tips/offset=0&limit=3");
    var jsonData = json.decode(data.body);
    var _data = jsonData['results'] as List;
    // print(jsonData['results']);

    // for(var val in jsonData['results']){
    //   Tips tip = Tips(
    //     val['id'],
    //     val['name'],
    //     val['image'],
    //     val['desc'],
    //   );
    //   tips.add(tip);
    // }
    // return tips;

    return _data
        .map((items) =>
        Tips(
          items['id'],
          items['name'],
          items['image'],
          items['desc'],
        )).toList();
  }

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    future = getDataTips();
    super.initState();
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    _controller.dispose();
    articleBloc.close();
    super.dispose();
  }

  int index = 0;
  int currentIndex;

  @override
  Widget build(BuildContext context) {
    articleBloc = BlocProvider.of<ArticleBloc>(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          elevation: 0,
          toolbarHeight: height / 14,
          backgroundColor: Colors.white,
          flexibleSpace: SafeArea(
            child: Column(
              children: [
                Container(
                  width: width,
                  height: height / 14,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: height / 20,
                            decoration: BoxDecoration(
                                color: Color(0xffF0F0F5),
                                borderRadius: BorderRadius.circular(6)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8),
                              child: Row(
                                children: [
                                  Icon(Icons.search, color: Color(0xffAFB1BD),),
                                  SizedBox(width: width / 32,),
                                  AutoSizeText(
                                    (isLanguage != true)?"Cari Produk":"Search Product",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xffAFB1BD))),
                                    maxLines: 1,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: width / 48,),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    child: NotificationActivity()));
                          },
                          child: Stack(
                            children: [
                              Icon(
                                Icons.notifications_none,
                                size: width / 14,
                                color: Color(0xff707070),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  width: width / 24,
                                  height: width / 24,
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      shape: BoxShape.circle,
                                      border: Border.all(color: Colors.white)
                                  ),
                                  child: Center(
                                    child: AutoSizeText(
                                      "99",
                                      style: GoogleFonts.poppins(
                                          textStyle: TextStyle(
                                              fontSize: 8,
                                              fontWeight:
                                              FontWeight.w500, color: Colors.white)),
                                      minFontSize: 0,
                                      maxLines: 1,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(width: width / 48,),
                        GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    child: CartActivity()));
                          },
                          child: Stack(
                            children: [
                              Icon(
                                Icons.shopping_cart,
                                size: width / 14,
                                color: Color(0xff707070),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  width: width / 24,
                                  height: width / 24,
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      shape: BoxShape.circle,
                                      border: Border.all(color: Colors.white)
                                  ),
                                  child: Center(
                                    child: AutoSizeText(
                                      "1",
                                      style: GoogleFonts.poppins(
                                          textStyle: TextStyle(
                                              fontSize: 8,
                                              fontWeight:
                                              FontWeight.w500, color: Colors.white)),
                                      minFontSize: 0,
                                      maxLines: 1,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: SingleChildScrollView(
          controller: _controller,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: ArticleDetailActivity()));
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: width,
                      height: height / 3,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/image/image_product_beauty.png"),
                              fit: BoxFit.cover
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: AutoSizeText(
                        (isLanguage != true)?"COMES FROM WITHIN":"Search Product",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 28, fontWeight: FontWeight.w600)),
                        maxLines: 1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: AutoSizeText(
                        (isLanguage != true)?"Mulailah hari dengan kebahagiaan, dan membuat cerita yang baru melalui versi terbaik kamu. ":"Search Product",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 4,
                      ),
                    ),
                    SizedBox(height: height / 86,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: AutoSizeText(
                        (isLanguage != true)?"7 Menit yang lalu":"Search Product",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                        maxLines: 4,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: height / 86,),
              Divider(thickness: 4,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true)?"Secret Artikel":"Best Seller",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              SizedBox(height: height / 86,),
              BlocBuilder<ArticleBloc, ArticleState>(
                builder: (_,state){
                  if(state is ArticleUninitialized){
                    return Center(
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }else{
                    ArticleLoaded articleLoaded = state as ArticleLoaded;
                    index = articleLoaded.articles.length;
                    return ListView.builder(
                        controller: _controller,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: (articleLoaded.hasReachedMax) ? articleLoaded.articles.length :
                        articleLoaded.articles.length,
                        itemBuilder: (_,index){
                          return (articleLoaded.articles[index].about != null)?Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      height: height / 8,
                                      width: width / 4,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(12),
                                          image: DecorationImage(
                                              image: NetworkImage(Links.mainUrl +
                                                  articleLoaded.articles[index].image),
                                              fit: BoxFit.cover
                                          )
                                      ),
                                    ),
                                    SizedBox(width: width / 86,),
                                    Container(
                                      width: width / 2,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          AutoSizeText(
                                            "Kecantikan",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Color(0xff907135))),
                                            maxLines: 1,
                                          ),
                                          SizedBox(height: height / 86,),
                                          AutoSizeText(
                                            articleLoaded.articles[index].about,
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                            maxLines: 2,
                                          ),
                                          AutoSizeText(
                                            articleLoaded.articles[index].date,
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                                            maxLines: 1,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Divider(thickness: 0.5,),
                              ],
                            ),
                          ):Container();
                        }
                    );
                  }
                },
              ),
              // SingleChildScrollView(
              //   controller: _scrollViewController,
              //   physics: NeverScrollableScrollPhysics(),
              //   child: Column(
              //     children: [
              //       1,2,3,4,5
              //     ].map((e) {
              //       return Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
              //         child: Column(
              //           children: [
              //             Row(
              //               children: [
              //                 Container(
              //                   height: height / 8,
              //                   width: width / 4,
              //                   decoration: BoxDecoration(
              //                       borderRadius: BorderRadius.circular(12),
              //                       image: DecorationImage(
              //                           image: AssetImage("assets/image/image_product_beauty.png")
              //                       )
              //                   ),
              //                 ),
              //                 SizedBox(width: width / 86,),
              //                 Container(
              //                   width: width / 2,
              //                   child: Column(
              //                     crossAxisAlignment: CrossAxisAlignment.start,
              //                     mainAxisAlignment: MainAxisAlignment.center,
              //                     children: [
              //                       AutoSizeText(
              //                         (isLanguage != true)?"Kecantikan":"Beauty",
              //                         style: GoogleFonts.poppins(textStyle:
              //                         TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: Color(0xff907135))),
              //                         maxLines: 1,
              //                       ),
              //                       SizedBox(height: height / 86,),
              //                       AutoSizeText(
              //                         (isLanguage != true)?"5 Produk The Ordinary yang Cocok Bagi Pemilik Kulit Keriput":"Beauty",
              //                         style: GoogleFonts.poppins(textStyle:
              //                         TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
              //                         maxLines: 2,
              //                       ),
              //                       AutoSizeText(
              //                         "11 Oktober 2020",
              //                         style: GoogleFonts.poppins(textStyle:
              //                         TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
              //                         maxLines: 1,
              //                       ),
              //                     ],
              //                   ),
              //                 ),
              //               ],
              //             ),
              //             Divider(thickness: 2,),
              //           ],
              //         ),
              //       );
              //     }).toList(),
              //   ),
              // ),
              SizedBox(height: height / 86,),
              (index != currentIndex)?FlatButton(
                onPressed: (){
                  setState(() {
                    currentIndex = index;
                  });
                  articleBloc.add(ArticleEvent());
                },
                child: Center(
                  child: Container(
                    width: width / 2,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Center(
                        child: Text("Load More"),
                      ),
                    ),
                  ),
                ),
              ):Container(),
              SizedBox(height: height / 72,),
              Divider(thickness: 4,),
              SizedBox(height: height / 72,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true)?"Tips - Tips Hari Ini":"Glossary Today",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              Container(
                width: width,
                height: height / 2.6,
                child: FutureBuilder(
                  future: future,
                  builder: (BuildContext context, AsyncSnapshot snapshot){
                    if(snapshot.hasData){
                      return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          physics: BouncingScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (_, index){
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8),
                              child: Container(
                                height: height / 2.8,
                                width: width / 1.8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Stack(
                                      children: [
                                        Container(
                                          height: height / 5.2,
                                          width: width / 1.8,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: NetworkImage(Links.mainUrl +
                                                      snapshot.data[index].image),
                                                  fit: BoxFit.cover
                                              ),
                                              borderRadius: BorderRadius.circular(8)
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: height / 86,),
                                    AutoSizeText(
                                      snapshot.data[index].name,
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
                                      maxLines: 1,
                                    ),
                                    AutoSizeText(
                                      snapshot.data[index].desc,
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                                      maxLines: 4,
                                    ),
                                    SizedBox(height: height / 52,),
                                    AutoSizeText(
                                      (isLanguage != true)?"Baca Lebih Banyak":"Read More",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
                                          color: Color(0xffC6AB74))),
                                      maxLines: 4,
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                      );
                    }else{
                      return Center(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  },
                ),
              ),
              // SingleChildScrollView(
              //   scrollDirection: Axis.horizontal,
              //   physics: BouncingScrollPhysics(),
              //   child: Row(
              //     children: [
              //       1,2,3,4
              //     ].map((e) {
              //       return Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 8),
              //         child: Container(
              //           height: height / 2.8,
              //           width: width / 1.8,
              //           child: Column(
              //             crossAxisAlignment: CrossAxisAlignment.start,
              //             children: [
              //               Stack(
              //                 children: [
              //                   Container(
              //                     height: height / 5.2,
              //                     width: width / 1.8,
              //                     decoration: BoxDecoration(
              //                         image: DecorationImage(
              //                             image: AssetImage("assets/image/banner_glossary.png"),
              //                             fit: BoxFit.cover
              //                         ),
              //                         borderRadius: BorderRadius.circular(8)
              //                     ),
              //                   ),
              //                 ],
              //               ),
              //               SizedBox(height: height / 86,),
              //               AutoSizeText(
              //                 (isLanguage != true)?"Alergi":"Glossary Today",
              //                 style: GoogleFonts.poppins(textStyle:
              //                 TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
              //                 maxLines: 1,
              //               ),
              //               AutoSizeText(
              //                 (isLanguage != true)?"Reaksi sistem kekebalan tubuh terhadap sesuatu subtansi yang bersentuhan...":"Glossary Today",
              //                 style: GoogleFonts.poppins(textStyle:
              //                 TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
              //                 maxLines: 4,
              //               ),
              //               SizedBox(height: height / 52,),
              //               AutoSizeText(
              //                 (isLanguage != true)?"Baca Lebih Banyak":"Read More",
              //                 style: GoogleFonts.poppins(textStyle:
              //                 TextStyle(fontSize: 12, fontWeight: FontWeight.w400,
              //                     color: Color(0xffC6AB74))),
              //                 maxLines: 4,
              //               ),
              //             ],
              //           ),
              //         ),
              //       );
              //     }).toList(),
              //   ),
              // ),
              SizedBox(height: height / 72,),
            ],
          ),
        ),
      ),
    );
  }
}
