import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SettingBalancePage extends StatefulWidget {
  int indexPage;

  SettingBalancePage(this.indexPage);

  @override
  _SettingBalancePageState createState() => _SettingBalancePageState(indexPage);
}

class _SettingBalancePageState extends State<SettingBalancePage>
    with SingleTickerProviderStateMixin{
  int indexPage;

  _SettingBalancePageState(this.indexPage);

  bool isLanguage = false;

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      initialIndex: indexPage,
      length: 3,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: DefaultTabController(
        length: _tabController.length,
        child: Scaffold(
          body: Column(
            children: [
              SizedBox(height: height / 62,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                  ),
                  Center(
                    child: AutoSizeText(
                      (isLanguage != true)?"Saldo Reborn":"Balance Reborn",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                      maxLines: 1,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.add, color: Colors.transparent,),
                  )
                ],
              ),
              SizedBox(height: height / 62,),
              Container(
                width: width,
                height: height / 16,
                decoration: BoxDecoration(
                  color: Color(0xffEBEBEB)
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Total Saldo":"Total Balance",
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        (isLanguage != true)?"Rp 500.000,-":"Detail Referral",
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16,
                            fontWeight: FontWeight.w600)),
                        maxLines: 1,
                      ),
                      Container(
                        width: width / 4.8,
                        height: height / 28,
                        decoration: BoxDecoration(
                            color: Color(0xffC6AB74),
                          borderRadius: BorderRadius.circular(4)
                        ),
                        child: Center(
                          child: AutoSizeText(
                            (isLanguage != true)?"Cairkan":"Take Money",
                            style: GoogleFonts.poppins(textStyle: TextStyle(
                              fontSize: 8,
                                fontWeight: FontWeight.w400, color: Colors.white)),
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                child: TabBar(
                  controller: _tabController,
                  indicatorColor: Color(0xffC6AB74),
                  labelColor: Colors.black,
                  unselectedLabelColor: Colors.grey,
                  labelPadding: EdgeInsets.only(top: height * 0.015),
                  indicatorWeight: 4,
                  tabs: [
                    Tab(child:
                      Center(
                        child: AutoSizeText(
                          (isLanguage != true)?"Detail Referral":"Detail Referral",
                          style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w500)),
                          maxLines: 2,
                        ),
                      ),
                    ),
                    Tab(child:
                      AutoSizeText(
                        (isLanguage != true)?"Detail Saldo":"Detail Balance",
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                          fontWeight: FontWeight.w500)),
                        maxLines: 2,
                      ),
                    ),
                    Tab(child:
                      Column(
                        children: [
                          AutoSizeText(
                            (isLanguage != true)?"Detail":"Detail",
                            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            (isLanguage != true)?"Withdrawal":"Withdrawal",
                            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: width,
                          height: height / 16,
                          decoration: BoxDecoration(
                              color: Color(0xffEBEBEB)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                AutoSizeText(
                                  (isLanguage != true)?"Total Referral":"6 Member",
                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                                      fontWeight: FontWeight.w500)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  (isLanguage != true)?"6 Member":"Detail Referral",
                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                                      fontWeight: FontWeight.w500)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: [1,2,3,4,5,6].map((e) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            SizedBox(height: height / 64,),
                                            AutoSizeText(
                                              (isLanguage != true)?"Username":"Username",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                  fontWeight: FontWeight.w400, color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              "denidc",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                                                  fontWeight: FontWeight.w400)),
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            AutoSizeText(
                                              (isLanguage != true)?"Email":"Email",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                  fontWeight: FontWeight.w400, color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              "denidc@gmail.com".substring(0, 4) + "****@gmail.com",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                                                  fontWeight: FontWeight.w400)),
                                              maxLines: 2,
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            AutoSizeText(
                                              (isLanguage != true)?"Tgl Bergabung":"Joined Date",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                  fontWeight: FontWeight.w400, color: Colors.grey)),
                                              maxLines: 1,
                                            ),
                                            AutoSizeText(
                                              "15 Febuari 2020",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                                                  fontWeight: FontWeight.w400)),
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: height / 64,),
                                    Divider(thickness: 2,),
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),

                    Column(
                      children: [
                        Container(
                          width: width,
                          height: height / 32,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                AutoSizeText(
                                  (isLanguage != true)?"Username":"Account",
                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                      fontWeight: FontWeight.w500, color: Colors.grey)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  (isLanguage != true)?"Status":"Status",
                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                      fontWeight: FontWeight.w500, color: Colors.grey)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: [1,2,3,4,5,6].map((e) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            AutoSizeText(
                                              "denidc",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                                                  fontWeight: FontWeight.w400)),
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: [
                                            Row(
                                              children: [
                                                AutoSizeText(
                                                  (isLanguage != true)?"Terverifikasi ":"Verified ",
                                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12,
                                                      fontWeight: FontWeight.w400, color: Color(0xffC6AB74))),
                                                  maxLines: 1,
                                                ),
                                                Icon(Icons.check_circle_outline, color: Color(0xffC6AB74), size: 16,)
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                AutoSizeText(
                                                  (isLanguage != true)?"Total Belanja :":"Total Shopping ",
                                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                      fontWeight: FontWeight.w500, color: Colors.grey)),
                                                  maxLines: 1,
                                                ),
                                                AutoSizeText(
                                                  "Rp 200K",
                                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                      fontWeight: FontWeight.w500)),
                                                  maxLines: 1,
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                AutoSizeText(
                                                  (isLanguage != true)?"Bonus :":"Bonus ",
                                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                      fontWeight: FontWeight.w500, color: Colors.grey)),
                                                  maxLines: 1,
                                                ),
                                                AutoSizeText(
                                                  "+Rp 50K",
                                                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11,
                                                      fontWeight: FontWeight.w500)),
                                                  maxLines: 1,
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: height * 0.01,),
                                    Divider(thickness: 2,),
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [

                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
