import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoAddressPage extends StatefulWidget {
  @override
  _InfoAddressPageState createState() => _InfoAddressPageState();
}

class _InfoAddressPageState extends State<InfoAddressPage> {
  bool isLanguage = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: height / 62,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                ),
                Center(
                  child: AutoSizeText(
                    "Deni",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                ),
                IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.add, color: Colors.transparent,),
                ),
              ],
            ),
            SizedBox(height: height / 62,),
            Divider(thickness: 24,),
            Center(
              child: AutoSizeText(
                (isLanguage != true)?"Alamat Utama":"Primary Address",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                maxLines: 1,
              ),
            ),
            Divider(thickness: 12,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Nama Penerima":"Primary Address",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        (isLanguage != true)?"Jhon":"Primary Address",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Divider(thickness: 12,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Alamat Sebagai":"Primary Address",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        (isLanguage != true)?"Rumah":"Primary Address",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Divider(thickness: 16,),
            SizedBox(height: height / 62,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    (isLanguage != true)?"Alamat":"Address",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                    maxLines: 1,
                  ),
                  SizedBox(height: height / 86,),
                  AutoSizeText(
                    "Kecamatan Sukodono, Kabupaten Sidoarjo, Jawa Timur, Indonesia, 61258",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    maxLines: 2,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 16,
        decoration: BoxDecoration(
          color: Colors.red
        ),
        child: Center(
          child: AutoSizeText(
            (isLanguage != true)?"Hapus":"Delete",
            style: GoogleFonts.poppins(textStyle:
            TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.white)),
            maxLines: 2,
          ),
        ),
      ),
    );
  }
}
