import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/filter/filter_order_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SettingOrderPage extends StatefulWidget {
  @override
  _SettingOrderPageState createState() => _SettingOrderPageState();
}

class _SettingOrderPageState extends State<SettingOrderPage> {
  bool isLanguage = false;
  ScrollController _scrollViewController;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  double ratingCount = 5;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: (){

            },
            icon: Icon(Icons.add, color: Colors.transparent,),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true)?"Order":"Change Address",
            style: GoogleFonts.poppins(textStyle:
            TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black)),
            maxLines: 1,
          ),
        ),
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
        ),
      ),
      body: Stack(
        children: [
          SmartRefresher(
            enablePullDown: true,
            enablePullUp: false,
            header: WaterDropMaterialHeader(
              color: Colors.grey,
              backgroundColor: Colors.white,
            ),
            footer: CustomFooter(
              builder: (BuildContext context,LoadStatus mode){
                Widget body ;
                if(mode==LoadStatus.idle){
                  body =  Text("pull up load");
                }
                else if(mode==LoadStatus.loading){
                  body =  CupertinoActivityIndicator();
                }
                else if(mode == LoadStatus.failed){
                  body = Text("Load Failed!Click retry!");
                }
                else if(mode == LoadStatus.canLoading){
                  body = Text("release to load more");
                }
                else{
                  body = Text("No more Data");
                }
                return Container(
                  height: 55.0,
                  child: Center(child:body),
                );
              },
            ),
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  "Payment Verification","Dibatalkan","Diterima"
                ].map((e) {
                  return Padding(
                    padding: const EdgeInsets.all(8),
                    child: Container(
                      width: width,
                      height: height / 2.7,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    AutoSizeText(
                                      "RMUDDCDZI",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                                      maxLines: 1,
                                    ),
                                    Container(
                                      width: (e == "Payment Verification")?width / 2.2:width/3.4,
                                      decoration: BoxDecoration(
                                        color: Color(0xffF2F2F2),
                                        borderRadius: BorderRadius.circular(18),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(4),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Icon(MaterialCommunityIcons.circle, size: 16,
                                              color: Color(0xffDDC58E),),
                                            AutoSizeText(
                                              " "+e,
                                              style: GoogleFonts.poppins(textStyle:
                                              TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: height / 86,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    AutoSizeText(
                                      "09 Nov 2020",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                      maxLines: 1,
                                    ),
                                  ],
                                ),
                                SizedBox(height: height / 86,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        AutoSizeText(
                                          "Total Price : ",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                        AutoSizeText(
                                          "Rp 319.000",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Icon(Icons.shopping_cart, color: Colors.grey,),
                                        AutoSizeText(
                                          "1 item Pembelian",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(thickness: 2,),
                          SizedBox(height: height / 86,),
                          Row(
                            children: [
                              Container(
                                width: width / 4,
                                height: height / 8,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    image: DecorationImage(
                                        image: AssetImage("assets/image/image_product_beauty.png")
                                    )
                                ),
                              ),
                              SizedBox(width: width / 86,),
                              Container(
                                width: width / 2.8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    AutoSizeText(
                                      "SAVE 20%",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14, fontWeight: FontWeight.w600,
                                          color: Color(0xffBE9639))),
                                      maxLines: 1,
                                    ),
                                    SizedBox(height: height / 86,),
                                    AutoSizeText(
                                      "Byoote",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                                      maxLines: 2,
                                    ),
                                    AutoSizeText(
                                      "Byoote collagen skin",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                      maxLines: 2,
                                    ),
                                    AutoSizeText(
                                      "Size: 50 gr",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                      maxLines: 2,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: height / 86,),
                          Divider(thickness: 2,),
                          SizedBox(height: height / 86,),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "Vied Detail",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                  maxLines: 2,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: height / 48),
              child: GestureDetector(
                onTap: (){
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.rightToLeft,
                          child: FilterOrderPage()));
                },
                child: Container(
                  width: width / 4,
                  height: height / 22,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(18),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey
                            .withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: Offset(0,
                            3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Octicons.settings, size: 18, color: Color(0xffBE9639),),
                      AutoSizeText(
                        " Filter",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight:
                                FontWeight.w500, color: Colors.grey)),
                        maxLines: 1,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
