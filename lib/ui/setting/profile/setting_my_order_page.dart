import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/cart/cart_activity.dart';
import 'package:ecommerce_reborn/ui/detail/resi_detail_activity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingMyOrderPage extends StatefulWidget {
  @override
  _SettingMyOrderPageState createState() => _SettingMyOrderPageState();
}

class _SettingMyOrderPageState extends State<SettingMyOrderPage>
    with SingleTickerProviderStateMixin{
  TabController _tabController;
  ScrollController _scrollControllerPacked = ScrollController();
  ScrollController _scrollControllerSent = ScrollController();

  bool isNoOrders = false;
  bool isLiked = false;
  bool notInterested = false;
  List<bool> accepted = [false, false];

  int tabPage = 0;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  _launchURL() async {
    const url = 'https://lear.page.link/try';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    _tabController = TabController(
      length: 3,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollControllerPacked.dispose();
    _scrollControllerSent.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: height / 7.5,
        backgroundColor: Colors.white,
        leading: Container(),
        flexibleSpace: SafeArea(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                  ),
                  AutoSizeText(
                    "Pesanan Saya",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.add, color: Colors.transparent, size: 32,),
                  ),
                ],
              ),
              TabBar(
                controller: _tabController,
                indicatorColor: Color(0xffC6AB74),
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.black,
                onTap: (v){
                  setState(() {
                    tabPage = v;
                  });
                },
                tabs: [
                  Tab(child: AutoSizeText(
                    "Dikemas",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),),
                  Tab(child:
                  AutoSizeText(
                    "Dikirim",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  )),
                  Tab(child:
                  AutoSizeText(
                    "Diterima",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  )),
                ],
              ),
            ],
          ),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: TabBarView(
          controller: _tabController,
          children: [
            (isNoOrders != true)?SingleChildScrollView(
              controller: _scrollControllerPacked,
              child: Column(
                children: [
                  SizedBox(height: height / 48,),
                  Container(
                    width: width / 3,
                      height: height / 6,
                      child: Image.asset("assets/image/icon_packed.png")),
                  SizedBox(height: height / 86,),
                  AutoSizeText(
                    "Barang Anda pesan",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    "Sedang diporses",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    maxLines: 1,
                  ),
                  ListView.builder(
                    controller: _scrollControllerPacked,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 2,
                      itemBuilder: (_,index){
                        return Padding(
                          padding: EdgeInsets.all(width / 48),
                          child: Container(
                            width: width,
                            height: height / 12,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      AutoSizeText(
                                        "Nomor pesanan",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "RMUDDCDZI",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      AutoSizeText(
                                        "Standar",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "Regular",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      AutoSizeText(
                                        "Status",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "Dikemas",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                  )
                ],
              ),
            ):Column(
              children: [
                SizedBox(height: height / 18,),
                Container(
                    width: width / 3,
                    height: height / 6,
                    child: Image.asset("assets/image/icon_no_orders.png")),
                SizedBox(height: height / 86,),
                AutoSizeText(
                  "Belum ada pesanan",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                  maxLines: 1,
                ),
              ],
            ),
            SingleChildScrollView(
              controller: _scrollControllerSent,
              child: Column(
                children: [
                  SizedBox(height: height / 48,),
                  Container(
                      width: width / 3,
                      height: height / 6,
                      child: Image.asset("assets/image/icon_sent.png")),
                  SizedBox(height: height / 48,),
                  ListView.builder(
                      controller: _scrollControllerSent,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: 2,
                      itemBuilder: (_,index){
                        return Padding(
                          padding: EdgeInsets.all(width / 48),
                          child: Column(
                            children: [
                              Container(
                                width: width,
                                height: height / 12,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          AutoSizeText(
                                            "Nomor pesanan",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                            maxLines: 1,
                                          ),
                                          AutoSizeText(
                                            "RMUDDCDZI",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                            maxLines: 1,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          AutoSizeText(
                                            "Standar",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                            maxLines: 1,
                                          ),
                                          AutoSizeText(
                                            "Regular",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                            maxLines: 1,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          AutoSizeText(
                                            "Status",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                            maxLines: 1,
                                          ),
                                          AutoSizeText(
                                            "Dikirim",
                                            style: GoogleFonts.poppins(textStyle:
                                            TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                            maxLines: 1,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: height/86,),
                              Row(
                                children: [
                                  AutoSizeText(
                                    "No Resi. ",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                                    maxLines: 1,
                                  ),
                                  AutoSizeText(
                                    "1234567890",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 14, fontWeight: FontWeight.w700)),
                                    maxLines: 1,
                                  ),
                                ],
                              ),
                              Divider(thickness: 0.5,),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.rightToLeft,
                                          child: ResiDetailActivity()));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(color: Colors.grey),
                                                  shape: BoxShape.circle
                                              ),
                                              child: Icon(Icons.fiber_manual_record,
                                                color: Color(0xff00B212), size: 18,)),
                                          SizedBox(width: width * 0.01,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: width / 1.3,
                                                child: AutoSizeText(
                                                  "[ SURABAYA ] DELIVERED TO [DENI | 27-11-2020 | SURABAYA]",
                                                  style: GoogleFonts.poppins(textStyle:
                                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                                                      color: Color(0xff00B212))),
                                                  maxLines: 2,
                                                ),
                                              ),
                                              AutoSizeText(
                                                "27-11-2020 15 : 30",
                                                style: GoogleFonts.poppins(textStyle:
                                                TextStyle(fontSize: 10, fontWeight: FontWeight.w400)),
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Icon(Icons.chevron_right, color: Colors.grey, size: 42,)
                                    ],
                                  ),
                                ),
                              ),
                              Divider(thickness: 0.5,),
                            ],
                          ),
                        );
                      }
                  ),
                ],
              ),
            ),
            ListView.builder(
              itemCount: accepted.length,
                itemBuilder: (_,index){
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: width / 48),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: width / 5,
                              height: height / 8,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("assets/image/product.png")
                                )
                              ),
                            ),
                            SizedBox(width: width / 86,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  height: height / 42,
                                  child: AutoSizeText(
                                    "SAVE 20%",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 12, fontWeight: FontWeight.w700,
                                        color: Color(0xffBE9639))),
                                    maxLines: 1,
                                    minFontSize: 0,
                                  ),
                                ),
                                SizedBox(height: height / 86,),
                                Container(
                                  height: height / 42,
                                  child: AutoSizeText(
                                    "Byoote",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 12,
                                            fontWeight:
                                            FontWeight.w700)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                                Container(
                                  height: height / 44,
                                  child: AutoSizeText(
                                    "Byoote gluthateon",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 11,
                                            fontWeight:
                                            FontWeight.w400)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                                Container(
                                  height: height / 44,
                                  child: AutoSizeText(
                                    "Size: 50 gr",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 11,
                                            fontWeight:
                                            FontWeight.w400,
                                            color: Colors.grey)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: height / 86,),
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "No. Pesanan",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "RMUDDCDZI",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w700)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                            SizedBox(width: width / 24,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "Total produk",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "1 pcs",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w700)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                            SizedBox(width: width / 24,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "Total Price : ",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                                AutoSizeText(
                                  "Rp 319.000",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w700)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: height/ 86,),
                        Align(
                          alignment: (accepted[index])?Alignment.bottomRight:Alignment.bottomLeft,
                          child: GestureDetector(
                            onTap: (){
                              if(accepted[index] != true){
                                setState(() {
                                  accepted[index] = true;
                                });
                              }else{
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: CartActivity()));
                              }
                            },
                            child: Container(
                              width: width / 2.4,
                              height: height / 16,
                              decoration: BoxDecoration(
                                color: Color(0xffBE9639),
                                borderRadius: BorderRadius.circular(4),
                              ),
                              child: Center(
                                child: AutoSizeText(
                                  (accepted[index])?"Beli Lagi":"Diterima",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 16, fontWeight: FontWeight.w500,
                                      color: Colors.white)),
                                  maxLines: 1,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Divider(thickness: 4,),
                        (index == 0 && notInterested != true)?
                        (isLiked)?Column(
                          children: [
                            SizedBox(height: height / 86,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.transparent),
                                    shape: BoxShape.circle
                                  ),
                                    child: Padding(
                                      padding: EdgeInsets.all(width * 0.01),
                                      child: Icon(Icons.close, color: Colors.transparent, size: 12,),
                                    )),
                                AutoSizeText(
                                  "Berikan nilai 5 bintang di playstore?",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                                  maxLines: 1,
                                ),
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      notInterested = true;
                                    });
                                  },
                                  child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(color: Colors.grey),
                                          shape: BoxShape.circle
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.all(width * 0.01),
                                        child: Icon(Icons.close, color: Colors.grey, size: 12,),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(height: height / 86,),
                            GestureDetector(
                              onTap: _launchURL,
                              child: Container(
                                width: width,
                                height: height / 18,
                                decoration: BoxDecoration(
                                  color: Color(0xffBE9639),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Center(
                                  child: AutoSizeText(
                                    "Nilai Sekarang",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 16, fontWeight: FontWeight.w500,
                                        color: Colors.white)),
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: height / 86,),
                            Divider(thickness: 4,)
                          ],
                        ):Column(
                          children: [
                            SizedBox(height: height / 86,),
                            Center(
                              child: AutoSizeText(
                                "Apakah Anda menyukai Reborn?",
                                style: GoogleFonts.poppins(textStyle:
                                TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                                maxLines: 1,
                              ),
                            ),
                            SizedBox(height: height / 86,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      notInterested = true;
                                    });
                                  },
                                  child: Container(
                                    width: width / 2.2,
                                    height: height / 18,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                      border: Border.all(color: Colors.black)
                                    ),
                                    child: Center(
                                      child: AutoSizeText(
                                        "Tidak",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                                        maxLines: 1,
                                      ),
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      isLiked = true;
                                    });
                                  },
                                  child: Container(
                                    width: width / 2.2,
                                    height: height / 18,
                                    decoration: BoxDecoration(
                                      color: Color(0xffBE9639),
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    child: Center(
                                      child: AutoSizeText(
                                        "Ya",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 16, fontWeight: FontWeight.w500,
                                            color: Colors.white)),
                                        maxLines: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: height / 86,),
                            Divider(thickness: 4,)
                          ],
                        ):Container(),
                      ],
                    ),
                  );
                }
            ),
          ],
        ),
      ),
    );
  }
}
