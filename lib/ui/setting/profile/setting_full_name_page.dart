import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class SettingFullNamePage extends StatefulWidget {
  @override
  _SettingFullNamePageState createState() => _SettingFullNamePageState();
}

class _SettingFullNamePageState extends State<SettingFullNamePage> {
  bool isLanguage = false;

  TextEditingController _fullNameText = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(height: height / 46,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                    ),
                    Center(
                      child: AutoSizeText(
                        (isLanguage != true)?"Ubah Nama":"Change Name",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        maxLines: 1,
                      ),
                    ),
                    IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.add, color: Colors.transparent,),
                    )
                  ],
                ),
                SizedBox(height: height / 46,),
                Divider(thickness: 2,),
                SizedBox(height: height / 48,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Masukkan Nama Lengkap":"Enter Full Name",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        maxLines: 1,
                      ),
                      Container(
                        height: height / 24,
                        child: TextField(
                          controller: _fullNameText,
                          keyboardType: TextInputType.streetAddress,
                          cursorColor: Colors.black,
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14)),
                          decoration: InputDecoration(
                            hintText: (isLanguage != true)?"Nama Lengkap":"Full Name",
                            hintStyle: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                            helperStyle: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 14)),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 2),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black, width: 2),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              child: Container(
                width: width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(6.0),
                  color: Color(0xffc6ab74),
                  child: InkWell(
                    onTap: (){
                      // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: SearchAddressMaps(isLanguage, Position(latitude: -7.3757489, longitude: 112.6902406)) ));
                    },
                    splashColor: Colors.white,
                    borderRadius: BorderRadius.circular(6.0),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        child: Text((isLanguage != true)?"Simpan":"Save", style: GoogleFonts.poppins(
                            textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        )
      ),
    );
  }
}
