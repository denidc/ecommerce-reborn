import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SettingStoredProductPage extends StatefulWidget {
  @override
  _SettingStoredProductPageState createState() => _SettingStoredProductPageState();
}

class _SettingStoredProductPageState extends State<SettingStoredProductPage> {
  bool isLanguage = false;
  ScrollController _scrollViewController;
  List<bool> isBookmark = [true, true, true, true, true, true, true, true, true, true];

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  double ratingCount = 5;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.add, color: Colors.transparent,),
          )
        ],
        title: Center(
          child: AutoSizeText(
            (isLanguage != true)?"Produk Tersimpan":"Change Address",
            style: GoogleFonts.poppins(textStyle:
            TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black)),
            maxLines: 1,
          ),
        ),
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: GridView.count(
          controller: _scrollViewController,
          shrinkWrap: true,
          crossAxisCount: 2,
          childAspectRatio: 3 / 5.8,
          children: [0,1,2,3,4,5,6,7,8,9].map((e) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Container(
                height: height / 2.1,
                width: width / 2.6,
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 12),
                          child: Container(
                            height: height / 5,
                            width: width / 2.6,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage("assets/image/product.png"),
                                    fit: BoxFit.fill
                                ),
                                borderRadius: BorderRadius.circular(8)
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: GestureDetector(
                            onTap: (){
                              setState(() {
                                isBookmark[e] = !isBookmark[e];
                              });
                            },
                            child: Container(
                              color: Colors.transparent,
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.all(width.round() / 86),
                                  child: Icon((isBookmark[e])?AntDesign.star:AntDesign.staro, size: 18,
                                    color: Color(0xffBE9639),),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Center(
                      child: AutoSizeText(
                        "Byoote",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ),
                    Center(
                      child: AutoSizeText(
                        "Byoote gluthateon",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                    ),
                    Center(
                      child: AutoSizeText(
                        "Rp 300.000",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RatingBar(
                          itemSize: 18,
                          initialRating: ratingCount,
                          ignoreGestures: true,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Color(0xffC6AB74),
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                        SizedBox(width: 4,),
                        Text(ratingCount.toString(), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                      ],
                    ),
                    SizedBox(height: height / 86,),
                    Container(
                      height: height / 24,
                      width: width / 2.6,
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffC6AB74)),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: Center(
                          child: AutoSizeText(
                            "ADD TO BAG",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xffC6AB74))),
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}