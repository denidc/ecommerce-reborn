import 'package:auto_size_text/auto_size_text.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_full_name_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_password_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_phone_number_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_username_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class SettingAccountPage extends StatefulWidget {
  @override
  _SettingAccountPageState createState() => _SettingAccountPageState();
}

class _SettingAccountPageState extends State<SettingAccountPage> {
  bool isLanguage = false;
  bool isGender = true;

  String birthDate = "";
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width / 28),
          child: Column(
            children: [
              SizedBox(height: height / 86,),
              Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Center(
                        child: AutoSizeText(
                          (isLanguage != true)?"Profil Saya":"My Profile",
                          style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                          child: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,)),
                    ],
                  )
                ],
              ),
              Divider(thickness: 2,),
              SizedBox(height: height / 46,),
              CircularProfileAvatar(
                '',
                radius: height * 0.05,
                borderColor: Colors.white,
                elevation: 5.0,
                // borderWidth: height * 0.008,
                cacheImage: true,
                child: SvgPicture.asset("assets/image/image_profile.svg"),
                // child: Container(
                //   decoration: BoxDecoration(
                //     color: Colors.amber
                //   ),
                // ),
              ),
              SizedBox(height: height / 68,),
              AutoSizeText(
                "Deni Dwi Candra",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 16,
                    fontWeight: FontWeight.w400)),
                maxLines: 1,
              ),
              SizedBox(height: height / 68,),
              Divider(thickness: 2,),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, PageTransition(type:
                  PageTransitionType.rightToLeft, child: SettingFullNamePage() ));
                },
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Nama Lengkap",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          AutoSizeText(
                            "Deni Dwi Candra",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          Icon(Icons.chevron_right)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, PageTransition(type:
                  PageTransitionType.rightToLeft, child: SettingUsernamePage() ));
                },
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Nama Pengguna",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          AutoSizeText(
                            "denidc",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          Icon(Icons.chevron_right)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        "Email",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        "denidc27@gmail.com",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w500, color: Colors.grey)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  SvgPicture.asset("assets/image/icon_email_verify.svg"),
                ],
              ),
              Divider(thickness: 2,),
              GestureDetector(

                onTap: ()async{
                  bool _result = await showDialog(
                      context: this.context,
                      builder: (ctx){
                        return new DialogGender(isLanguage, isGender);
                      }
                  );
                  setState(() {
                    isGender = _result;
                  });
                },
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Jenis Kelamin",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          AutoSizeText(
                            (isGender != null)?
                            (isGender != true)?"Perempuan": "Laki-Laki":
                            "Belum diatur",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500, color: (isGender != null)?Colors.black:Colors.grey)),
                            maxLines: 1,
                          ),
                          Icon(Icons.chevron_right,)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
              GestureDetector(
                onTap: (){
                  DatePicker.showDatePicker(context, showTitleActions: true,
                      onConfirm: (date) {
                    setState(() {
                      birthDate = date.toString().split(' ')[0];
                    });
                    print(date.toString().split(' ')[0]);
                  },
                      currentTime: DateTime(DateTime.now().year, DateTime.now().month,
                          DateTime.now().day),
                      locale: LocaleType.id,
                    maxTime: DateTime(DateTime.now().year, 12, 31)
                  );
                },
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Tanggal Lahir",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          AutoSizeText(
                            (birthDate != "")?birthDate:"Belum diatur",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500, color: (birthDate != "")?Colors.black:Colors.grey)),
                            maxLines: 1,
                          ),
                          Icon(Icons.chevron_right)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, PageTransition(type:
                  PageTransitionType.rightToLeft, child: SettingPhoneNumberPage() ));
                },
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Nomer Ponsel",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          AutoSizeText(
                            "Belum diatur",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500, color: Colors.grey)),
                            maxLines: 1,
                          ),
                          Icon(Icons.chevron_right,)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, PageTransition(type:
                  PageTransitionType.rightToLeft, child: SettingPasswordPage() ));
                },
                child: Container(
                  width: width,
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Password",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14,
                            fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      ),
                      Row(
                        children: [
                          AutoSizeText(
                            "********",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 14,
                                fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          Icon(Icons.chevron_right,)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Divider(thickness: 2,),
            ],
          ),
        ),
      ),
    );
  }
}

class DialogGender extends StatefulWidget {
  bool isLanguage;
  bool isGender;

  DialogGender(this.isLanguage, this.isGender);

  @override
  _DialogGenderState createState() => _DialogGenderState(isLanguage, isGender);
}

class _DialogGenderState extends State<DialogGender> {
  bool isLanguage;
  bool isGender;

  _DialogGenderState(this.isLanguage, this.isGender);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                onPressed: (){
                  Navigator.of(context, rootNavigator: true).pop(isGender);
                },
                icon: Icon(AntDesign.closecircleo, color: Colors.grey,),
              )
            ],
          ),
          Center(
            child: AutoSizeText(
              (isLanguage != true)?"Jenis Kelamin":"Gender",
              wrapWords: true,
              style: GoogleFonts.poppins(textStyle:
              TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
              maxLines: 1,
            ),
          ),
          SizedBox(height: height / 36,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: (){
                  setState(() {
                    isGender = false;
                  });
                  Navigator.of(context, rootNavigator: true).pop(false);
                },
                child: Container(
                  width: width / 4,
                  height: height / 6,
                  decoration: BoxDecoration(
                      color: (isGender != true)?Color(0xffFBF3E7):Colors.transparent,
                      border: Border.all(color: (isGender != true)?Color(0xffBEA268):Colors.grey),
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Ionicons.md_female, size: 80, color: (isGender != true)?Color(0xffBEA268):Colors.grey),
                      SizedBox(height: height / 68,),
                      Center(
                        child: AutoSizeText(
                          (isLanguage != true)?"Perempuan":"Female",
                          wrapWords: true,
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: (isGender != true)?Color(0xffBEA268):Colors.grey,)),
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: (){
                  setState(() {
                    isGender = true;
                  });
                  Navigator.of(context, rootNavigator: true).pop(true);
                },
                child: Container(
                  width: width / 4,
                  height: height /6,
                  decoration: BoxDecoration(
                      color: (isGender != true)?Colors.transparent:Color(0xffFBF3E7),
                      border: Border.all(color: (isGender != true)?Colors.grey:Color(0xffBEA268)),
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Ionicons.md_male, size: 80, color: (isGender != true)?Colors.grey:Color(0xffBEA268),),
                      SizedBox(height: height / 68,),
                      Center(
                        child: AutoSizeText(
                          (isLanguage != true)?"Laki-Laki":"Male",
                          wrapWords: true,
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: (isGender != true)?Colors.grey:Color(0xffBEA268))),
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height / 28,),
        ],
      ),
    );
  }
}


