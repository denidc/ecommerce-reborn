import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/google_maps/search_address_maps.dart';
import 'package:ecommerce_reborn/ui/setting/profile/info_address_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class SettingAddressPage extends StatefulWidget {
  @override
  _SettingAddressPageState createState() => _SettingAddressPageState();
}

class _SettingAddressPageState extends State<SettingAddressPage> {
  bool isLanguage = false;
  bool isNotEmpty = false;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: (isNotEmpty)?Column(
            children: [
              SizedBox(height: height / 62,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                  ),
                  Center(
                    child: AutoSizeText(
                      (isLanguage != true)?"Ubah Alamat":"Change Address",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.add, color: Colors.transparent,),
                  ),
                ],
              ),
              Divider(thickness: 2,),
              SizedBox(height: height / 24,),
              SvgPicture.asset("assets/image/image_maps.svg"),
              SizedBox(height: height / 28,),
              Center(
                child: AutoSizeText(
                  (isLanguage != true)?"Belum Ada Alamat Tersimpan":"Full Name",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              SizedBox(height: height / 62,),
              Center(
                child: Container(
                  width: width / 1.4,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Material(
                    borderRadius: BorderRadius.circular(6.0),
                    color: Color(0xffc6ab74),
                    child: InkWell(
                      onTap: (){
                        showModalBottomSheet<dynamic>(
                            isScrollControlled: true,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(16),
                                    topRight: Radius.circular(16))
                            ),
                            context: this.context,
                            builder: (ctx){
                              return PopUpSearchAddress();
                            }
                        );
                      },
                      splashColor: Colors.white,
                      borderRadius: BorderRadius.circular(6.0),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Text((isLanguage != true)?"Tambah Alamat":"Add Address", style: GoogleFonts.poppins(
                              textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ):Column(
            children: [
              SizedBox(height: height / 62,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                  ),
                  Center(
                    child: AutoSizeText(
                      (isLanguage != true)?"Ubah Alamat":"Change Address",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                  ),
                  IconButton(
                    onPressed: (){
                      showModalBottomSheet<dynamic>(
                          isScrollControlled: true,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(16),
                                  topRight: Radius.circular(16))
                          ),
                          context: this.context,
                          builder: (ctx){
                            return PopUpSearchAddress();
                          }
                      );
                    },
                    icon: Icon(Icons.add, color: Color(0xffDDC58E), size: 32,),
                  ),
                ],
              ),
              Divider(thickness: 2,),
              SizedBox(height: height / 48,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, PageTransition(type:
                        PageTransitionType.rightToLeft, child:
                        InfoAddressPage()));
                      },
                      child: Container(
                        width: width,
                        decoration: BoxDecoration(
                          color: Colors.transparent
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Icon(Icons.location_on, color: Color(0xffC6AB74),),
                                AutoSizeText(
                                  (isLanguage != true)?"Rumah":"Empty Bank Account",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                AutoSizeText(
                                  (isLanguage != true)?"Alamat Utama":"Empty Bank Account",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                                Icon(Icons.chevron_right, color: Color(0xffC6AB74),),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(thickness: 2,),
                    SizedBox(height: height / 62,),
                    AutoSizeText(
                      (isLanguage != true)?"Nama Penerima:":"Empty Bank Account",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                      maxLines: 1,
                    ),
                    AutoSizeText(
                      (isLanguage != true)?"Deni Dwi Candra":"Empty Bank Account",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                    SizedBox(height: height / 62,),
                    AutoSizeText(
                      (isLanguage != true)?"Alamat:":"Empty Bank Account",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                      maxLines: 1,
                    ),
                    AutoSizeText(
                      (isLanguage != true)?"Jl. Ambengan Batu I, Kec. Tambaksari, Kota SBY, Jawa Timur, No.25 Rt.01. Rw.02 Tambaksari Kota Surabaya Jawa TImur, Indonesia 12345":"Empty Bank Account",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                      maxLines: 4,
                    ),
                    SizedBox(height: height / 62,),
                    AutoSizeText(
                      (isLanguage != true)?"Telepon Penerima:":"Empty Bank Account",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey)),
                      maxLines: 1,
                    ),
                    AutoSizeText(
                      (isLanguage != true)?"12345678910":"Empty Bank Account",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PopUpSearchAddress extends StatefulWidget {
  @override
  _PopUpSearchAddressState createState() => _PopUpSearchAddressState();
}

class _PopUpSearchAddressState extends State<PopUpSearchAddress> {
  bool isLanguage = false;
  bool isEnableAddress = false;

  TextEditingController _searchAddressText = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Wrap(
      children: <Widget>[
        Container(
          width: width,
          height: height / 1.2,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    icon: Icon(Icons.close, color: Colors.grey,),
                  ),
                  AutoSizeText(
                    (isLanguage != true)?"Cari Lokasi":"Search Location",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
                    maxLines: 1,
                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    icon: Icon(Icons.close, color: Colors.transparent,),
                  ),
                ],
              ),
              SizedBox(height: height / 62,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  children: [
                    Container(
                      height: height / 16,
                      child: TextField(
                        controller: _searchAddressText,
                        onChanged: (v){
                          if(v.length > 0){
                            setState(() {
                              isEnableAddress = true;
                            });
                          }else if(v.length == 0){
                            setState(() {
                              isEnableAddress = false;
                            });
                          }
                        },
                        keyboardType: TextInputType.streetAddress,
                        cursorColor: Colors.black,
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w500,)),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(0),
                          filled: true,
                          fillColor: Color(0xffF0F0F5),
                          focusColor: Color(0xffF0F0F5),
                          hoverColor: Color(0xffF0F0F5),
                          hintText: (isLanguage != true)?"Tulis jalan / perumahan / gedung":"Tulis jalan / perumahan / gedung",
                          hintStyle: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w500,
                                  color: Colors.grey)),
                          prefixIcon: Icon(Icons.search, color: Colors.grey,),
                          helperStyle: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.transparent,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              )
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: height / 86,),
                    Divider(thickness: 2,),
                    SizedBox(height: height / 86,),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, PageTransition(type:
                        PageTransitionType.rightToLeft, child:
                        SearchAddressMaps(isLanguage, Position(latitude:
                        -7.3757489, longitude: 112.6902406)) ));
                      },
                      child: Container(
                        width: width,
                        decoration: BoxDecoration(
                          color: Colors.transparent
                        ),
                        child: Row(
                          children: [
                            Icon(Icons.person_pin_circle, color: Colors.grey),
                            SizedBox(width: width / 48,),
                            AutoSizeText(
                              (isLanguage != true)?"Gunakan Lokasi Sekarang":"Username Now",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey)),
                              maxLines: 1,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: height / 86,),
                    Divider(thickness: 2,),
                    (isEnableAddress != true)?AutoSizeText(
                      (isLanguage != true)?"Silahkan masukkan alamat / lokasi pada kolom diatas agar kamu dapat menemukan lokasimu.":"Username Now",
                      style: GoogleFonts.poppins(textStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.grey)),
                      maxLines: 2,
                    ):Column(
                      children: [1,2,3,4].map((e) {
                        return Column(
                          children: [
                            Row(
                              children: [
                                Icon(Icons.location_on, color: Colors.grey,),
                                SizedBox(width: width / 48,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: height / 62,),
                                    AutoSizeText(
                                      "Jalan Ambengan Batu I",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                                      maxLines: 1,
                                    ),
                                    AutoSizeText(
                                      "Tambaksari, Surabaya City, East Java, Indonesia",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 10, fontWeight: FontWeight.w500, color: Colors.grey)),
                                      maxLines: 2,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: height / 62,),
                            Divider(thickness: 2,),
                          ],
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

