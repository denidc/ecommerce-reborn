import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/auth/forgot_password/forgot_password_activity.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_icons/flutter_icons.dart';

class SettingPasswordPage extends StatefulWidget {
  @override
  _SettingPasswordPageState createState() => _SettingPasswordPageState();
}

class _SettingPasswordPageState extends State<SettingPasswordPage> {
  bool isLanguage = false;

  TextEditingController _oldPasswordText = TextEditingController(text: "");
  TextEditingController _newPasswordText = TextEditingController(text: "");
  TextEditingController _confirmPasswordText = TextEditingController(text: "");
  FocusNode fOldPassword;
  FocusNode fNewPassword;
  FocusNode fConfirmPassword;

  bool _obscureOldPassword = true;
  bool _obscureNewPassword = true;
  bool _obscureConfirmPassword = true;

  void _toggleOldPassword() {
    setState(() {
      _obscureOldPassword = !_obscureOldPassword;
    });
  }
  void _toggleNewPassword() {
    setState(() {
      _obscureNewPassword = !_obscureNewPassword;
    });
  }
  void _toggleConfirmPassword() {
    setState(() {
      _obscureConfirmPassword = !_obscureConfirmPassword;
    });
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(height: height / 62,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                    ),
                    Center(
                      child: AutoSizeText(
                        (isLanguage != true)?"Ubah Kata Sandi":"Change Password",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        maxLines: 1,
                      ),
                    ),
                    IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.add, color: Colors.transparent,),
                    )
                  ],
                ),
                SizedBox(height: height / 62,),
                Divider(thickness: 2,),
                SizedBox(height: height / 48,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        (isLanguage != true)?"Kata Sandi Sekarang":"Password Now",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        maxLines: 1,
                      ),
                      Container(
                        height: height / 24,
                        child: TextField(
                          controller: _oldPasswordText,
                          enableInteractiveSelection: false,
                          autocorrect: false,
                          focusNode: fOldPassword,
                          onSubmitted: (term){
                            fOldPassword.unfocus();
                          },
                          obscureText: _obscureOldPassword,
                          keyboardType: TextInputType.visiblePassword,
                          cursorColor: Colors.black,
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14)),
                          decoration: InputDecoration(
                              hintText: (isLanguage != true)?"Masukkan Kata Sandi Sekarang":"Enter Password Now",
                              hintStyle: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                              helperStyle: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey, width: 2),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 2),
                              ),
                              suffixIcon: IconButton(
                                onPressed: _toggleOldPassword,
                                icon: Icon(_obscureOldPassword ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                                  color: Colors.black,),
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: height / 62,),
                      AutoSizeText(
                        (isLanguage != true)?"Kata Sandi Baru":"New Password",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        maxLines: 1,
                      ),
                      Container(
                        height: height / 24,
                        child: TextField(
                          controller: _newPasswordText,
                          enableInteractiveSelection: false,
                          autocorrect: false,
                          focusNode: fNewPassword,
                          onSubmitted: (term){
                            fNewPassword.unfocus();
                          },
                          obscureText: _obscureNewPassword,
                          keyboardType: TextInputType.visiblePassword,
                          cursorColor: Colors.black,
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14)),
                          decoration: InputDecoration(
                              hintText: (isLanguage != true)?"Masukkan Kata Sandi Baru":"Enter New Password",
                              hintStyle: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                              helperStyle: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey, width: 2),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 2),
                              ),
                              suffixIcon: IconButton(
                                onPressed: _toggleNewPassword,
                                icon: Icon(_obscureNewPassword ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                                  color: Colors.black,),
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: height / 62,),
                      AutoSizeText(
                        (isLanguage != true)?"Ulangi Kata Sandi Anda":"Repeat Your Password",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        maxLines: 1,
                      ),
                      Container(
                        height: height / 24,
                        child: TextField(
                          controller: _confirmPasswordText,
                          enableInteractiveSelection: false,
                          autocorrect: false,
                          focusNode: fConfirmPassword,
                          onSubmitted: (term){
                            fConfirmPassword.unfocus();
                          },
                          obscureText: _obscureConfirmPassword,
                          keyboardType: TextInputType.visiblePassword,
                          cursorColor: Colors.black,
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14)),
                          decoration: InputDecoration(
                              hintText: (isLanguage != true)?"Masukkan Kata Sandi Anda":"Enter Your Password",
                              hintStyle: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                              helperStyle: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey, width: 2),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black, width: 2),
                              ),
                              suffixIcon: IconButton(
                                onPressed: _toggleConfirmPassword,
                                icon: Icon(_obscureConfirmPassword ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                                  color: Colors.black,),
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: height / 48,),
                      AutoSizeText(
                        (isLanguage != true)?"Masukkan password reborn kamu saat mendaftar akun reborn ini.":"Your Password",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Colors.grey)),
                        maxLines: 2,
                      ),
                      SizedBox(height: height / 62,),
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ForgotPasswordActivity()) );
                        },
                        child: Center(
                          child: AutoSizeText(
                            (isLanguage != true)?"Lupa Kata Sandi?":"Forgot Password?",
                            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.w400, color: Color(0xff907135))),
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              child: Container(
                width: width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(6.0),
                  color: Color(0xffc6ab74),
                  child: InkWell(
                    onTap: (){
                      // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: SearchAddressMaps(isLanguage, Position(latitude: -7.3757489, longitude: 112.6902406)) ));
                    },
                    splashColor: Colors.white,
                    borderRadius: BorderRadius.circular(6.0),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        child: Text((isLanguage != true)?"Simpan":"Save", style: GoogleFonts.poppins(
                            textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
