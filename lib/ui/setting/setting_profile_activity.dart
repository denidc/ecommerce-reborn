import 'package:auto_size_text/auto_size_text.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:ecommerce_reborn/ui/setting/balance/setting_balance_page.dart';
import 'package:ecommerce_reborn/ui/setting/bank_account/setting_bank_account_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_account_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_address_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_my_order_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_order_page.dart';
import 'package:ecommerce_reborn/ui/setting/profile/setting_stored_product_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SettingProfileActivity extends StatefulWidget {
  @override
  _SettingProfileActivityState createState() => _SettingProfileActivityState();
}

class _SettingProfileActivityState extends State<SettingProfileActivity> {
  bool isLanguage = false;
  bool isShopping = false;
  bool isReview = false;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: height / 46,
            ),
            Center(
              child: AutoSizeText(
                (isLanguage != true) ? "Profil Saya" : "My Profile",
                style: GoogleFonts.poppins(
                    textStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
            ),
            Divider(
              thickness: 1,
            ),
            Expanded(
              child: SmartRefresher(
                enablePullDown: true,
                enablePullUp: false,
                header: WaterDropMaterialHeader(
                  color: Colors.grey,
                  backgroundColor: Colors.white,
                ),
                footer: CustomFooter(
                  builder: (BuildContext context,LoadStatus mode){
                    Widget body ;
                    if(mode==LoadStatus.idle){
                      body =  Text("pull up load");
                    }
                    else if(mode==LoadStatus.loading){
                      body =  CupertinoActivityIndicator();
                    }
                    else if(mode == LoadStatus.failed){
                      body = Text("Load Failed!Click retry!");
                    }
                    else if(mode == LoadStatus.canLoading){
                      body = Text("release to load more");
                    }
                    else{
                      body = Text("No more Data");
                    }
                    return Container(
                      height: 55.0,
                      child: Center(child:body),
                    );
                  },
                ),
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: width,
                              child: SvgPicture.asset("assets/image/border_profile.svg")),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: width / 18,
                              vertical: height / 32,
                            ),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    CircularProfileAvatar(
                                      '',
                                      radius: height * 0.05,
                                      borderColor: Colors.white,
                                      elevation: 5.0,
                                      // borderWidth: height * 0.008,
                                      cacheImage: true,
                                      child: SvgPicture.asset(
                                          "assets/image/image_profile.svg"),
                                      // child: Container(
                                      //   decoration: BoxDecoration(
                                      //     color: Colors.amber
                                      //   ),
                                      // ),
                                    ),
                                    SizedBox(
                                      width: width / 32,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        AutoSizeText(
                                          "Deni Dwi Candra",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                        AutoSizeText(
                                          "denidc",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                        AutoSizeText(
                                          "LIHAT PROFILE",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 11,
                                                  fontWeight: FontWeight.w600, color: Color(0xffBE9639))),
                                          maxLines: 1,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: height / 48,
                                ),
                                Container(
                                  width: width / 1.18,
                                  height: height / 12,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Color(0xffF5E2C2)),
                                    borderRadius: BorderRadius.circular(8),
                                    color: Color(0xffF5E2C2).withOpacity(.5),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          AutoSizeText(
                                            "Profile Anda 67% Lengkap!",
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w400)),
                                            maxLines: 1,
                                          ),
                                          SizedBox(
                                            width: width / 62,
                                          ),
                                          AutoSizeText(
                                            "Lengkapi profil Anda",
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    decoration:
                                                    TextDecoration.underline,
                                                    fontSize: 10,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xffc6ab74))),
                                            maxLines: 1,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: height * 0.01,
                                      ),
                                      Container(
                                        width: width / 1.8,
                                        child: FAProgressBar(
                                          currentValue: 67,
                                          displayText: '%',
                                          size: 14,
                                          progressColor: Color(0xffc6ab74),
                                          backgroundColor: Color(0xffE0E0E0),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                ),
                                SizedBox(
                                  height: height * 0.01,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: width / 42),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      AutoSizeText(
                                        "Saldo Reborn",
                                        style: GoogleFonts.poppins(
                                            textStyle: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600)),
                                        maxLines: 1,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.rightToLeft,
                                                  child: SettingBalancePage(0)));
                                        },
                                        child: AutoSizeText(
                                          "Lihat Detail",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: height / 62,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: width / 28),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      GestureDetector(
                                        onTap: (){
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.rightToLeft,
                                                  child: SettingBalancePage(0)));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.transparent
                                          ),
                                          child: Column(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/image/icon_referral.svg"),
                                              SizedBox(height: height / 48,),
                                              AutoSizeText(
                                                "Referral",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 10,
                                                        fontWeight: FontWeight.w500)),
                                                maxLines: 1,
                                              ),
                                              AutoSizeText(
                                                "30 Member",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w600)),
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: (){
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.rightToLeft,
                                                  child: SettingBalancePage(1)));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.transparent
                                          ),
                                          child: Column(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/image/icon_balance.svg"),
                                              SizedBox(height: height / 48,),
                                              AutoSizeText(
                                                "Saldo",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 10,
                                                        fontWeight: FontWeight.w500)),
                                                maxLines: 1,
                                              ),
                                              AutoSizeText(
                                                "Rp 500rb",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w600)),
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: (){
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.rightToLeft,
                                                  child: SettingBalancePage(2)));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.transparent
                                          ),
                                          child: Column(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/image/icon_withdrawal.svg"),
                                              SizedBox(height: height / 48,),
                                              AutoSizeText(
                                                "Withdrawal",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 10,
                                                        fontWeight: FontWeight.w500)),
                                                maxLines: 1,
                                              ),
                                              AutoSizeText(
                                                "500k",
                                                style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w600)),
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: height.round() / 48, right: width / 48),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: IconButton(
                                onPressed: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.rightToLeft,
                                          child: SettingAccountPage()));
                                },
                                icon: Icon(Icons.settings, color: Color(0xffc6ab74),),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: height / 48,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: width / 28),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isShopping = !isShopping;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(color: Colors.transparent),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    AutoSizeText(
                                      "Shopping",
                                      style: GoogleFonts.poppins(
                                          textStyle: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700)),
                                      maxLines: 1,
                                    ),
                                    (isShopping)
                                        ? Icon(Icons.keyboard_arrow_up)
                                        : Icon(Icons.keyboard_arrow_down)
                                  ],
                                ),
                              ),
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            AnimatedContainer(
                              duration: Duration(milliseconds: 300),
                              width: width,
                              height: (isShopping) ? height / 7 : 0,
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: (){
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType.rightToLeft,
                                                child: SettingOrderPage()));
                                      },
                                      child: Container(
                                        width: width,
                                        height: height / 36,
                                        decoration: BoxDecoration(
                                            color: Colors.transparent
                                        ),
                                        child: AutoSizeText(
                                          "Order",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height * 0.01,
                                    ),
                                    GestureDetector(
                                      onTap: (){
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType.rightToLeft,
                                                child: SettingStoredProductPage()));
                                      },
                                      child: Container(
                                        width: width,
                                        height: height / 36,
                                        decoration: BoxDecoration(
                                            color: Colors.transparent
                                        ),
                                        child: AutoSizeText(
                                          "Produk Tersimpan",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height * 0.01,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType.rightToLeft,
                                                child: SettingAddressPage()));
                                      },
                                      child: Container(
                                        width: width,
                                        height: height / 36,
                                        decoration:
                                        BoxDecoration(color: Colors.transparent),
                                        child: AutoSizeText(
                                          "Alamat Pengiriman",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height * 0.01,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType.rightToLeft,
                                                child: SettingMyOrderPage()));
                                      },
                                      child: Container(
                                        width: width,
                                        height: height / 36,
                                        decoration:
                                        BoxDecoration(color: Colors.transparent),
                                        child: AutoSizeText(
                                          "Pesanan Saya",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.01,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isReview = !isReview;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(color: Colors.transparent),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    AutoSizeText(
                                      "Ulasan Saya",
                                      style: GoogleFonts.poppins(
                                          textStyle: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700)),
                                      maxLines: 1,
                                    ),
                                    (isReview)
                                        ? Icon(Icons.keyboard_arrow_up)
                                        : Icon(Icons.keyboard_arrow_down)
                                  ],
                                ),
                              ),
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            AnimatedContainer(
                              duration: Duration(milliseconds: 300),
                              width: width,
                              height: (isReview) ? height / 15 : 0,
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: (){

                                      },
                                      child: Container(
                                        width: width,
                                        height: height / 36,
                                        decoration: BoxDecoration(
                                            color: Colors.transparent
                                        ),
                                        child: AutoSizeText(
                                          "Topik Favorit",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height * 0.01,
                                    ),
                                    GestureDetector(
                                      onTap: (){

                                      },
                                      child: Container(
                                        width: width,
                                        height: height / 36,
                                        decoration: BoxDecoration(
                                            color: Colors.transparent
                                        ),
                                        child: AutoSizeText(
                                          "Review",
                                          style: GoogleFonts.poppins(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.01,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.rightToLeft,
                                        child: SettingBankAccountPage()));
                              },
                              child: Container(
                                width: width,
                                decoration: BoxDecoration(color: Colors.transparent),
                                child: AutoSizeText(
                                  "Bank Akun",
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 16, fontWeight: FontWeight.w700)),
                                  maxLines: 1,
                                ),
                              ),
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            SizedBox(
                              height: height * 0.01,
                            ),
                            AutoSizeText(
                              "Pusat Bantuan",
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      fontSize: 16, fontWeight: FontWeight.w700)),
                              maxLines: 1,
                            ),
                            Divider(
                              thickness: 1,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
