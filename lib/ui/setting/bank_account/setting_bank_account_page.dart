import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/setting/bank_account/add_bank_account_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class SettingBankAccountPage extends StatefulWidget {
  @override
  _SettingBankAccountPageState createState() => _SettingBankAccountPageState();
}

class _SettingBankAccountPageState extends State<SettingBankAccountPage> {
  bool isLanguage = false;
  bool isEmpty = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: (isEmpty)?Column(
          children: [
            SizedBox(height: height / 62,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                ),
                Center(
                  child: AutoSizeText(
                    (isLanguage != true)?"Rekening Bank":"Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                ),
                IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.add, color: Colors.transparent,),
                ),
              ],
            ),
            SizedBox(height: height / 62,),
            Divider(thickness: 2,),
            SizedBox(height: height / 12,),
            SvgPicture.asset("assets/image/image_bank_account.svg"),
            SizedBox(height: height / 32,),
            Center(
              child: AutoSizeText(
                (isLanguage != true)?"Rekening Bank Masih Kosong":"Empty Bank Account",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
            ),
            Center(
              child: AutoSizeText(
                (isLanguage != true)?"Tambah Rekening Bank-mu untuk mempermudah":"Add your Bank Account to make it easier",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.grey)),
                maxLines: 1,
              ),
            ),
            Center(
              child: AutoSizeText(
                (isLanguage != true)?"proses tarik saldo di reborn":"withdrawal process in Reborn",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.grey)),
                maxLines: 1,
              ),
            ),
          ],
        ) :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: height / 62,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                ),
                Center(
                  child: AutoSizeText(
                    (isLanguage != true)?"Rekening Bank":"Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                ),
                IconButton(
                  onPressed: (){
                    showModalBottomSheet<dynamic>(
                        isScrollControlled: true,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16),
                                topRight: Radius.circular(16))
                        ),
                        context: this.context,
                        builder: (ctx){
                          return Wrap(
                            children: <Widget>[
                              Container(
                                width: width,
                                height: height / 1.2,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        IconButton(
                                          onPressed: (){
                                            Navigator.of(context, rootNavigator: true).pop();
                                          },
                                          icon: Icon(Icons.close, color: Colors.grey,),
                                        ),
                                        AutoSizeText(
                                          (isLanguage != true)?"Pilih Bank":"Choose Bank",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                        IconButton(
                                          onPressed: (){
                                            Navigator.of(context, rootNavigator: true).pop();
                                          },
                                          icon: Icon(Icons.close, color: Colors.transparent,),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: height / 48,),
                                    Column(
                                      children: [
                                        "PT. BCA (BANK CENTRAL ASIA) TBK/BCA",
                                        "PT. BANK NEGARA INDONESIA (BNI) PERSERO",
                                        "PT. BANK RAKYAT INDONESIA (BRI) PERSERO",
                                        "PT. BANK CIMB NIAGA TBK"
                                      ].map((e) {
                                        return Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 8),
                                          child: Column(
                                            children: [
                                              SizedBox(height: height / 62,),
                                              GestureDetector(
                                                onTap: (){
                                                  Navigator.push(context, PageTransition(type:
                                                  PageTransitionType.rightToLeft, child:
                                                  AddBankAccountPage(e)));
                                                },
                                                child: Container(
                                                  width: width,
                                                  decoration: BoxDecoration(
                                                      color: Colors.transparent
                                                  ),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      AutoSizeText(
                                                        e,
                                                        style: GoogleFonts.poppins(textStyle:
                                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                                                        maxLines: 1,
                                                      ),
                                                      Icon(Icons.panorama_fish_eye)
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: height / 86,),
                                              Divider(thickness: 2,)
                                            ],
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          );
                        }
                    );
                  },
                  icon: Icon(Icons.add, color: Color(0xffDDC58E), size: 32,),
                )
              ],
            ),
            Divider(thickness: 2,),
            SizedBox(height: height / 62,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    (isLanguage != true)?"REKENING UTAMA":"MAIN ACCOUNT",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 11, fontWeight: FontWeight.w400, color: Colors.grey)),
                    maxLines: 1,
                  ),
                  SizedBox(height: height / 62,),
                  AutoSizeText(
                    "Bank Central Asia (BCA)",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    "a/n Deni Dwi Candra",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    "12345678910",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                    maxLines: 1,
                  ),
                  SizedBox(height: height / 62,),
                  Container(
                    width: width / 8,
                    height: height * 0.035,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(MaterialCommunityIcons.pencil_outline, size: 18,),
                        AutoSizeText(
                          "Edit",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                          maxLines: 1,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: height / 62,),
                  Divider(thickness: 2,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      "Bank Central Asia (BCA)",
                      "BANK NEGARA INDONESIA (BNI)",
                      "BANK RAKYAT INDONESIA (BRI)",
                    ].map((e) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: height / 62,),
                          AutoSizeText(
                            e,
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "a/n Deni Dwi Candra",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                            maxLines: 1,
                          ),
                          AutoSizeText(
                            "12345678910",
                            style: GoogleFonts.poppins(textStyle:
                            TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                            maxLines: 1,
                          ),
                          SizedBox(height: height / 62,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: width / 8,
                                    height: height * 0.035,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black)
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(MaterialCommunityIcons.pencil_outline, size: 18,),
                                        AutoSizeText(
                                          "Edit",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: width / 32,),
                                  Container(
                                    width: width / 5,
                                    height: height * 0.035,
                                    decoration: BoxDecoration(
                                        color: Color(0xffC6AB74)
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        AutoSizeText(
                                          "Set Utama",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 11, fontWeight: FontWeight.w500, color: Colors.white)),
                                          maxLines: 1,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                width: width / 8,
                                height: height * 0.035,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    AutoSizeText(
                                      "Hapus",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w500)),
                                      maxLines: 1,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: (isEmpty)?Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: SearchAddressMaps(isLanguage, Position(latitude: -7.3757489, longitude: 112.6902406)) ));
                    showModalBottomSheet<dynamic>(
                      isScrollControlled: true,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16),
                              topRight: Radius.circular(16))
                      ),
                        context: this.context,
                        builder: (ctx){
                          return Wrap(
                            children: <Widget>[
                              Container(
                                width: width,
                                height: height / 1.2,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        IconButton(
                                          onPressed: (){
                                            Navigator.of(context, rootNavigator: true).pop();
                                          },
                                          icon: Icon(Icons.close, color: Colors.grey,),
                                        ),
                                        AutoSizeText(
                                          (isLanguage != true)?"Pilih Bank":"Choose Bank",
                                          style: GoogleFonts.poppins(textStyle:
                                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                                          maxLines: 1,
                                        ),
                                        IconButton(
                                          onPressed: (){
                                            Navigator.of(context, rootNavigator: true).pop();
                                          },
                                          icon: Icon(Icons.close, color: Colors.transparent,),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: height / 48,),
                                    Column(
                                      children: [
                                        "PT. BCA (BANK CENTRAL ASIA) TBK/BCA",
                                        "PT. BANK NEGARA INDONESIA (BNI) PERSERO",
                                        "PT. BANK RAKYAT INDONESIA (BRI) PERSERO",
                                        "PT. BANK CIMB NIAGA TBK"
                                      ].map((e) {
                                        return Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 8),
                                          child: Column(
                                            children: [
                                              SizedBox(height: height / 62,),
                                              GestureDetector(
                                                onTap: (){
                                                  Navigator.push(context, PageTransition(type:
                                                  PageTransitionType.rightToLeft, child:
                                                  AddBankAccountPage(e)));
                                                },
                                                child: Container(
                                                  width: width,
                                                  decoration: BoxDecoration(
                                                    color: Colors.transparent
                                                  ),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      AutoSizeText(
                                                        e,
                                                        style: GoogleFonts.poppins(textStyle:
                                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                                                        maxLines: 1,
                                                      ),
                                                      Icon(Icons.panorama_fish_eye)
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: height / 86,),
                                              Divider(thickness: 2,)
                                            ],
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          );
                        }
                    );
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"Tambah Rekening":"Tambah Bank Account", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ):SizedBox(),
    );
  }
}
