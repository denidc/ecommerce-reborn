import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AddBankAccountPage extends StatefulWidget {
  String bank;

  AddBankAccountPage(this.bank);

  @override
  _AddBankAccountPageState createState() => _AddBankAccountPageState(bank);
}

class _AddBankAccountPageState extends State<AddBankAccountPage> {
  String bank;

  _AddBankAccountPageState(this.bank);

  TextEditingController _nameBankText;
  TextEditingController _nameBankOwnerText = TextEditingController(text: "");
  TextEditingController _accountNumberText = TextEditingController(text: "");

  bool isLanguage = false;

  @override
  void initState() {
    _nameBankText = TextEditingController(text: bank);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(height: height / 62,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                ),
                Center(
                  child: AutoSizeText(
                    (isLanguage != true)?"Rekening Bank":"Bank Account",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                ),
                IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.add, color: Colors.transparent,),
                ),
              ],
            ),
            Divider(thickness: 2,),
            SizedBox(height: height / 62,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    (isLanguage != true)?"Nama Bank":"Bank Name",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                  Container(
                    height: height / 24,
                    child: TextField(
                      enabled: false,
                      controller: _nameBankText,
                      keyboardType: TextInputType.name,
                      cursorColor: Colors.black,
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Nama Bank":"Enter Bank Name",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 2),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: height / 62,),
                  AutoSizeText(
                    (isLanguage != true)?"Nama Pemilik Bank":"Name Bank Owner",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                  Container(
                    height: height / 24,
                    child: TextField(
                      controller: _nameBankOwnerText,
                      keyboardType: TextInputType.name,
                      cursorColor: Colors.black,
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Nama":"Enter Name",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 2),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: height / 62,),
                  AutoSizeText(
                    (isLanguage != true)?"Nomor Rekening":"Account Number",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                  Container(
                    height: height / 24,
                    child: TextField(
                      controller: _accountNumberText,
                      keyboardType: TextInputType.numberWithOptions(decimal: false),
                      cursorColor: Colors.black,
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Nomor":"Enter Account Number",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 12,
              blurRadius: 12,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width / 1.2,
              height: height / 16,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(6.0),
                color: Color(0xffc6ab74),
                child: InkWell(
                  onTap: (){
                    // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: SearchAddressMaps(isLanguage, Position(latitude: -7.3757489, longitude: 112.6902406)) ));
                  },
                  splashColor: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  child: Center(
                    child: Text((isLanguage != true)?"Tambah Rekening":"Tambah Bank Account", style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
