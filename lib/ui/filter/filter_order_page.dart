import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';

class FilterOrderPage extends StatefulWidget {
  @override
  _FilterOrderPageState createState() => _FilterOrderPageState();
}

class _FilterOrderPageState extends State<FilterOrderPage> {
  ScrollController _scrollController = ScrollController();
  int orderStatusOne;
  int orderStatusTwo;
  int productType = 0;

  String fromDate = "";
  String endDate = "";

  List<Status> _orderStatusOne = [
    Status(0, "Menunggu Pembayaran", false),
    Status(1, "Pembayaran Diterima", false),
    Status(2, "Barang Diterima", false)
  ];
  List<Status> _orderStatusTwo = [
    Status(0, "Verifikasi Pembayaran", false),
    Status(1, "Pembatalan", false),
    Status(2, "Pengiriman", false),
    Status(3, "Proses Order", false)
  ];
  List<Status> _productType = [
    Status(0, "Semua Produk", true),
    Status(1, "Produk Kesehatan", false),
    Status(2, "Produk Kecantikan", false),
  ];

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.add,
              color: Colors.transparent,
            ),
          )
        ],
        title: Center(
          child: AutoSizeText(
            "Filter",
            style: GoogleFonts.poppins(
                textStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black)),
            maxLines: 1,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: Color(0xffDDC58E),
            size: 32,
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: height / 86,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width / 48),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  "Order Status",
                  style: GoogleFonts.poppins(
                      textStyle:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                  maxLines: 1,
                ),
                SizedBox(
                  height: height / 48,
                ),
                Scrollbar(
                  controller: _scrollController,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: width / 86),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      controller: _scrollController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            controller: _scrollController,
                            physics: NeverScrollableScrollPhysics(),
                            child: Row(
                              children: _orderStatusOne.map((e) {
                                return Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: width / 86),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _orderStatusTwo = [
                                          Status(
                                              0, "Verification Pembayaran", false),
                                          Status(1, "Pembatalan", false),
                                          Status(2, "Pengiriman", false),
                                          Status(3, "Proses Order", false)
                                        ];
                                      });
                                      if (orderStatusOne != null) {
                                        setState(() {
                                          if (orderStatusOne !=
                                                  _orderStatusOne[e.index].index ||
                                              _orderStatusOne[e.index].selected !=
                                                  true) {
                                            _orderStatusOne[orderStatusOne]
                                                .selected = false;
                                            _orderStatusOne[e.index].selected =
                                                true;
                                            orderStatusOne = e.index;
                                          } else {
                                            _orderStatusOne[orderStatusOne]
                                                .selected = false;
                                          }
                                        });
                                      } else {
                                        setState(() {
                                          _orderStatusOne[e.index].selected = true;
                                          orderStatusOne = e.index;
                                        });
                                      }
                                    },
                                    child: Container(
                                      height: height / 18,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.grey,
                                              width: (e.selected) ? 0 : 0.5),
                                          borderRadius: BorderRadius.circular(24),
                                          color: (e.selected)
                                              ? Color(0xffBE9639)
                                              : Colors.white),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: width / 30),
                                        child: Center(
                                          child: AutoSizeText(
                                            e.name,
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    color: (e.selected)
                                                        ? Colors.white
                                                        : Colors.black)),
                                            maxLines: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                          SizedBox(
                            height: height / 86,
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            controller: _scrollController,
                            physics: NeverScrollableScrollPhysics(),
                            child: Row(
                              children: _orderStatusTwo.map((e) {
                                return Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: width / 86),
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _orderStatusOne = [
                                          Status(0, "Menunggu Pembayaran", false),
                                          Status(1, "Pembayaran Diterima", false),
                                          Status(2, "Barang Diterima", false)
                                        ];
                                      });
                                      if (orderStatusTwo != null) {
                                        setState(() {
                                          if (orderStatusTwo !=
                                                  _orderStatusTwo[e.index].index ||
                                              _orderStatusTwo[e.index].selected !=
                                                  true) {
                                            _orderStatusTwo[orderStatusTwo]
                                                .selected = false;
                                            _orderStatusTwo[e.index].selected =
                                                true;
                                            orderStatusTwo = e.index;
                                          } else {
                                            _orderStatusTwo[orderStatusTwo]
                                                .selected = false;
                                          }
                                        });
                                      } else {
                                        setState(() {
                                          _orderStatusTwo[e.index].selected = true;
                                          orderStatusTwo = e.index;
                                        });
                                      }
                                    },
                                    child: Container(
                                      height: height / 18,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.grey,
                                              width: (e.selected) ? 0 : 0.5),
                                          borderRadius: BorderRadius.circular(24),
                                          color: (e.selected)
                                              ? Color(0xffBE9639)
                                              : Colors.white),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: width / 30),
                                        child: Center(
                                          child: AutoSizeText(
                                            e.name,
                                            style: GoogleFonts.poppins(
                                                textStyle: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    color: (e.selected)
                                                        ? Colors.white
                                                        : Colors.black)),
                                            maxLines: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: height / 86,),
          Divider(thickness: 4,),
          SizedBox(height: height / 86,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width / 48),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  "Tanggal",
                  style: GoogleFonts.poppins(
                      textStyle:
                      TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                  maxLines: 1,
                ),
                SizedBox(
                  height: height / 86,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: (){
                        DatePicker.showDatePicker(context, showTitleActions: true,
                            onConfirm: (date) {
                              setState(() {
                                fromDate = date.toString().split(' ')[0];
                              });
                              print(date.toString().split(' ')[0]);
                            },
                            currentTime: DateTime(DateTime.now().year, DateTime.now().month,
                                DateTime.now().day),
                            locale: LocaleType.id,
                            maxTime: DateTime(DateTime.now().year, 12, 31)
                        );
                      },
                      child: Container(
                        width: width / 2.2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            (fromDate != "")?AutoSizeText(
                              "Dari Tanggal",
                              style: GoogleFonts.poppins(
                                  textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ):Container(),
                            AutoSizeText(
                              (fromDate != "")?fromDate:"Dari Tanggal",
                              style: GoogleFonts.poppins(
                                  textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                            Divider(thickness: 2,)
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        DatePicker.showDatePicker(context, showTitleActions: true,
                            onConfirm: (date) {
                              setState(() {
                                endDate = date.toString().split(' ')[0];
                              });
                              print(date.toString().split(' ')[0]);
                            },
                            currentTime: DateTime(DateTime.now().year, DateTime.now().month,
                                DateTime.now().day),
                            locale: LocaleType.id,
                            maxTime: DateTime(DateTime.now().year, 12, 31)
                        );
                      },
                      child: Container(
                        width: width / 2.2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            (endDate != "")?AutoSizeText(
                              "Sampai Tanggal",
                              style: GoogleFonts.poppins(
                                  textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ):Container(),
                            AutoSizeText(
                              (endDate != "")?endDate:"Sampai Tanggal",
                              style: GoogleFonts.poppins(
                                  textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                            Divider(thickness: 2,)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: height / 86,),
          Divider(thickness: 4,),
          SizedBox(height: height / 86,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width / 48),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  "Tipe Produk",
                  style: GoogleFonts.poppins(
                      textStyle:
                      TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                  maxLines: 1,
                ),
                SizedBox(
                  height: height / 86,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: _productType.map((e) {
                      return Padding(
                        padding:
                        EdgeInsets.symmetric(horizontal: width / 86),
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              _productType[productType]
                                  .selected = false;
                              _productType[e.index].selected =
                              true;
                              productType = e.index;
                            });
                          },
                          child: Container(
                            height: height / 18,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.grey,
                                    width: (e.selected) ? 0 : 0.5),
                                borderRadius: BorderRadius.circular(24),
                                color: (e.selected)
                                    ? Color(0xffBE9639)
                                    : Colors.white),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width / 30),
                              child: Center(
                                child: AutoSizeText(
                                  e.name,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color: (e.selected)
                                              ? Colors.white
                                              : Colors.black)),
                                  maxLines: 1,
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: width,
        height: height / 12,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: width / 2.4,
              height: height / 18,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
                borderRadius: BorderRadius.circular(8)
              ),
              child: Center(
                child: AutoSizeText(
                  "Reset",
                  style: GoogleFonts.poppins(
                      textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
            ),
            Container(
              width: width / 2.4,
              height: height / 18,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                color: Color(0xffBE9639)
              ),
              child: Center(
                child: AutoSizeText(
                  "Filter",
                  style: GoogleFonts.poppins(
                      textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                        color: Colors.white)),
                  maxLines: 1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Status {
  int index;
  String name;
  bool selected;

  Status(this.index, this.name, this.selected);
}
