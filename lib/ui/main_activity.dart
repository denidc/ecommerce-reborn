import 'dart:io';

import 'package:ecommerce_reborn/ui/colcal/colcal_activity.dart';
import 'package:ecommerce_reborn/ui/home/home_activity.dart';
import 'package:ecommerce_reborn/ui/secret/secret_activity.dart';
import 'package:ecommerce_reborn/ui/setting/setting_profile_activity.dart';
import 'package:ecommerce_reborn/ui/shop/shop_activity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class MainActivity extends StatefulWidget {
  int bottomNavIndex;

  MainActivity(this.bottomNavIndex);

  @override
  _MainActivityState createState() => _MainActivityState(bottomNavIndex);
}

class _MainActivityState extends State<MainActivity> {
  int bottomNavIndex;

  _MainActivityState(this.bottomNavIndex);

  final homeRefresh = ChangeNotifier();

  DateTime currentBackPressTime;

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(
          msg: "Tekan kembali lagi untuk keluar dari aplikasi",
          backgroundColor: Colors.grey[300],
          textColor: Colors.black,
          fontSize: 14.0);
      return Future.value(false);
    }
    if (Platform.isAndroid) {
      SystemNavigator.pop();
    } else if (Platform.isIOS) {
      exit(0);
    }
    return Future.value(true);
  }

  // final _listPage = <Widget>[
  //   new HomeActivity(
  //     key: PageStorageKey('page1'),
  //   ),
  //   new ShopActivity(),
  //   new SecretActivity(),
  //   new ColcalActivity(),
  //   new SettingProfileActivity(),
  // ];
  //
  // final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery
        .of(context)
        .size
        .width;
    var height = MediaQuery
        .of(context)
        .size
        .height;
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          body: IndexedStack(index: bottomNavIndex, children: [
            HomeActivity(),
            ShopActivity(),
            SecretActivity(),
            ColcalActivity(),
            SettingProfileActivity(),
          ]),
          // body: PageStorage(
          //   child: _listPage[bottomNavIndex],
          //   bucket: bucket,
          // ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: bottomNavIndex,
            onTap: (i) {
              setState(() {
                bottomNavIndex = i;
              });
            },
            type: BottomNavigationBarType.fixed,
            unselectedItemColor: Colors.grey,
            selectedItemColor: Color(0xff664911),
            items: [
              BottomNavigationBarItem(
                  icon: SvgPicture.asset((bottomNavIndex == 0)
                      ? "assets/image/icon_home.svg"
                      : "assets/image/icon_home_unselected.svg",
                    width: width / 14,
                  ),
                  title: Text("Home",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)))),
              BottomNavigationBarItem(
                  icon: SvgPicture.asset((bottomNavIndex == 1)
                      ? "assets/image/icon_shop.svg"
                      : "assets/image/icon_shop_unselected.svg",
                    width: width / 14,
                  ),
                  title: Text("Shop",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)))),
              BottomNavigationBarItem(
                  icon: SvgPicture.asset((bottomNavIndex == 2)
                      ? "assets/image/icon_secret.svg"
                      : "assets/image/icon_secret_unselected.svg",
                    width: width / 12,
                  ),
                  title: Text("Secret",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)))),
              BottomNavigationBarItem(
                  icon: SvgPicture.asset((bottomNavIndex == 3)
                      ? "assets/image/icon_calculator_health.svg"
                      : "assets/image/icon_calculator_health_unselected.svg",
                    width: width / 18,
                  ),
                  title: Text("Colcal",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)))),
              BottomNavigationBarItem(
                  icon: SvgPicture.asset((bottomNavIndex == 4)
                      ? "assets/image/icon_account.svg"
                      : "assets/image/icon_account_unselected.svg",
                    width: width / 18,
                  ),
                  title: Text("Akun",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)))),
            ],
          )),
    );
  }
}
