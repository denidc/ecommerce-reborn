import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_reborn/bloc/product_beauty_bloc.dart';
import 'package:ecommerce_reborn/ui/detail/product_detail_activity.dart';
import 'package:ecommerce_reborn/utils/links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';

class ProductBeautyPage extends StatefulWidget {
  @override
  _ProductBeautyPageState createState() => _ProductBeautyPageState();
}

class _ProductBeautyPageState extends State<ProductBeautyPage> {
  bool isLanguage = false;
  ScrollController _scrollViewController = ScrollController();
  ScrollController _controller = ScrollController();
  ProductBeautyBloc productBeautyBloc;

  final key = GlobalKey<_ProductBeautyPageState>();

  int ratingCount = 5;

  int tabPage = 0;

  bool isPopupCarousel = true;
  int _currentHeader = 0;
  List carouselHeader = [
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
  ];

  List<T> mapHeader<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  void onScroll(){
    double maxScroll = _scrollViewController.position.maxScrollExtent;
    double currentScroll = _scrollViewController.position.pixels;

    if(currentScroll == maxScroll) productBeautyBloc.add(ProductBeautyEvent());
    // if(currentScroll == maxScroll) print(currentScroll);
  }

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    _controller.dispose();
    productBeautyBloc.close();
    super.dispose();
  }

  int index = 0;
  int currentIndex;

  @override
  Widget build(BuildContext context) {
      productBeautyBloc = BlocProvider.of<ProductBeautyBloc>(context);
    _scrollViewController.addListener(onScroll);
    var width = MediaQuery
        .of(context)
        .size
        .width;
    var height = MediaQuery
        .of(context)
        .size
        .height;
    setState(() {

    });
    return Scaffold(
      key: key,
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: SingleChildScrollView(
          controller: _controller,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  CarouselSlider(
                    options: CarouselOptions(
                        autoPlay: false,
                        height: height / 3.5,
                        scrollDirection: Axis.horizontal,
                        enableInfiniteScroll: true,
                        onScrolled: (v) {
                          if (v.toString().split('.')[1] == "0") {
                            setState(() {
                              isPopupCarousel = true;
                            });
                          } else {
                            setState(() {
                              isPopupCarousel = false;
                            });
                          }
                        },
                        onPageChanged: (index, reason) {
                          setState(() {
                            _currentHeader = index;
                          });
                        }
                    ),
                    items: carouselHeader.map((e) {
                      return Container(
                          width: width,
                          decoration: BoxDecoration(
                              color: Colors.pinkAccent
                          ),
                          child: Center(
                              child: SvgPicture.asset(e)
                          )
                      );
                    }).toList(),
                  ),
                  Positioned(
                    left: 0,
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 200),
                      width: (isPopupCarousel) ? width / 8 : 0,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Colors.greenAccent,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(8),
                            bottomRight: Radius.circular(8)),
                      ),
                      child: Center(child: Text(
                        "Text", style: TextStyle(fontWeight: FontWeight.bold),)),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: mapHeader<Widget>(
                                carouselHeader, (index, url) {
                              return AnimatedContainer(
                                duration: Duration(milliseconds: 200),
                                width: _currentHeader == index ? 12 : 6.0,
                                height: _currentHeader == index ? 12 : 6.0,
                                margin: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _currentHeader == index
                                      ? Color(0xffC6AB74)
                                      : Colors.grey,
                                ),
                              );
                            }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true) ? "Produk Baru" : "New Product",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                child: Row(
                  children: [
                    1, 2, 3, 4
                  ].map((e) {
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: ProductDetailActivity()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Container(
                          height: height / 2.4,
                          width: width / 2.6,
                          child: Column(
                            children: [
                              Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 12),
                                    child: Container(
                                      height: height / 4.6,
                                      width: width / 2.6,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/image/product.png"),
                                              fit: BoxFit.cover
                                          ),
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Padding(
                                        padding: EdgeInsets.all(width.round() / 86),
                                        child: Icon(AntDesign.staro, size: 18,
                                          color: Colors.grey,),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Center(
                                child: Container(
                                  height: height / 32,
                                  child: AutoSizeText(
                                    "Byoote",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 18,
                                            fontWeight:
                                            FontWeight.w500)),
                                    minFontSize: 14,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Center(
                                child: Container(
                                  height: height / 42,
                                  child: AutoSizeText(
                                    "Byoote gluthateon",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight:
                                            FontWeight.w400)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Center(
                                child: Container(
                                  height: height / 32,
                                  child: AutoSizeText(
                                    "Rp 300.000",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 18,
                                            fontWeight:
                                            FontWeight.w500)),
                                    minFontSize: 14,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RatingBar(
                                    itemSize: 18,
                                    initialRating: 5.0,
                                    ignoreGestures: true,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: ratingCount,
                                    itemBuilder: (context, _) =>
                                        Icon(
                                          Icons.star,
                                          color: Color(0xffC6AB74),
                                        ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                  SizedBox(width: 4,),
                                  Text(ratingCount.toString(),
                                      style: GoogleFonts.poppins(
                                          textStyle: TextStyle(
                                              fontSize: 10, color: Colors.grey)))
                                ],
                              ),
                              SizedBox(height: height / 86,),
                              Container(
                                height: height / 24,
                                width: width / 2.6,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xffC6AB74)),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: height * 0.002),
                                  child: Center(
                                    child: AutoSizeText(
                                      "ADD TO BAG",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xffC6AB74))),
                                      maxLines: 1,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ),
              Divider(thickness: 4,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true) ? "Terlaris" : "Best Seller",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                child: Row(
                  children: [
                    1, 2, 3, 4
                  ].map((e) {
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: ProductDetailActivity()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Container(
                          height: height / 2.4,
                          width: width / 2.6,
                          child: Column(
                            children: [
                              Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 12),
                                    child: Container(
                                      height: height / 4.6,
                                      width: width / 2.6,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/image/product.png"),
                                              fit: BoxFit.cover
                                          ),
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    child: Container(
                                      color: Colors.transparent,
                                      child: Padding(
                                        padding: EdgeInsets.all(width.round() / 86),
                                        child: Icon(AntDesign.staro, size: 18,
                                          color: Colors.grey,),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Center(
                                child: Container(
                                  height: height / 32,
                                  child: AutoSizeText(
                                    "Byoote",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 18,
                                            fontWeight:
                                            FontWeight.w500)),
                                    minFontSize: 14,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Center(
                                child: Container(
                                  height: height / 42,
                                  child: AutoSizeText(
                                    "Byoote gluthateon",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight:
                                            FontWeight.w400)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Center(
                                child: Container(
                                  height: height / 32,
                                  child: AutoSizeText(
                                    "Rp 300.000",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 18,
                                            fontWeight:
                                            FontWeight.w500)),
                                    minFontSize: 14,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RatingBar(
                                    itemSize: 18,
                                    initialRating: 5.0,
                                    ignoreGestures: true,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: ratingCount,
                                    itemBuilder: (context, _) =>
                                        Icon(
                                          Icons.star,
                                          color: Color(0xffC6AB74),
                                        ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                  SizedBox(width: 4,),
                                  Text(ratingCount.toString(),
                                      style: GoogleFonts.poppins(
                                          textStyle: TextStyle(
                                              fontSize: 10, color: Colors.grey)))
                                ],
                              ),
                              SizedBox(height: height / 86,),
                              Container(
                                height: height / 24,
                                width: width / 2.6,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xffC6AB74)),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: height * 0.002),
                                  child: Center(
                                    child: AutoSizeText(
                                      "ADD TO BAG",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xffC6AB74))),
                                      maxLines: 1,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ),
              Divider(thickness: 4,),
              SizedBox(height: height / 86,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: AutoSizeText(
                  (isLanguage != true) ? "Produk" : "Product",
                  style: GoogleFonts.poppins(textStyle:
                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  maxLines: 1,
                ),
              ),
              BlocBuilder<ProductBeautyBloc, ProductBeautyState>(
                builder: (_, state){
                  if(state is ProductBeautyUninitialized){
                    return Center(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        physics: BouncingScrollPhysics(),
                        child: Row(
                          children: [1, 2].map((e) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: width / 18),
                              child: Container(
                                height: height / 2.5,
                                width: width / 2.6,
                                child: Column(
                                  children: [
                                    Stack(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(top: 12),
                                          child: Shimmer.fromColors(
                                            highlightColor: Color(0xffF2F2F2),
                                            baseColor: Colors.white,
                                            child: Container(
                                              height: height / 4.2,
                                              width: width / 2.6,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                  BorderRadius.circular(8)),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: height / 86,
                                    ),
                                    Center(
                                      child: Shimmer.fromColors(
                                        highlightColor: Color(0xffF2F2F2),
                                        baseColor: Colors.white,
                                        child: Container(
                                          width: width / 4,
                                          height: height / 32,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              BorderRadius.circular(16)),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 86,
                                    ),
                                    Center(
                                      child: Shimmer.fromColors(
                                        highlightColor: Color(0xffF2F2F2),
                                        baseColor: Colors.white,
                                        child: Container(
                                          width: width / 3,
                                          height: height / 32,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              BorderRadius.circular(16)),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 86,
                                    ),
                                    Center(
                                      child: Shimmer.fromColors(
                                        highlightColor: Color(0xffF2F2F2),
                                        baseColor: Colors.white,
                                        child: Container(
                                          width: width / 4,
                                          height: height / 32,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              BorderRadius.circular(16)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    );
                  }else{
                    ProductBeautyLoaded productBeautyLoaded = state as ProductBeautyLoaded;
                    index = productBeautyLoaded.products.length;
                    return StaggeredGridView.countBuilder(
                      controller: _scrollViewController,
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      staggeredTileBuilder: (index) => StaggeredTile.count(1, 1.9),
                      itemCount: (productBeautyLoaded.hasReachedMax) ? productBeautyLoaded.products.length :
                      productBeautyLoaded.products.length,
                      itemBuilder: (ctx, index){
                        return (productBeautyLoaded.products[index].name != null)?GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    child: ProductDetailActivity()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Column(
                              children: [
                                Stack(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 12),
                                      child: Container(
                                        height: height / 4.8,
                                        width: width / 2.6,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: NetworkImage(Links.mainUrl + productBeautyLoaded.products[index].image),
                                                fit: BoxFit.cover
                                            ),
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      right: 0,
                                      top: 0,
                                      child: Container(
                                        color: Colors.transparent,
                                        child: (productBeautyLoaded.products[index].isBookmark != 1)?
                                        Padding(
                                          padding: EdgeInsets.all(width.round() / 86),
                                          child: Icon(AntDesign.staro, size: 18,
                                            color: Colors.grey,),
                                        ) : Padding(
                                          padding: EdgeInsets.all(width.round() / 86),
                                          child: Icon(AntDesign.star, size: 18,
                                            color: Color(0xffBE9639),),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Center(
                                  child: AutoSizeText(
                                    "Byoote",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                                    maxLines: 1,
                                  ),
                                ),
                                Center(
                                  child: AutoSizeText(
                                    productBeautyLoaded.products[index].name,
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                    maxLines: 1,
                                  ),
                                ),
                                Center(
                                  child: AutoSizeText(
                                    "Rp " + productBeautyLoaded.products[index].price.toString(),
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                                    maxLines: 1,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    RatingBar(
                                      itemSize: 18,
                                      initialRating: double.parse(productBeautyLoaded.products[index].rating.toString()),
                                      ignoreGestures: true,
                                      minRating: 1,
                                      direction: Axis.horizontal,
                                      allowHalfRating: true,
                                      itemCount: ratingCount,
                                      itemBuilder: (context, _) => Icon(
                                        Icons.star,
                                        color: Color(0xffC6AB74),
                                      ),
                                      onRatingUpdate: (rating) {
                                        print(rating);
                                      },
                                    ),
                                    SizedBox(width: 4,),
                                    Text(productBeautyLoaded.products[index].rating.toString(), style: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                                  ],
                                ),
                                SizedBox(height: height / 86,),
                                Container(
                                  height: height / 24,
                                  width: width / 2.6,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Color(0xffC6AB74)),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 4),
                                    child: Center(
                                      child: AutoSizeText(
                                        "ADD TO BAG",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xffC6AB74))),
                                        maxLines: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ):Container();
                      },
                    );
                    // return GridView.count(
                    //   controller: _scrollViewController,
                    //   shrinkWrap: true,
                    //   crossAxisCount: 2,
                    //   childAspectRatio: 3 / 6,
                    //   children: productBeautyLoaded.products.map((value) {
                    //     return Padding(
                    //       padding: const EdgeInsets.symmetric(horizontal: 8),
                    //       child: Container(
                    //         height: height / 2.3,
                    //         width: width / 2.6,
                    //         child: Column(
                    //           children: [
                    //             Stack(
                    //               children: [
                    //                 Padding(
                    //                   padding: const EdgeInsets.only(top: 12),
                    //                   child: Container(
                    //                     height: height / 4.2,
                    //                     width: width / 2.6,
                    //                     decoration: BoxDecoration(
                    //                         image: DecorationImage(
                    //                             image: NetworkImage(Links.mainUrl + value.image),
                    //                             fit: BoxFit.cover
                    //                         ),
                    //                         borderRadius: BorderRadius.circular(8)
                    //                     ),
                    //                   ),
                    //                 ),
                    //                 Positioned(
                    //                   right: 0,
                    //                   top: 0,
                    //                   child: Container(
                    //                     decoration: BoxDecoration(
                    //                       color: Colors.white,
                    //                       shape: BoxShape.circle,
                    //                       boxShadow: [
                    //                         BoxShadow(
                    //                           color: Colors.grey.withOpacity(0.5),
                    //                           spreadRadius: 2,
                    //                           blurRadius: 5,
                    //                           offset: Offset(0, 3), // changes position of shadow
                    //                         ),
                    //                       ],
                    //                     ),
                    //                     child: (value.isBookmark != 1)?Icon(Icons.star_border, size: 32,
                    //                       color: Colors.grey,) : Icon(Icons.star, size: 32,
                    //                       color: Color(0xffBE9639),),
                    //                   ),
                    //                 )
                    //               ],
                    //             ),
                    //             Center(
                    //               child: AutoSizeText(
                    //                 "Byoote",
                    //                 style: GoogleFonts.poppins(textStyle:
                    //                 TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                 maxLines: 1,
                    //               ),
                    //             ),
                    //             Center(
                    //               child: AutoSizeText(
                    //                 value.name,
                    //                 style: GoogleFonts.poppins(textStyle:
                    //                 TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                    //                 maxLines: 1,
                    //               ),
                    //             ),
                    //             Center(
                    //               child: AutoSizeText(
                    //                 "Rp " + value.price.toString(),
                    //                 style: GoogleFonts.poppins(textStyle:
                    //                 TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    //                 maxLines: 1,
                    //               ),
                    //             ),
                    //             Row(
                    //               mainAxisAlignment: MainAxisAlignment.center,
                    //               children: <Widget>[
                    //                 RatingBar(
                    //                   itemSize: 18,
                    //                   initialRating: double.parse(value.rating.toString()),
                    //                   ignoreGestures: true,
                    //                   minRating: 1,
                    //                   direction: Axis.horizontal,
                    //                   allowHalfRating: true,
                    //                   itemCount: ratingCount,
                    //                   itemBuilder: (context, _) => Icon(
                    //                     Icons.star,
                    //                     color: Color(0xffC6AB74),
                    //                   ),
                    //                   onRatingUpdate: (rating) {
                    //                     print(rating);
                    //                   },
                    //                 ),
                    //                 SizedBox(width: 4,),
                    //                 Text(value.rating.toString(), style: GoogleFonts.poppins(
                    //                     textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                    //               ],
                    //             ),
                    //             SizedBox(height: height / 86,),
                    //             Container(
                    //               height: height / 32,
                    //               width: width / 2.6,
                    //               decoration: BoxDecoration(
                    //                 border: Border.all(color: Color(0xffC6AB74)),
                    //                 borderRadius: BorderRadius.circular(4),
                    //               ),
                    //               child: Center(
                    //                 child: AutoSizeText(
                    //                   "Tambah Keranjang",
                    //                   style: GoogleFonts.poppins(textStyle:
                    //                   TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                    //                       color: Color(0xffC6AB74))),
                    //                   maxLines: 1,
                    //                 ),
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     );
                    //   }).toList(),
                    // );
                  }
                },
              ),
              (index != currentIndex)?FlatButton(
                onPressed: (){
                  productBeautyBloc.add(ProductBeautyEvent());
                  setState(() {
                    currentIndex = index;
                  });
                },
                child: Center(
                  child: Container(
                    width: width / 2,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Center(
                        child: Text("Load More"),
                      ),
                    ),
                  ),
                ),
              ):Container(),
            ],
          ),
        ),
      ),
    );
  }
}
