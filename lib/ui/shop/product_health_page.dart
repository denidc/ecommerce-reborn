import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_reborn/bloc/product_health_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProductHealthPage extends StatefulWidget {
  @override
  _ProductHealthPageState createState() => _ProductHealthPageState();
}

class _ProductHealthPageState extends State<ProductHealthPage> {
  bool isLanguage = false;
  ScrollController _scrollViewController = ScrollController();
  ProductHealthBloc bloc;

  double ratingCount = 5;

  int tabPage = 0;

  bool isPopupCarousel = true;
  int _currentHeader = 0;
  List carouselHeader = [
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
    "assets/image/image_open_store.svg",
  ];

  List<T> mapHeader<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  void onScroll(){
    double maxScroll = _scrollViewController.position.maxScrollExtent;
    double currentScroll = _scrollViewController.position.pixels;

    if(currentScroll == maxScroll) bloc.add(ProductHealthEvent());
  }

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<ProductHealthBloc>(context);
    _scrollViewController.addListener(onScroll);
    var width = MediaQuery
        .of(context)
        .size
        .width;
    var height = MediaQuery
        .of(context)
        .size
        .height;
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: false,
      header: WaterDropMaterialHeader(
        color: Colors.grey,
        backgroundColor: Colors.white,
      ),
      footer: CustomFooter(
        builder: (BuildContext context,LoadStatus mode){
          Widget body ;
          if(mode==LoadStatus.idle){
            body =  Text("pull up load");
          }
          else if(mode==LoadStatus.loading){
            body =  CupertinoActivityIndicator();
          }
          else if(mode == LoadStatus.failed){
            body = Text("Load Failed!Click retry!");
          }
          else if(mode == LoadStatus.canLoading){
            body = Text("release to load more");
          }
          else{
            body = Text("No more Data");
          }
          return Container(
            height: 55.0,
            child: Center(child:body),
          );
        },
      ),
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      child: SingleChildScrollView(
        controller: _scrollViewController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                CarouselSlider(
                  options: CarouselOptions(
                      autoPlay: false,
                      height: height / 3.5,
                      scrollDirection: Axis.horizontal,
                      enableInfiniteScroll: true,
                      onScrolled: (v) {
                        if (v.toString().split('.')[1] == "0") {
                          setState(() {
                            isPopupCarousel = true;
                          });
                        } else {
                          setState(() {
                            isPopupCarousel = false;
                          });
                        }
                      },
                      onPageChanged: (index, reason) {
                        setState(() {
                          _currentHeader = index;
                        });
                      }
                  ),
                  items: carouselHeader.map((e) {
                    return Container(
                        width: width,
                        decoration: BoxDecoration(
                            color: Colors.pinkAccent
                        ),
                        child: Center(
                            child: SvgPicture.asset(e)
                        )
                    );
                  }).toList(),
                ),
                Positioned(
                  left: 0,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    width: (isPopupCarousel) ? width / 8 : 0,
                    height: 20,
                    decoration: BoxDecoration(
                      color: Colors.greenAccent,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          bottomRight: Radius.circular(8)),
                    ),
                    child: Center(child: Text(
                      "Text", style: TextStyle(fontWeight: FontWeight.bold),)),
                  ),
                ),
                Positioned(
                  right: 0,
                  bottom: 0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: mapHeader<Widget>(
                              carouselHeader, (index, url) {
                            return AnimatedContainer(
                              duration: Duration(milliseconds: 200),
                              width: _currentHeader == index ? 12 : 6.0,
                              height: _currentHeader == index ? 12 : 6.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _currentHeader == index
                                    ? Color(0xffC6AB74)
                                    : Colors.grey,
                              ),
                            );
                          }),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: height / 86,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AutoSizeText(
                (isLanguage != true) ? "Produk Baru" : "New Product",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              child: Row(
                children: [
                  1, 2, 3, 4
                ].map((e) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Container(
                      height: height / 2.3,
                      width: width / 2.6,
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 12),
                                child: Container(
                                  height: height / 4.2,
                                  width: width / 2.6,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/image/image_product_beauty.png"),
                                          fit: BoxFit.cover
                                      ),
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 2,
                                        blurRadius: 5,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Icon(Icons.star_border, size: 32,
                                    color: Colors.grey,),
                                ),
                              )
                            ],
                          ),
                          Center(
                            child: AutoSizeText(
                              "Byoote",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          Center(
                            child: AutoSizeText(
                              "Byoote gluthateon",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w400)),
                              maxLines: 1,
                            ),
                          ),
                          Center(
                            child: AutoSizeText(
                              "Rp 300.000",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              RatingBar(
                                itemSize: 18,
                                initialRating: ratingCount,
                                ignoreGestures: true,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemBuilder: (context, _) =>
                                    Icon(
                                      Icons.star,
                                      color: Color(0xffC6AB74),
                                    ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              SizedBox(width: 4,),
                              Text(ratingCount.toString(),
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 10, color: Colors.grey)))
                            ],
                          ),
                          SizedBox(height: height / 86,),
                          Container(
                            height: height / 32,
                            width: width / 2.6,
                            decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffC6AB74)),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Center(
                              child: AutoSizeText(
                                "Tambah Keranjang",
                                style: GoogleFonts.poppins(textStyle:
                                TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w400,
                                    color: Color(0xffC6AB74))),
                                maxLines: 1,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            Divider(thickness: 4,),
            SizedBox(height: height / 86,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AutoSizeText(
                (isLanguage != true) ? "Terlaris" : "Best Seller",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              child: Row(
                children: [
                  1, 2, 3, 4
                ].map((e) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Container(
                      height: height / 2.3,
                      width: width / 2.6,
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 12),
                                child: Container(
                                  height: height / 4.2,
                                  width: width / 2.6,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/image/image_product_beauty.png"),
                                          fit: BoxFit.cover
                                      ),
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 2,
                                        blurRadius: 5,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Icon(Icons.star_border, size: 32,
                                    color: Colors.grey,),
                                ),
                              )
                            ],
                          ),
                          Center(
                            child: AutoSizeText(
                              "Byoote",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          Center(
                            child: AutoSizeText(
                              "Byoote gluthateon",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w400)),
                              maxLines: 1,
                            ),
                          ),
                          Center(
                            child: AutoSizeText(
                              "Rp 300.000",
                              style: GoogleFonts.poppins(textStyle:
                              TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              RatingBar(
                                itemSize: 18,
                                initialRating: ratingCount,
                                ignoreGestures: true,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemBuilder: (context, _) =>
                                    Icon(
                                      Icons.star,
                                      color: Color(0xffC6AB74),
                                    ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              SizedBox(width: 4,),
                              Text(ratingCount.toString(),
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 10, color: Colors.grey)))
                            ],
                          ),
                          SizedBox(height: height / 86,),
                          Container(
                            height: height / 32,
                            width: width / 2.6,
                            decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffC6AB74)),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Center(
                              child: AutoSizeText(
                                "Tambah Keranjang",
                                style: GoogleFonts.poppins(textStyle:
                                TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w400,
                                    color: Color(0xffC6AB74))),
                                maxLines: 1,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            Divider(thickness: 4,),
            SizedBox(height: height / 86,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AutoSizeText(
                (isLanguage != true) ? "Produk" : "Product",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
            ),
            BlocBuilder<ProductHealthBloc, ProductHealthState>(
              builder: (_, state){
                if(state is ProductHealthUninitialized){
                  return Center(
                    child: SizedBox(
                      width: 30,
                      height: 30,
                      child: CircularProgressIndicator(),
                    ),
                  );
                }else{
                  ProductHealthLoaded productHealthLoaded = state as ProductHealthLoaded;
                  return GridView.count(
                    controller: _scrollViewController,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    childAspectRatio: 3 / 6,
                    children: productHealthLoaded.products.map((value) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Container(
                          height: height / 2.3,
                          width: width / 2.6,
                          child: Column(
                            children: [
                              Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 12),
                                    child: Container(
                                      height: height / 4.2,
                                      width: width / 2.6,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage("assets/image/image_product_beauty.png"),
                                              fit: BoxFit.cover
                                          ),
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        shape: BoxShape.circle,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 2,
                                            blurRadius: 5,
                                            offset: Offset(0, 3), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: Icon(Icons.star_border, size: 32,
                                        color: Colors.grey,),
                                    ),
                                  )
                                ],
                              ),
                              Center(
                                child: AutoSizeText(
                                  "Byoote",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                                  maxLines: 1,
                                ),
                              ),
                              Center(
                                child: AutoSizeText(
                                  value.name,
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                  maxLines: 1,
                                ),
                              ),
                              Center(
                                child: AutoSizeText(
                                  "Rp " + value.price.toString(),
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                                  maxLines: 1,
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RatingBar(
                                    itemSize: 18,
                                    initialRating: ratingCount,
                                    ignoreGestures: true,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: value.rating,
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Color(0xffC6AB74),
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                  SizedBox(width: 4,),
                                  Text(value.rating.toString(), style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 10, color: Colors.grey)))
                                ],
                              ),
                              SizedBox(height: height / 86,),
                              Container(
                                height: height / 32,
                                width: width / 2.6,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xffC6AB74)),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Center(
                                  child: AutoSizeText(
                                    "Tambah Keranjang",
                                    style: GoogleFonts.poppins(textStyle:
                                    TextStyle(fontSize: 14, fontWeight: FontWeight.w400,
                                        color: Color(0xffC6AB74))),
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
