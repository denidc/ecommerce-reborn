import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_reborn/ui/cart/cart_activity.dart';
import 'package:ecommerce_reborn/ui/notification/notification_activity.dart';
import 'package:ecommerce_reborn/ui/shop/product_beauty_page.dart';
import 'package:ecommerce_reborn/ui/shop/product_health_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ShopActivity extends StatefulWidget {
  @override
  _ShopActivityState createState() => _ShopActivityState();
}

class _ShopActivityState extends State<ShopActivity> with SingleTickerProviderStateMixin{
  bool isLanguage = false;
  TabController _tabController;

  int tabPage = 0;

  @override
  void initState() {
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: height / 7.2,
        backgroundColor: Colors.white,
        flexibleSpace: SafeArea(
          child: Column(
            children: [
              Container(
                width: width,
                height: height / 14,
                child: Padding(
                  padding: EdgeInsets.all(width / 86),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: height / 20,
                          decoration: BoxDecoration(
                              color: Color(0xffF0F0F5),
                              borderRadius: BorderRadius.circular(6)
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: width / 86),
                            child: Row(
                              children: [
                                Icon(Icons.search, color: Color(0xffAFB1BD),),
                                SizedBox(width: width / 32,),
                                AutoSizeText(
                                  (isLanguage != true)?"Cari Produk":"Search Product",
                                  style: GoogleFonts.poppins(textStyle:
                                  TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xffAFB1BD))),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width / 48,),
                      GestureDetector(
                        onTap: (){
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: NotificationActivity()));
                        },
                        child: Stack(
                          children: [
                            Icon(
                              Icons.notifications_none,
                              size: width / 14,
                              color: Color(0xff707070),
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                width: width / 24,
                                height: width / 24,
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.white)
                                ),
                                child: Center(
                                  child: AutoSizeText(
                                    "99",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 8,
                                            fontWeight:
                                            FontWeight.w500, color: Colors.white)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: width / 48,),
                      GestureDetector(
                        onTap: (){
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: CartActivity()));
                        },
                        child: Stack(
                          children: [
                            Icon(
                              Icons.shopping_cart,
                              size: width / 14,
                              color: Color(0xff707070),
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                width: width / 24,
                                height: width / 24,
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.white)
                                ),
                                child: Center(
                                  child: AutoSizeText(
                                    "1",
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 8,
                                            fontWeight:
                                            FontWeight.w500, color: Colors.white)),
                                    minFontSize: 0,
                                    maxLines: 1,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              TabBar(
                controller: _tabController,
                indicatorColor: Color(0xffC6AB74),
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.black,
                labelPadding: EdgeInsets.all(0),
                indicatorPadding: EdgeInsets.all(0),
                onTap: (v){
                  setState(() {
                    tabPage = v;
                  });
                },
                tabs: [
                  Container(
                    height: height / 18,
                    child: Tab(
                      child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: width / 12,
                          height: height / 18,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage((tabPage != 1)?
                                "assets/image/icon_enable_beauty.png":
                                "assets/image/icon_disable_beauty.png"),
                              ),
                          ),
                        ),
                        SizedBox(width: width / 72,),
                        AutoSizeText(
                          (isLanguage != true)?"Kecantikan":"Beauty",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                          minFontSize: 0,
                          maxLines: 1,
                        ),
                      ],
                    ),),
                  ),
                  Container(
                    height: height / 18,
                    child: Tab(child:
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: width / 12,
                          height: height / 18,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage((tabPage != 0)?
                                "assets/image/icon_enable_health.png":
                                "assets/image/icon_disable_health.png"),
                              )
                          ),
                        ),
                        SizedBox(width: width / 72,),
                        AutoSizeText(
                          (isLanguage != true)?"Kesehatan":"Health",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                          minFontSize: 0,
                          maxLines: 1,
                        ),
                      ],
                    )),
                  ),
                ],
              ),
            ],
          ),
        )
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          new ProductBeautyPage(),
          new ProductHealthPage(),
        ],
      ),
    );
  }
}
