import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/auth/forgot_password/forgot_password_activity.dart';
import 'package:ecommerce_reborn/ui/auth/registration/registration_activity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class LoginActivity extends StatefulWidget {
  @override
  _LoginActivityState createState() => _LoginActivityState();
}

class _LoginActivityState extends State<LoginActivity> {
  bool isLanguage = false;

  TextEditingController _loginTextUsername = TextEditingController(text: "");
  TextEditingController _loginTextPassword = TextEditingController(text: "");
  FocusNode fPassword;

  bool isEnableUsername = false;
  bool isEnablePassword = false;

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height / 46,),
                Center(
                  child: AutoSizeText(
                      (isLanguage != true)?"Masuk":"Sign in",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                ),
                SizedBox(height: height / 46,),
                Divider(thickness: 2,),
                SizedBox(height: height / 32,),
                Center(child: SvgPicture.asset("assets/image/logo_app.svg")),
                Center(
                  child: AutoSizeText(
                    "REBORN",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.w600)),
                    maxLines: 1,
                  ),
                ),
                SizedBox(height: height / 32,),
                AutoSizeText(
                  (isLanguage != true)?"Nama Pengguna":"Username",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    controller: _loginTextUsername,
                    onChanged: (v){
                      if(v.length > 4){
                        setState(() {
                          isEnableUsername = true;
                        });
                      }else if(v.length < 5){
                        setState(() {
                          isEnableUsername = false;
                        });
                      }
                    },
                    keyboardType: TextInputType.name,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Nama Pengguna":"Enter Username",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height / 46,),
                AutoSizeText(
                  (isLanguage != true)?"Kata Sandi":"Password",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    enableInteractiveSelection: false,
                    autocorrect: false,
                    focusNode: fPassword,
                    onSubmitted: (term){
                      fPassword.unfocus();
                    },
                    onChanged: (v){
                      if(v.length > 7){
                        setState(() {
                          isEnablePassword = true;
                        });
                      }else if(v.length < 8){
                        setState(() {
                          isEnablePassword = false;
                        });
                      }
                    },
                    obscureText: _obscureText,
                    controller: _loginTextPassword,
                    keyboardType: TextInputType.visiblePassword,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Kata Sandi":"Enter Password",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                        suffixIcon: IconButton(
                          onPressed: _toggle,
                          icon: Icon(_obscureText ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                            color: Colors.black,),
                        )
                    ),
                  ),
                ),
                SizedBox(height: height / 28,),
                Center(
                  child: Container(
                    width: width / 1.4,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6.0),
                        color: (isEnableUsername && isEnablePassword)?Color(0xffc6ab74):Color(0xffF5E2C2),
                        child: InkWell(
                          onTap: (){
                            // Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.leftToRight, child: RegistrationActivity()) );
                          },
                          splashColor: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 8),
                              child: Text((isLanguage != true)?"Masuk":"Sign in", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                            ),
                          ),
                        ),
                      ),
                  ),
                ),
                SizedBox(height: height / 28,),
                Center(
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ForgotPasswordActivity()) );
                    },
                    child: AutoSizeText(
                      (isLanguage != true)?"Lupa Kata Sandi?":"Forgot Password?",
                      style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10, color: Color(0xffC6AB74))),
                      maxLines: 1,
                    ),
                  ),
                ),
                SizedBox(height: height / 48,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AutoSizeText(
                      (isLanguage != true)?"Belum punya akun?":"Don't have an account yet?",
                      style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10, color: Color(0xffC6AB74))),
                      maxLines: 1,
                    ),
                    SizedBox(width: width / 28,),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationActivity()) );
                      },
                      child: AutoSizeText(
                        (isLanguage != true)?"Daftar sekarang":"Sign up now",
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 12, color: Color(0xffc6ab74))),
                        maxLines: 1,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
