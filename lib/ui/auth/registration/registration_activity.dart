import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/auth/confirm_registration/confirm_registration_activity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class RegistrationActivity extends StatefulWidget {
  @override
  _RegistrationActivityState createState() => _RegistrationActivityState();
}

class _RegistrationActivityState extends State<RegistrationActivity> {
  bool isLanguage = false;

  TextEditingController _registrationTextEmail = TextEditingController(text: "");
  TextEditingController _registrationTextUsername = TextEditingController(text: "");
  TextEditingController _registrationTextPassword = TextEditingController(text: "");
  TextEditingController _registrationTextConfirmPassword = TextEditingController(text: "");
  FocusNode fPassword;
  FocusNode fConfirmPassword;

  bool isEnableUsername = false;
  bool isEnablePassword = false;
  bool isEnableConfirmPassword = false;

  bool _obscureTextPassword = true;
  bool _obscureTextConfirmPassword = true;

  void _togglePassword() {
    setState(() {
      _obscureTextPassword = !_obscureTextPassword;
    });
  }
  void _toggleConfirmPassword() {
    setState(() {
      _obscureTextConfirmPassword = !_obscureTextConfirmPassword;
    });
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height / 46,),
                Center(
                  child: AutoSizeText(
                    (isLanguage != true)?"Daftar":"Sign up",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                ),
                SizedBox(height: height / 46,),
                Divider(thickness: 2,),
                SizedBox(height: height / 32,),
                Center(child: SvgPicture.asset("assets/image/logo_app.svg")),
                Center(
                  child: AutoSizeText(
                    "REBORN",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.w600)),
                    maxLines: 1,
                  ),
                ),
                SizedBox(height: height / 32,),
                AutoSizeText(
                  (isLanguage != true)?"Email":"Email",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    controller: _registrationTextEmail,
                    keyboardType: TextInputType.emailAddress,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Email":"Enter Email",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height / 46,),
                AutoSizeText(
                  (isLanguage != true)?"Nama Pengguna":"Username",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    controller: _registrationTextUsername,
                    onChanged: (v){
                      if(v.length > 4){
                        setState(() {
                          isEnableUsername = true;
                        });
                      }else if(v.length < 5){
                        setState(() {
                          isEnableUsername = false;
                        });
                      }
                    },
                    keyboardType: TextInputType.name,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Nama Pengguna":"Enter Username",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height / 46,),
                AutoSizeText(
                  (isLanguage != true)?"Kata Sandi":"Password",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    enableInteractiveSelection: false,
                    autocorrect: false,
                    focusNode: fPassword,
                    onSubmitted: (term){
                      fPassword.unfocus();
                    },
                    onChanged: (v){
                      if(v.length > 7){
                        setState(() {
                          isEnablePassword = true;
                        });
                      }else if(v.length < 8){
                        setState(() {
                          isEnablePassword = false;
                        });
                      }
                    },
                    obscureText: _obscureTextPassword,
                    controller: _registrationTextPassword,
                    keyboardType: TextInputType.visiblePassword,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Kata Sandi":"Enter Password",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 1),
                        ),
                        suffixIcon: IconButton(
                          onPressed: _togglePassword,
                          icon: Icon(_obscureTextPassword ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                            color: Colors.black,),
                        )
                    ),
                  ),
                ),
                SizedBox(height: height / 46,),
                AutoSizeText(
                  (isLanguage != true)?"Ulangi Kata Sandi":"Password",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    enableInteractiveSelection: false,
                    autocorrect: false,
                    focusNode: fConfirmPassword,
                    onSubmitted: (term){
                      fConfirmPassword.unfocus();
                    },
                    onChanged: (v){
                      if(v.length > 7){
                        setState(() {
                          isEnableConfirmPassword = true;
                        });
                      }else if(v.length < 8){
                        setState(() {
                          isEnableConfirmPassword = false;
                        });
                      }
                    },
                    obscureText: _obscureTextConfirmPassword,
                    controller: _registrationTextConfirmPassword,
                    keyboardType: TextInputType.visiblePassword,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Ulang Kata Sandi":"Enter Again Password",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 1),
                        ),
                        suffixIcon: IconButton(
                          onPressed: _toggleConfirmPassword,
                          icon: Icon(_obscureTextConfirmPassword ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                            color: Colors.black,),
                        )
                    ),
                  ),
                ),
                SizedBox(height: height / 28,),
                Center(
                  child: Container(
                    width: width / 1.4,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: (isEnableUsername && isEnablePassword && isEnableConfirmPassword
                          && _registrationTextEmail.value.toString() != "")
                          ?Color(0xffc6ab74):Color(0xffF5E2C2),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ConfirmRegistrationActivity()) );
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text((isLanguage != true)?"Daftar":"Sign up", style: GoogleFonts.poppins(
                                textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
