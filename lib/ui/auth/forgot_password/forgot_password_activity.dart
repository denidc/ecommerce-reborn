import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ForgotPasswordActivity extends StatefulWidget {
  @override
  _ForgotPasswordActivityState createState() => _ForgotPasswordActivityState();
}

class _ForgotPasswordActivityState extends State<ForgotPasswordActivity> {
  bool isLanguage = false;

  TextEditingController _registrationTextEmail = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height / 46,),
                Center(
                  child: AutoSizeText(
                    (isLanguage != true)?"Lupa Kata Sandi":"Forgot Password",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                ),
                SizedBox(height: height / 46,),
                Divider(thickness: 2,),
                SizedBox(height: height / 24,),
                AutoSizeText(
                  (isLanguage != true)?"Email":"Email",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    controller: _registrationTextEmail,
                    keyboardType: TextInputType.emailAddress,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Email":"Enter Email",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height / 28,),
                Center(
                  child: Container(
                    width: width / 1.4,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: (_registrationTextEmail.value.toString() != "")?Color(0xffc6ab74):Color(0xffF5E2C2),
                      child: InkWell(
                        onTap: (){
                          showDialog(
                              context: context,
                            builder: (ctx){
                              return AlertDialog(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/image/image_mail_sent.svg"),
                                    Container(
                                      width: width,
                                      height: height / 8,
                                      child: Column(
                                        children: [
                                          SizedBox(height: height / 64,),
                                          Center(
                                            child: AutoSizeText(
                                              (isLanguage != true)?"Link Terkirim":"Link Sent",
                                              wrapWords: true,
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11, color: Color(0xffc6ab74))),
                                              maxLines: 1,
                                            ),
                                          ),
                                          SizedBox(height: height / 64,),
                                          Center(
                                            child: AutoSizeText(
                                              (isLanguage != true)?"Kami telah mengirimkan link untuk mengubah":"We have sent you a link to change",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11)),
                                              maxLines: 1,
                                            ),
                                          ),
                                          Center(
                                            child: AutoSizeText(
                                              (isLanguage != true)?"kata sandi Anda":"Your password",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11)),
                                              maxLines: 1,
                                            ),
                                          ),
                                          Center(
                                            child: AutoSizeText(
                                              (isLanguage != true)?"Silahkan cek email Anda":"Please check Your email",
                                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11)),
                                              maxLines: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Center(
                                      child: Container(
                                        width: width / 2,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(6.0),
                                        ),
                                        child: Material(
                                          borderRadius: BorderRadius.circular(6.0),
                                          color: (_registrationTextEmail.value.toString() != "")?Color(0xffc6ab74):Color(0xffF5E2C2),
                                          child: InkWell(
                                            onTap: (){

                                            },
                                            splashColor: Colors.white,
                                            borderRadius: BorderRadius.circular(6.0),
                                            child: Center(
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(vertical: 8),
                                                child: Text((isLanguage != true)?"OK":"OK", style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          );
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text((isLanguage != true)?"Lanjut":"Continue", style: GoogleFonts.poppins(
                                textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
