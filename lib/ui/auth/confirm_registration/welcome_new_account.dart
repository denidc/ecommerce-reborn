import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class WelcomeNewAccount extends StatefulWidget {
  @override
  _WelcomeNewAccountState createState() => _WelcomeNewAccountState();
}

class _WelcomeNewAccountState extends State<WelcomeNewAccount> {
  bool isLanguage = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AutoSizeText(
                (isLanguage != true)?"Selamat Datang Di Rebond!":"Welcome in Rebond!",
                style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                maxLines: 1,
              ),
              SizedBox(height: height / 48,),
              SvgPicture.asset("assets/image/image_welcome.svg"),
              SizedBox(height: height / 32,),
              Center(
                child: AutoSizeText(
                  (isLanguage != true)?"Kami telah mengirim email verikasi ke":"We have sent you a link to change",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11)),
                  maxLines: 1,
                ),
              ),
              Center(
                child: AutoSizeText(
                  "denidc27@gmail.com",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11, fontWeight: FontWeight.w700)),
                  maxLines: 1,
                ),
              ),
              Center(
                child: AutoSizeText(
                  (isLanguage != true)?"Silahkan cek email Anda dan verikasi untuk ":"Please check Your email",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11)),
                  maxLines: 1,
                ),
              ),
              Center(
                child: AutoSizeText(
                  (isLanguage != true)?"pengalaman belanja yang lebih baik":"Please check Your email",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 11)),
                  maxLines: 1,
                ),
              ),
              SizedBox(height: height / 28,),
              Center(
                child: Container(
                  width: width / 1.4,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Material(
                    borderRadius: BorderRadius.circular(6.0),
                    color: Color(0xffc6ab74),
                    child: InkWell(
                      onTap: (){
                        // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: WelcomeNewAccount()) );
                      },
                      splashColor: Colors.white,
                      borderRadius: BorderRadius.circular(6.0),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          child: Text((isLanguage != true)?"Mulai berbelanja sekarang!":"Continue", style: GoogleFonts.poppins(
                              textStyle: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w500)),),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: height / 36,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    (isLanguage != true)?"Atau lengkapi":"Don't have an account yet?",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10)),
                    maxLines: 1,
                  ),
                  SizedBox(width: width / 48,),
                  GestureDetector(
                    onTap: (){
                      // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationActivity()) );
                    },
                    child: AutoSizeText(
                      (isLanguage != true)?"Profil Saya":"Sign up now",
                      style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 12, color: Color(0xffc6ab74))),
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
