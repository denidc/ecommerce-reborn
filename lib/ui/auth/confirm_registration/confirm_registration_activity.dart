import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/auth/confirm_registration/welcome_new_account.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class ConfirmRegistrationActivity extends StatefulWidget {
  @override
  _ConfirmRegistrationActivityState createState() => _ConfirmRegistrationActivityState();
}

class _ConfirmRegistrationActivityState extends State<ConfirmRegistrationActivity> {
  bool isLanguage = false;

  TextEditingController _confirmRegistrationTextUsername = TextEditingController(text: "");
  bool isEnableUsername = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height / 46,),
                Center(
                  child: AutoSizeText(
                    (isLanguage != true)?"Daftar":"Sign up",
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    maxLines: 1,
                  ),
                ),
                SizedBox(height: height / 46,),
                Divider(thickness: 2,),
                SizedBox(height: height / 24,),
                AutoSizeText(
                  (isLanguage != true)?"Nama Lengkap":"Full Name",
                  style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  maxLines: 1,
                ),
                Container(
                  height: height / 18,
                  child: TextField(
                    controller: _confirmRegistrationTextUsername,
                    onChanged: (v){
                      if(v.length > 4){
                        setState(() {
                          isEnableUsername = true;
                        });
                      }else if(v.length < 5){
                        setState(() {
                          isEnableUsername = false;
                        });
                      }
                    },
                    keyboardType: TextInputType.name,
                    cursorColor: Colors.black,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                    decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Nama Lengkap":"Enter Full Name",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height / 28,),
                Center(
                  child: Container(
                    width: width / 1.4,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: (isEnableUsername)?Color(0xffc6ab74):Color(0xffF5E2C2),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: WelcomeNewAccount()) );
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text((isLanguage != true)?"Lanjut":"Continue", style: GoogleFonts.poppins(
                                textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
