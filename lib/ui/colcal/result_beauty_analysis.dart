import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class ResultBeautyAnalysis extends StatefulWidget {
  File _image;
  String _expression;
  String _gender;
  String _age;

  ResultBeautyAnalysis(this._image, this._expression, this._gender, this._age);

  @override
  _ResultBeautyAnalysisState createState() => _ResultBeautyAnalysisState(_image, _expression, _gender, _age);
}

class _ResultBeautyAnalysisState extends State<ResultBeautyAnalysis> {
  File _image;
  String _expression;
  String _gender;
  String _age;

  _ResultBeautyAnalysisState(
      this._image, this._expression, this._gender, this._age);

  bool isLanguage = false;
  bool isTips = false;
  int _point = 0;

  @override
  void initState() {
    if(int.parse(_age) <= 10){
      Future.delayed(Duration(seconds: 1)).then((value) {
        setState(() {
          _point = 90;
        });
      });
    }else if(int.parse(_age) <= 20){
      Future.delayed(Duration(seconds: 1)).then((value) {
        setState(() {
          _point = 80;
        });
      });
    }else if(int.parse(_age) <= 30){
      Future.delayed(Duration(seconds: 1)).then((value) {
        setState(() {
          _point = 70;
        });
      });
    }else if(int.parse(_age) <= 40){
      Future.delayed(Duration(seconds: 1)).then((value) {
        setState(() {
          _point = 60;
        });
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SfRadialGauge(
                  animationDuration: 10000,
                    axes: <RadialAxis>[
                      RadialAxis(minimum: 0, maximum: 100, ranges: <GaugeRange>[
                        GaugeRange(
                            startValue: 0,
                            endValue: 40,
                            gradient: SweepGradient(
                              colors: [Colors.red, Colors.deepPurple],
                            ),
                            startWidth: 20,
                            endWidth: 20),
                        GaugeRange(
                            startValue: 40,
                            endValue: 60,
                            gradient: SweepGradient(
                              colors: [Colors.deepPurple, Colors.cyan],
                            ),
                            startWidth: 20,
                            endWidth: 20),
                        GaugeRange(
                            startValue: 60,
                            endValue: 70,
                            gradient: SweepGradient(
                              colors: [Colors.cyan, Color(0xff63DDA9)],
                            ),
                            startWidth: 20,
                            endWidth: 20),
                        GaugeRange(
                            startValue: 70,
                            endValue: 100,
                            gradient: SweepGradient(
                              colors: [Color(0xff63DDA9), Colors.green],
                            ),
                            startWidth: 20,
                            endWidth: 20),
                      ], pointers: <GaugePointer>[
                        NeedlePointer(value: double.parse(_point.toString()))
                      ], annotations: <GaugeAnnotation>[
                        GaugeAnnotation(
                            widget: Container(
                                child:
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    AutoSizeText(
                                      _point.toString(),
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 24, fontWeight: FontWeight.w700)),
                                      maxLines: 1,
                                    ),
                                    AutoSizeText(
                                      "Points",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
                                      maxLines: 1,
                                    ),
                                  ],
                                )),
                            angle: 90,
                            positionFactor: 0.5)
                      ]
                      ),
                    ]
                ),
              ),
              AutoSizeText(
                "Your face beauty score is $_point !",
                style: GoogleFonts.poppins(textStyle:
                TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
                maxLines: 1,
              ),
              (isTips != true)?Column(
                children: [
                  Container(
                    width: width,
                    height: height / 3,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4)
                    ),
                    child: InteractiveViewer(
                      constrained: false,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Container(
                          width: width,
                          height: height / 3.4,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: FileImage(_image)
                              )
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        "Ekspresi : ",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        _expression,
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        "Jenis Kelamin : ",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        _gender,
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        "Umur : ",
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        _age,
                        style: GoogleFonts.poppins(textStyle:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
                        maxLines: 1,
                      ),
                    ],
                  ),
                  SizedBox(height: height / 86,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: width / 8,
                        height: height / 20,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff3C3B4E),
                        ),
                        child: Icon(Icons.star, color: Colors.white,),
                      ),
                      Container(
                        width: width / 8,
                        height: height / 20,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff3C3B4E),
                        ),
                        child: Icon(Icons.link, color: Colors.white,),
                      ),
                      Container(
                        width: width / 8,
                        height: height / 20,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff3C3B4E),
                        ),
                        child: Icon(Icons.share, color: Colors.white,),
                      ),
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            isTips = true;
                          });
                        },
                        child: Container(
                          width: width / 8,
                          height: height / 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xff3C3B4E),
                          ),
                          child: Icon(MaterialCommunityIcons.lightbulb, color: Colors.white,),
                        ),
                      ),
                    ],
                  ),
                ],
              ):Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: width / 1.6,
                      height: height / 2.6,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16),
                            topRight: Radius.circular(16), bottomLeft: Radius.circular(16))
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      width: width / 10,
                                      height: height / 20,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Color(0xff3C3B4E),
                                      ),
                                      child: Icon(MaterialCommunityIcons.lightbulb,
                                        color: Colors.white,),
                                    ),
                                    AutoSizeText(
                                      " Tips",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
                                      maxLines: 1,
                                    ),
                                  ],
                                ),
                                IconButton(
                                  onPressed: (){
                                    setState(() {
                                      isTips = false;
                                    });
                                  },
                                  icon: Icon(Icons.clear),
                                )
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    AutoSizeText(
                                      "15%",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                                          color: Color(0xff6147FF))),
                                      maxLines: 1,
                                    ),
                                    Container(
                                      width: width / 2.6,
                                      child: AutoSizeText(
                                        "Pakai Byoote secara berkala",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.italic)),
                                        maxLines: 2,
                                      ),
                                    ),
                                    SizedBox(height: height/86,),
                                    AutoSizeText(
                                      "10%",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                                          color: Color(0xff6147FF))),
                                      maxLines: 1,
                                    ),
                                    Container(
                                      width: width / 2.6,
                                      child: AutoSizeText(
                                        "Perbanyak konsumsi air mineral",
                                        style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 12, fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.italic)),
                                        maxLines: 2,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SvgPicture.asset("assets/image/image_tips.svg"),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
