import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:auto_size_text/auto_size_text.dart';

class ResultCalculatorCollagen extends StatefulWidget {
  @override
  _ResultCalculatorCollagenState createState() => _ResultCalculatorCollagenState();
}

class _ResultCalculatorCollagenState extends State<ResultCalculatorCollagen> {
  bool isLanguage = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              children: [
                Container(
                  width: width / 10,
                  height: height / 18,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Icon(Icons.star_border, color: Colors.grey,),
                ),
                SizedBox(width: width / 48,),
                Container(
                  width: width / 10,
                  height: height / 18,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Icon(Icons.link, color: Colors.grey,),
                ),
                SizedBox(width: width / 48,),
                Container(
                  width: width / 10,
                  height: height / 18,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Icon(Icons.share, color: Colors.grey,),
                ),
              ],
            ),
          ),
          SizedBox(height: height / 48,),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new AutoSizeText(
                "Hasil Untuk Kebutuhan",
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600),),
                maxLines: 1,
              ),
              new AutoSizeText(
                "Collagen Anda",
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600),),
                maxLines: 1,
              ),
              SizedBox(height: height / 48,),
              Center(
                child: new CircularPercentIndicator(
                  radius: width / 1.8,
                  lineWidth: 12,
                  animation: true,
                  percent: 0.3,
                  center: new AutoSizeText(
                    "20-30g",
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 45,
                          fontWeight: FontWeight.w500),),
                    maxLines: 1,
                  ),
                  footer: new AutoSizeText(
                    "COLLAGEN PERHARI",
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600),),
                    maxLines: 1,
                  ),
                  circularStrokeCap: CircularStrokeCap.round,
                  progressColor: Color(0xffC6AB74),
                  backgroundColor: Color(0xffC4C4C4),
                ),
              ),
              new AutoSizeText(
                "Hari : Sabtu",
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400),),
                maxLines: 1,
              ),
              new AutoSizeText(
                "Tanggal/Bulan/Tahun : 31 September 2020",
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400),),
                maxLines: 1,
              ),
            ],
          )
        ],
      ),
    );
  }
}
