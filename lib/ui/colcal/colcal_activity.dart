import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce_reborn/ui/colcal/result_beauty_analysis.dart';
import 'package:ecommerce_reborn/ui/colcal/result_calculator_collagen.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:menu_button/menu_button.dart';
import 'package:page_transition/page_transition.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:path/path.dart' as path;

class ColcalActivity extends StatefulWidget {
  @override
  _ColcalActivityState createState() => _ColcalActivityState();
}

class _ColcalActivityState extends State<ColcalActivity>
    with SingleTickerProviderStateMixin {
  bool isLanguage = false;
  TabController _tabController;
  ScrollController _scrollViewController;
  bool isLoading = false;
  bool isProses = false;
  bool isCancel = false;
  bool isClear = true;

  TextEditingController _textWeight = TextEditingController(text: "");
  TextEditingController _textAge = TextEditingController(text: "");

  int tabPage = 0;

  String selectedReason = "Alasan Anda mengkonsumsi collagen?";
  String selectedDiet = "Jenis Diet apa yang Anda jalankan saat ini?";
  String selectedGender = "Jenis Kelamin Anda?";
  String selectedProduct = "Produk kecantikan yang Anda pakai?";

  String _smile = "";
  String _gender = "";
  String _age = "";

  File _image;
  File _imageFile;
  List<Face> _faces;
  FaceDetector faceDetector;
  final picker = ImagePicker();

  ui.Image _imageUi;

  Future checkGenderAndAge(File data) async {
    var stream = http.ByteStream(DelegatingStream.typed(data.openRead()));
    var length = await data.length();
    var uri = Uri.parse("https://api.luxand.cloud/photo/detect");
    var request = http.MultipartRequest("POST", uri);
    request.headers.addAll({"token": "{ad51886a1bfc4936b3a4d494ec628dd8}"});

    request.files.add(http.MultipartFile("photo", stream, length,
        filename: path.basename(data.path)));
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    var d = jsonDecode(responseString);
    print(d);
    print(d[0]["gender"]["value"].toString());
    print(d[0]["age"].toString());
    if(int.parse(d[0]["age"].toString().split('.')[1].substring(0,1)) >= 5){
      int ageRounding = int.parse(d[0]["age"].toString().split('.')[0]) + 1;
      setState(() {
        _age = ageRounding.toString();
        _gender = d[0]["gender"]["value"].toString();
      });
    }else{
      _age = d[0]["age"].toString().split('.')[0];
      _gender = d[0]["gender"]["value"].toString();
    }

    final image = FirebaseVisionImage.fromFile(data);
    List<Face> faces = await faceDetector.processImage(image);
    if (mounted) {
      setState(() {
        _imageFile = data;
        _faces = faces;
        // _loadImage(imageFile);
      });
    }
    List<Rect> rects = [];
    List<dynamic> eyeRight = [];
    List<dynamic> eyeLeft = [];
    List<double> smile = [];

    for (var i = 0; i < faces.length; i++) {
      rects.add(faces[i].boundingBox);
      eyeRight.add(faces[i].rightEyeOpenProbability);
      eyeLeft.add(faces[i].leftEyeOpenProbability);
      smile.add(faces[i].smilingProbability);
    }

    if(eyeRight[0] < 0.4 && eyeLeft[0] < 0.4 ){
      Fluttertoast.showToast(
          msg: "Mata Tertutup",
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );
    }else if(smile[0] < 0.05){
      setState(() {
        _smile = "Tidak Tersenyum";
      });
      print("Tidak Tersenyum");
      // Fluttertoast.showToast(
      //     msg: "Tidak tersenyum",
      //     backgroundColor: Colors.white,
      //     textColor: Colors.black,
      //     fontSize: 16.0
      // );
    }else{
      setState(() {
        _smile = "Tersenyum";
      });
      print("Tersenyum");
      // Fluttertoast.showToast(
      //     msg: "Mata Terbuka dan tersenyum",
      //     backgroundColor: Colors.white,
      //     textColor: Colors.black,
      //     fontSize: 16.0
      // );
    }
    setState(() {
      isLoading = false;
      isProses = false;
      isClear = true;
    });
    if(isCancel != true) {
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.rightToLeft,
              child: ResultBeautyAnalysis(_image, _smile, _gender, _age)));
    }
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery, imageQuality: 10);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<void> _getImageAndDetectFaces(File imageFile) async {
    final image = FirebaseVisionImage.fromFile(imageFile);
    List<Face> faces = await faceDetector.processImage(image);
    if (mounted) {
      setState(() {
        _imageFile = imageFile;
        _faces = faces;
        // _loadImage(imageFile);
      });
    }
    List<Rect> rects = [];
    List<dynamic> eyeRight = [];
    List<dynamic> eyeLeft = [];
    List<double> smile = [];

    for (var i = 0; i < faces.length; i++) {
      rects.add(faces[i].boundingBox);
      eyeRight.add(faces[i].rightEyeOpenProbability);
      eyeLeft.add(faces[i].leftEyeOpenProbability);
      smile.add(faces[i].smilingProbability);
    }

    if(eyeRight[0] < 0.4 && eyeLeft[0] < 0.4 ){
      Fluttertoast.showToast(
          msg: "Mata Tertutup",
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );
    }else if(smile[0] < 0.05){
      Fluttertoast.showToast(
          msg: "Tidak tersenyum",
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );
    }else{
      Fluttertoast.showToast(
          msg: "Mata Terbuka dan tersenyum",
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );
    }
    setState(() {
      isLoading = false;
    });
  }

  _loadImage(File file) async {
    final data = await file.readAsBytes();
    await decodeImageFromList(data).then(
          (value) => setState(() {
        _imageUi = value;
      }),
    );
  }

  @override
  void initState() {
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
    faceDetector = FirebaseVision.instance.faceDetector(
        FaceDetectorOptions(
          mode: FaceDetectorMode.accurate,
          enableLandmarks: true,
          enableClassification: true,
          enableTracking: true,
          enableContours: true,
        )
    );
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    final Widget buttonReason = SizedBox(
      width: width,
      height: height / 18,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                selectedReason,
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: (selectedReason !=
                                "Alasan Anda mengkonsumsi collagen?")
                            ? Colors.black
                            : Colors.grey),),
                maxLines: 1,
              ),
            ),
            SizedBox(
                width: 12,
                height: 17,
                child: FittedBox(
                    fit: BoxFit.fill,
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.grey,
                    )
                )
            ),
          ],
        ),
      ),
    );

    final Widget buttonDiet = SizedBox(
      width: width,
      height: height / 18,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                selectedDiet,
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: (selectedDiet !=
                                "Jenis Diet apa yang Anda jalankan saat ini?")
                            ? Colors.black
                            : Colors.grey),),
                maxLines: 1,
              ),
            ),
            SizedBox(
                width: 12,
                height: 17,
                child: FittedBox(
                    fit: BoxFit.fill,
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.grey,
                    )
                )
            ),
          ],
        ),
      ),
    );

    final Widget buttonGender = SizedBox(
      width: width,
      height: height / 18,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                selectedGender,
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: (selectedGender !=
                                "Jenis Kelamin Anda?")
                            ? Colors.black
                            : Colors.grey),),
                maxLines: 1,
              ),
            ),
            SizedBox(
                width: 12,
                height: 17,
                child: FittedBox(
                    fit: BoxFit.fill,
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.grey,
                    )
                )
            ),
          ],
        ),
      ),
    );

    final Widget buttonProduct = SizedBox(
      width: width,
      height: height / 18,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: AutoSizeText(
                selectedProduct,
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: (selectedProduct !=
                          "Produk kecantikan yang Anda pakai?")
                          ? Colors.black
                          : Colors.grey),),
                maxLines: 1,
              ),
            ),
            SizedBox(
                width: 12,
                height: 17,
                child: FittedBox(
                    fit: BoxFit.fill,
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.grey,
                    )
                )
            ),
          ],
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          elevation: 0,
          toolbarHeight: height / 14,
          backgroundColor: Colors.white,
          flexibleSpace: SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: TabBar(
                controller: _tabController,
                indicatorColor: Color(0xffC6AB74),
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.black,
                onTap: (v) {
                  setState(() {
                    tabPage = v;
                  });
                },
                tabs: [
                  Tab(
                    child: AutoSizeText(
                      (isLanguage != true) ? "Kalkulator Collagen" : "Beauty",
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                      maxLines: 1,
                    ),
                  ),
                  Tab(
                      child: AutoSizeText(
                        (isLanguage != true) ? "Analisis Kecantikan" : "Health",
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400)),
                        maxLines: 1,
                      )
                  ),
                ],
              ),
            ),
          )
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: [
                  SizedBox(height: height / 24,),
                  MenuButton(
                    child: buttonReason,
                    items: ["Untuk Kesehatan Keseluruhan", "Untuk Manfaat Kecantikan",
                      "Untuk Penampilan Muda", "Untuk Kesehatan Sendi", "Untuk Menjaga Kebugaran Tubuh"],
                    topDivider: true,
                    popupHeight: height / 3.1,
                    itemBuilder: (value) => Container(
                        width: width,
                        height: height / 20,
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: AutoSizeText(
                          value,
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500),),
                          maxLines: 1,
                        ),
                    ),
                    toggledChild: Container(
                      color: Colors.white,
                      child: buttonReason,
                    ),
                    divider: Container(
                      height: 1,
                      color: Color(0xffC6AB74),
                    ),
                    onItemSelected: (value) {
                      setState(() {
                        selectedReason = value;
                      });
                    },
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffC6AB74)),
                        borderRadius: const BorderRadius.all(Radius.circular(3.0)),
                        color: Colors.white
                    ),
                    onMenuButtonToggle: (isToggle) {
                      print(isToggle);
                    },
                  ),
                  SizedBox(height: height / 24,),
                  MenuButton(
                    child: buttonDiet,
                    items: ["Saya menjalankan Diet Protein Tinggi", "Saya menjalankan Diet Protein Rata - rata",
                      "Saya menjalankan Diet Rendah Protein"],
                    topDivider: true,
                    popupHeight: height / 4.6,
                    itemBuilder: (value) => Container(
                      width: width,
                      height: height / 20,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AutoSizeText(
                        value,
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500),),
                        maxLines: 1,
                      ),
                    ),
                    toggledChild: Container(
                      color: Colors.white,
                      child: buttonDiet,
                    ),
                    divider: Container(
                      height: 1,
                      color: Color(0xffC6AB74),
                    ),
                    onItemSelected: (value) {
                      setState(() {
                        selectedDiet = value;
                      });
                    },
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffC6AB74)),
                        borderRadius: const BorderRadius.all(Radius.circular(3.0)),
                        color: Colors.white
                    ),
                    onMenuButtonToggle: (isToggle) {
                      print(isToggle);
                    },
                  ),
                  SizedBox(height: height / 24,),
                  MenuButton(
                    child: buttonGender,
                    items: ["Laki - Laki","Perempuan"],
                    topDivider: true,
                    popupHeight: height / 6,
                    itemBuilder: (value) => Container(
                      width: width,
                      height: height / 20,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AutoSizeText(
                        value,
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500),),
                        maxLines: 1,
                      ),
                    ),
                    toggledChild: Container(
                      color: Colors.white,
                      child: buttonGender,
                    ),
                    divider: Container(
                      height: 1,
                      color: Color(0xffC6AB74),
                    ),
                    onItemSelected: (value) {
                      setState(() {
                        selectedGender = value;
                      });
                    },
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffC6AB74)),
                        borderRadius: const BorderRadius.all(Radius.circular(3.0)),
                        color: Colors.white
                    ),
                    onMenuButtonToggle: (isToggle) {
                      print(isToggle);
                    },
                  ),
                  SizedBox(height: height / 24,),
                  Row(
                    children: [
                      Column(
                        children: [
                          AutoSizeText(
                            (isLanguage != true) ? "Berat Badan" : "Health",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          SizedBox(height: height / 86,),
                          Container(
                            height: height / 18,
                            width: width / 2.2,
                            child: TextField(
                              controller: _textWeight,
                              keyboardType: TextInputType.numberWithOptions(),
                              cursorColor: Colors.black,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w600)),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(horizontal: 16),
                                hintStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                helperStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14)),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black, width: 1),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: width / 24,),
                      Column(
                        children: [
                          AutoSizeText(
                            (isLanguage != true) ? "Usia" : "Health",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                          SizedBox(height: height / 86,),
                          Container(
                            height: height / 18,
                            width: width / 2.2,
                            child: TextField(
                              controller: _textAge,
                              keyboardType: TextInputType.numberWithOptions(),
                              cursorColor: Colors.black,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w600)),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(horizontal: 16),
                                hintStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                helperStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14)),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black, width: 1),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: height / 18,),
                  Container(
                    width: width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Color(0xffc6ab74),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: ResultCalculatorCollagen()));
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 12),
                            child: Text((isLanguage != true)?"Hitung Collagen":"Sign in", style: GoogleFonts.poppins(
                                textStyle: TextStyle(color: Colors.white, fontSize: 12,
                                    fontWeight: FontWeight.w600)),),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: [
                  SizedBox(height: height / 24,),
                  MenuButton(
                    child: buttonProduct,
                    items: ["Byoote Collagen Juz","Byoote Juz", "Byoote Collagen"],
                    topDivider: true,
                    popupHeight: height / 4.7,
                    itemBuilder: (value) => Container(
                      width: width,
                      height: height / 20,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AutoSizeText(
                        value,
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500),),
                        maxLines: 1,
                      ),
                    ),
                    toggledChild: Container(
                      color: Colors.white,
                      child: buttonProduct,
                    ),
                    divider: Container(
                      height: 1,
                      color: Color(0xffC6AB74),
                    ),
                    onItemSelected: (value) {
                      setState(() {
                        selectedProduct = value;
                      });
                    },
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffC6AB74)),
                        borderRadius: const BorderRadius.all(Radius.circular(3.0)),
                        color: Colors.white
                    ),
                    onMenuButtonToggle: (isToggle) {
                      print(isToggle);
                    },
                  ),
                  SizedBox(height: height / 86,),
                  Container(
                    width: width,
                    height: height / 3,
                    decoration: BoxDecoration(
                        color: Color(0xffF7F7F8),
                        borderRadius: BorderRadius.circular(4)
                    ),
                    child: (_image != null)
                        ?
                    // Center(
                    //   child: FittedBox(
                    //     child: SizedBox(
                    //       width: _imageUi.width.toDouble(),
                    //       height: _imageUi.height.toDouble(),
                    //       child: CustomPaint(
                    //         painter: FacePainter(_imageUi, _faces),
                    //       ),
                    //     ),
                    //   ),
                    // )
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Container(
                        width: width,
                        height: height / 3.4,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: FileImage(_image)
                            )
                        ),),
                    )
                        :Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.image,
                          color: Color(0xffDFDFE4),
                          size: width / 4,
                        ),
                        SizedBox(height: height / 86,),
                        Container(
                          width: width / 1.6,
                          child: AutoSizeText(
                            "Upload photo hasil pemakain produk kecantikan Anda. JPG, JPEG",
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500, color: Colors.grey)),
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: height / 86,),
                  GestureDetector(
                    onTap: (){
                      if(isProses != true) {
                        getImage();
                      }else{
                        setState(() {
                          isProses = false;
                          isCancel = true;
                        });
                      }
                    },
                    child: Container(
                      width: width / 2,
                      height: height / 24,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: Center(
                        child: AutoSizeText(
                          (isProses != true)?"Upload Image":"Cancel",
                          style: GoogleFonts.poppins(textStyle:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: height / 48,),
                  (_image != null)?Container(
                    width: width,
                    height: height / 18,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Container(
                      width: width,
                      height: height / 18,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6.0),
                        color: Color(0xffc6ab74),
                        child: InkWell(
                          onTap: (){
                            if(isClear) {
                              setState(() {
                                // isLoading = true;
                                isProses = true;
                                isCancel = false;
                                isClear = false;
                              });
                              // _getImageAndDetectFaces(_image);
                              Fluttertoast.showToast(
                                  msg: "Sedang menganalisis wajah",
                                  backgroundColor: Colors.white,
                                  textColor: Colors.black,
                                  fontSize: 16.0
                              );
                              checkGenderAndAge(_image);
                            }else{
                              Fluttertoast.showToast(
                                  msg: "Tunggu beberapa saat, lalu coba ulangi lagi",
                                  backgroundColor: Colors.white,
                                  textColor: Colors.black,
                                  fontSize: 16.0
                              );
                            }
                          },
                          splashColor: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                          child: Center(
                            child: (isLoading != true)?Text(
                              (isLanguage != true)
                                  ? "Analisis kecantikan"
                                  : "Check Out",
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600
                                  )
                              ),
                            ):Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ):Container(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
