import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationActivity extends StatefulWidget {
  @override
  _NotificationActivityState createState() => _NotificationActivityState();
}

class _NotificationActivityState extends State<NotificationActivity>
    with SingleTickerProviderStateMixin{

  TabController _tabController;

  int tabPage = 0;

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: height / 7.5,
        backgroundColor: Colors.white,
        leading: Container(),
        flexibleSpace: SafeArea(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,),
                  ),
                  AutoSizeText(
                    "Notification",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.add, color: Colors.transparent, size: 32,),
                  ),
                ],
              ),
              TabBar(
                controller: _tabController,
                indicatorColor: Color(0xffC6AB74),
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.black,
                onTap: (v){
                  setState(() {
                    tabPage = v;
                  });
                },
                tabs: [
                  Tab(child: AutoSizeText(
                    "Notification",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  ),),
                  Tab(child:
                  AutoSizeText(
                    "Activity",
                    style: GoogleFonts.poppins(textStyle:
                    TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                    maxLines: 1,
                  )),
                ],
              ),
            ],
          ),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          color: Colors.grey,
          backgroundColor: Colors.white,
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: TabBarView(
          controller: _tabController,
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  1,2,3,4,5
                ].map((e) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: width / 48),
                    child: Column(
                      children: [
                        SizedBox(height: height / 86,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width /48),
                          child: Row(
                            children: [
                              Container(
                                width: width / 8,
                                height: width / 8,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey, width: 0.5),
                                    shape: BoxShape.circle
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(width / 38),
                                  child: SvgPicture.asset("assets/image/logo_app.svg"),
                                ),
                              ),
                              SizedBox(width: width / 86,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: width / 1.3,
                                    height: height / 24,
                                    child: AutoSizeText.rich(
                                      TextSpan(children: <InlineSpan>[
                                        TextSpan(text: "Pembelian dengan nomor pesanan "),
                                        TextSpan(text: "RMUDDCDZI", style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 11, fontWeight: FontWeight.w600,
                                            color: Colors.black))),
                                        TextSpan(text: " telah dikonfirmasi"),
                                      ]),
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                                      minFontSize: 0,
                                      maxLines: 2,
                                    ),
                                  ),
                                  SizedBox(height: height * 0.005,),
                                  Container(
                                    height: height / 48,
                                    child: AutoSizeText(
                                      "5 detik yang lalu",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w400,
                                          color: Color(0xffAFB1BD))),
                                      maxLines: 1,
                                      minFontSize: 0,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: height * 0.001,),
                        Divider(thickness: 0.5,),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: [
                  1,2,3,4,5
                ].map((e) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: width / 48),
                    child: Column(
                      children: [
                        SizedBox(height: height / 86,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width /48),
                          child: Row(
                            children: [
                              Container(
                                width: width / 8,
                                height: width / 8,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey, width: 0.5),
                                    shape: BoxShape.circle
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(width / 38),
                                  child: SvgPicture.asset("assets/image/logo_app.svg"),
                                ),
                              ),
                              SizedBox(width: width / 86,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: width / 1.3,
                                    height: height / 24,
                                    child: AutoSizeText.rich(
                                      TextSpan(children: <InlineSpan>[
                                        TextSpan(text: "Kamu baru saja membagikan artikel "),
                                        TextSpan(text: "MCI Official | CaraMembuat Wajah Tetap Fresh", style: GoogleFonts.poppins(textStyle:
                                        TextStyle(fontSize: 11, fontWeight: FontWeight.w600,
                                            color: Colors.black))),
                                      ]),
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w400)),
                                      minFontSize: 0,
                                      maxLines: 2,
                                    ),
                                  ),
                                  SizedBox(height: height * 0.005,),
                                  Container(
                                    height: height / 48,
                                    child: AutoSizeText(
                                      "5 detik yang lalu",
                                      style: GoogleFonts.poppins(textStyle:
                                      TextStyle(fontSize: 11, fontWeight: FontWeight.w400,
                                          color: Color(0xffAFB1BD))),
                                      maxLines: 1,
                                      minFontSize: 0,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: height * 0.001,),
                        Divider(thickness: 0.5,),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
