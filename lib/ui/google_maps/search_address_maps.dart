import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:page_transition/page_transition.dart';

class SearchAddressMaps extends StatefulWidget {
  bool isLanguage;
  Position currentPosition;

  SearchAddressMaps(this.isLanguage, this.currentPosition);

  @override
  _SearchAddressMapsState createState() => _SearchAddressMapsState(isLanguage, currentPosition);
}

class _SearchAddressMapsState extends State<SearchAddressMaps> {
  bool isLanguage;
  Position currentPosition;

  _SearchAddressMapsState(this.isLanguage, this.currentPosition);

  String address = "";
  String specificAddress = "";
  double _lat;
  double _long;
  GoogleMapController mapController;
  Completer<GoogleMapController> _controller = Completer();
  bool isMove = false;
  bool isKeyboardOn = false;

  TextEditingController _addressText = TextEditingController(text: "");

  @override
  void initState() {
    setState(() {
      _lat = currentPosition.latitude;
      _long = currentPosition.longitude;
    });
    KeyboardVisibility.onChange.listen((bool visible) {
      isKeyboardOn = visible;
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: double.infinity,
                child: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(currentPosition.latitude, currentPosition.longitude),
                    zoom: 18,
                  ),
                  onMapCreated: (GoogleMapController controller) {
                    if(isKeyboardOn){
                      _controller.complete(controller);
                    }else{
                      _controller.complete(controller);
                    }
                  },
                  myLocationEnabled: true,
                  compassEnabled: true,
                  myLocationButtonEnabled: true,
                  mapToolbarEnabled: true,
                  onCameraIdle: () async{
                    var addresses = await Geocoder.local.findAddressesFromCoordinates(new Coordinates(_lat, _long));
                    var first = addresses.first;
                    setState(() {
                      isMove = false;
                      address = first.thoroughfare;
                      specificAddress = first.locality +", "+ first.subAdminArea +", "+
                          first.adminArea +", "+ first.countryName +", "+ first.postalCode;
                      print(specificAddress);
                    });
                  },
                  onCameraMoveStarted: (){
                    setState(() {
                      isMove = true;
                      address = (isLanguage != true)?"Memuat":"Loading";
                      specificAddress = (isLanguage != true)?"Memuat":"Loading";
                    });
                  },
                  onCameraMove: (position){
                    setState(() {
                      print(position);
                      _lat = position.target.latitude;
                      _long = position.target.longitude;
                    });
                  },
                )
              ),
              Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Container(
                      //   width: width / 1.1,
                      //   height: height / 16,
                      //   decoration: BoxDecoration(
                      //       color: Colors.white,
                      //       borderRadius: BorderRadius.circular(8)
                      //   ),
                      //   child: Padding(
                      //     padding: const EdgeInsets.symmetric(horizontal: 8),
                      //     child: Center(child: Text(address)),
                      //   ),
                      // ),
                      (isKeyboardOn)?Container():Icon(Icons.location_on,
                        size: 32, color: Colors.red,),
                      SizedBox(height: height / 24,)
                    ],
                  )
              ),
              Positioned(
                top: MediaQuery.of(context).viewInsets.top,
                child: Container(
                  width: width,
                  height: height / 14,
                  decoration: BoxDecoration(
                      color: Colors.white
                  ),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                            icon: Icon(Icons.chevron_left, color: Color(0xffDDC58E), size: 32,)
                        ),
                        Center(
                          child: AutoSizeText(
                            (isLanguage != true)?"Tambah Alamat":"Add Address",
                            style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                            maxLines: 1,
                          ),
                        ),
                        IconButton(
                            onPressed: (){

                            },
                            icon: Icon(Icons.add, color: Color(0xffDDC58E), size: 32,)
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: MediaQuery.of(context).viewInsets.bottom,
                child: Container(
                  width: width,
                  height: height / 3.2,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(15),
                        topRight: Radius.circular(15)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: AutoSizeText(
                              (address != null)?address:"Unnamed Road",
                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                                  fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: AutoSizeText(
                              specificAddress,
                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10,
                                  fontWeight: FontWeight.w500, color: Colors.grey)),
                              maxLines: 2,
                            ),
                          ),
                          SizedBox(height: height / 62,),
                          Align(
                            alignment: Alignment.topLeft,
                            child: AutoSizeText(
                                (isLanguage != true)?"Alamat Detail":"Detail Address",
                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14,
                                  fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          Container(
                            height: height / 24,
                            child: TextField(
                              controller: _addressText,
                              keyboardType: TextInputType.streetAddress,
                              cursorColor: Colors.black,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),
                              decoration: InputDecoration(
                                hintText: (isLanguage != true)?"Tulis No. Rumah, Blok, RT/RW, dll.":"Phone Number",
                                hintStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                helperStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14)),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black, width: 2),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: AutoSizeText(
                              "Tulis Detail Alamat Anda dengan Jelas ",
                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10,
                                  fontWeight: FontWeight.w500)),
                              maxLines: 1,
                            ),
                          ),
                          SizedBox(height: height / 36,),
                          Container(
                              width: width / 1.4,
                              height: height / 18,
                              decoration: BoxDecoration(
                                color: Color(0xffe3cea3),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(6.0),
                                color: Colors.transparent,
                                child: AnimatedSwitcher(
                                    duration: Duration.zero,
                                    child: InkWell(
                                      onTap: () async{
                                        // Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: ShippingAddressPage(isLanguage, Position(latitude: _lat, longitude: _long))) );
                                      },
                                      splashColor: Colors.white,
                                      borderRadius: BorderRadius.circular(6.0),
                                      child: Center(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 22.0, vertical: 4.0),
                                          child: Text((isLanguage != true)?"Pilih Lokasi Ini":"Choose Address", style: GoogleFonts.poppins(
                                              textStyle: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),),
                                        ),
                                      ),
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }
}
